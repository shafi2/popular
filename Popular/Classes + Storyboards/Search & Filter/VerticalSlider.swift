//
//  VerticalSlider.swift
//  Popular
//
//  Created by Appzoc on 08/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import RangeSeekSlider

class VerticalSlider: UITableViewCell, RangeSeekSliderDelegate{
    
    
    @IBOutlet var mainContainer: UIView!
    @IBOutlet var sliderContainer: UIView!
    @IBOutlet var rangeSlider: RangeSeekSlider!
    @IBOutlet var minRangeLBL: UILabel!
    @IBOutlet var maxRangeLBL: UILabel!
    
    @IBOutlet var sliderDescription: UILabel!
    
    
    var min:CGFloat = 0
    var max:CGFloat = 0
    var currentMinValue:CGFloat = 0
    var currentMaxValue:CGFloat = 1000
    
    
    private var currentMinPositionX:CGFloat = 0.0
    private var currentMinPositionY:CGFloat = 0.0
    private var currentMaxPositionX:CGFloat = 0.0
    private var currentMaxPositionY:CGFloat = 0.0
    
    private var minLBLPositionX: CGFloat = 0.0 {
        didSet{
            currentMinPositionX = oldValue
        }
    }
    
    private var minLBLPositionY: CGFloat = 0.0 {
        didSet{
            currentMinPositionY = oldValue
        }
    }
    
    private var maxLBLPositionX: CGFloat = 0.0 {
        didSet{
            currentMaxPositionX = oldValue
        }
    }
    
    private var maxLBLPositionY: CGFloat = 0.0 {
        didSet{
            currentMaxPositionY = oldValue
        }
    }
    
    var mainDataDelegate: FilterMainDataContainer?
    final var currentType: CurrentListType = .kilometers
    
    override func awakeFromNib() {
//        setUpRangeSlider()
//        setUpView()
    }
    
    
    func setUpRangeSlider(){
        rangeSlider.minValue = min
        rangeSlider.maxValue = max
        rangeSlider.selectedMinValue = currentMinValue
        rangeSlider.selectedMaxValue = currentMaxValue
        //print("Min ",min," Max ",max," cMin ",currentMinValue," cMax ",currentMaxValue)
        sliderContainer.transform = CGAffineTransform(rotationAngle: -.pi/2)
        minRangeLBL.transform = CGAffineTransform.init(rotationAngle: .pi/2)
        maxRangeLBL.transform = CGAffineTransform.init(rotationAngle: .pi/2)
        //layoutSubviews()
        //layoutIfNeeded()
        rangeSlider.setNeedsLayout()
    }
    
    func setUpView(type: CurrentListType){
       // mainContainer.isHidden = true
        self.layoutSubviews()
        rangeSlider.delegate = self
        rangeSlider.handleImage = #imageLiteral(resourceName: "rangesliderThumb")
        rangeSlider.selectedHandleDiameterMultiplier = 1.3
        rangeSlider.lineHeight = 10.0
        rangeSlider.hideLabels = true
        
//        minRangeLBL.text = currentMinValue.description
//        maxRangeLBL.text = currentMaxValue.description
        self.currentType = type
        if type == .price {
            minRangeLBL.text = currentMinValue.formatInRuppees()
            maxRangeLBL.text = currentMaxValue.formatInRuppees()
            sliderDescription.text = "Slide the price range to filter out the results"
        }else if type == .kilometers {
            minRangeLBL.text = currentMinValue.formatInKilometer()
            maxRangeLBL.text = currentMaxValue.formatInKilometer()
            sliderDescription.text = "Slide the kilometer range to filter out the results"
        }else {
            minRangeLBL.text = currentMinValue.intValue.description
            maxRangeLBL.text = currentMaxValue.intValue.description
            sliderDescription.text = "Slide the year range to filter out the results"
        }
        

        
        minRangeLBL.layer.borderWidth = 2.5
        minRangeLBL.layer.cornerRadius = 10
        minRangeLBL.layer.borderColor = UIColor.themeGreenDark.cgColor
        minRangeLBL.layer.masksToBounds = true
        maxRangeLBL.layer.borderWidth = 2.5
        maxRangeLBL.layer.cornerRadius = 10
        maxRangeLBL.layer.borderColor = UIColor.themeGreenDark.cgColor
        maxRangeLBL.layer.masksToBounds = true
        //mainContainer.isHidden = false
//        DispatchQueue.main.async {
//            self.rangeSlider.backgroundColor = UIColor.red
//            let newHeight = self.mainContainer.frame.height
//            let newWidth = self.mainContainer.frame.width
//            self.sliderContainer.frame.size.height = newWidth
//            //self.sliderContainer.frame.size.width = newWidth
//        }
        
//        rangeSlider.minValue = PLSearchAttributes.shared.priceMin.CGFloatValue
//        rangeSlider.maxValue = PLSearchAttributes.shared.priceMax.CGFloatValue
//        rangeSlider.selectedMinValue = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
//        rangeSlider.selectedMaxValue = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
//        print("priceMin",PLSearchAttributes.shared.selectedPriceMin.CGFloatValue)
//        print("priceMax",PLSearchAttributes.shared.selectedPriceMax.CGFloatValue)
        
    }
    
    func labelPositionsOfRangeSeekSlider(minLabelPoint: CGPoint, maxLabelPoint: CGPoint) {
//        print("minLBLpoint",minLabelPoint)
//        print("maxLabelPoint",maxLabelPoint)
        minLBLPositionX = minLabelPoint.x
        maxLBLPositionX = maxLabelPoint.x
        minLBLPositionY = minLabelPoint.y
        maxLBLPositionY = maxLabelPoint.y
        
        var positionYForYear: CGFloat = 0.0
        if currentType == .year {
            positionYForYear = -15.0
        }
        
        if detectPhoneModel() == .model_X {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2
            minRangeLBL.frame.origin.y = currentMinPositionY + 58 // plus value move x towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6 + positionYForYear
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 52
        }else if detectPhoneModel() == .model_6P_6sP_7P_8P {
            minRangeLBL.frame.origin.x = currentMinPositionX + 10
            minRangeLBL.frame.origin.y = currentMinPositionY + 59 // adding the value move towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 5 + positionYForYear
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 53
        }else if detectPhoneModel() == .model_6_6s_7_8 {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2
            minRangeLBL.frame.origin.y = currentMinPositionY + 58 // plus value move x towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6 + positionYForYear
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 52
        }else {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2 // minus value upwards changing y
            minRangeLBL.frame.origin.y = currentMinPositionY + 50
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6 + positionYForYear
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 48 // minus value move right changing x
        }

        
        /*if detectPhoneModel() == .model_X {
            minRangeLBL.frame.origin.x = currentMinPositionX + 20
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 20
            minRangeLBL.frame.origin.y = currentMinPositionY + 58
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 55
            
        }else if detectPhoneModel() == .model_6P_6sP_7P_8P {
            minRangeLBL.frame.origin.x = currentMinPositionX - 5
            maxRangeLBL.frame.origin.x = currentMaxPositionX - 5
            minRangeLBL.frame.origin.y = currentMinPositionY + 62
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 55
            
        }else {
            minRangeLBL.frame.origin.x = currentMinPositionX - 5
            maxRangeLBL.frame.origin.x = currentMaxPositionX - 5
            minRangeLBL.frame.origin.y = currentMinPositionY + 60
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 55
        }*/
        
        
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        if mainDataDelegate?.lisType == .price {
            //print("Price")
            // print("rangeSeekSlider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            
//            minRangeLBL.text = minValue.int64Value.description
//            maxRangeLBL.text = maxValue.int64Value.description
            let setMin = CGFloat(mainDataDelegate?.mainSourceData?.price.min ?? 0)
            let setMax = CGFloat(mainDataDelegate?.mainSourceData?.price.max ?? 0)
            if minValue == setMin{
                minRangeLBL.text = minValue.formatInRuppees()
                mainDataDelegate?.mainSourceData?.price.currentMin = minValue
            }else{
                minRangeLBL.text = minValue.roundedToTenThousand.formatInRuppees()
                mainDataDelegate?.mainSourceData?.price.currentMin = minValue.roundedToTenThousand
            }
            
            if maxValue == setMax{
                maxRangeLBL.text = maxValue.formatInRuppees()
                mainDataDelegate?.mainSourceData?.price.currentMax = maxValue
            }else{
                maxRangeLBL.text = maxValue.roundedToTenThousand.formatInRuppees()
                mainDataDelegate?.mainSourceData?.price.currentMax = maxValue.roundedToTenThousand
            }
            
            
            mainDataDelegate?.menuDataSource.setIndicator(type: .price, isVisible: true)
//            PLSearchAttributes.shared.selectedPriceMin = minValue.int64Value
//            PLSearchAttributes.shared.selectedPriceMax = maxValue.int64Value
//
//            PLSearchAttributes.shared.isSelectedPrice = true
        }else if mainDataDelegate?.lisType == .kilometers {
            //print("Kilometer")
            
            let setMin = CGFloat(mainDataDelegate?.mainSourceData?.kilometer.min ?? 0)
            let setMax = CGFloat(mainDataDelegate?.mainSourceData?.kilometer.max ?? 0)
            
            if minValue == setMin{
                minRangeLBL.text = minValue.formatInKilometer()
                mainDataDelegate?.mainSourceData?.kilometer.currentMin = minValue
            }else{
                minRangeLBL.text = minValue.roundedToFiveThousand.formatInKilometer()
                mainDataDelegate?.mainSourceData?.kilometer.currentMin = minValue.roundedToFiveThousand
            }
            
            if maxValue == setMax{
                maxRangeLBL.text = maxValue.formatInKilometer()
                mainDataDelegate?.mainSourceData?.kilometer.currentMax = maxValue
            }else{
                maxRangeLBL.text = maxValue.roundedToFiveThousand.formatInKilometer()
                mainDataDelegate?.mainSourceData?.kilometer.currentMax = maxValue.roundedToFiveThousand
            }
//            minRangeLBL.text = minValue.roundedToFiveThousand.formatInKilometer()// + " km"
//            maxRangeLBL.text = maxValue.roundedToFiveThousand.formatInKilometer()// + " km"
//
//            mainDataDelegate?.mainSourceData?.kilometer.currentMin = minValue.roundedToFiveThousand
//            mainDataDelegate?.mainSourceData?.kilometer.currentMax = maxValue.roundedToFiveThousand
            
            mainDataDelegate?.menuDataSource.setIndicator(type: .kilometers, isVisible: true)
//            PLSearchAttributes.shared.selectedKilometerMin = minValue.intValue
//            PLSearchAttributes.shared.selectedKilometerMax = maxValue.intValue
//            PLSearchAttributes.shared.isSelectedKilometer = true
        }else{
            minRangeLBL.text = minValue.intValue.description
            maxRangeLBL.text = maxValue.intValue.description
            mainDataDelegate?.mainSourceData?.year.currentMin = minValue
            mainDataDelegate?.mainSourceData?.year.currentMax = maxValue
            mainDataDelegate?.menuDataSource.setIndicator(type: .year, isVisible: true)
        }
    }
    
//    func didStartTouches(in slider: RangeSeekSlider) {
//        // print("did start touches")
//    }
//
    func didEndTouches(in slider: RangeSeekSlider) {
        //print("did end touches")
        mainDataDelegate?.reloadMenuTable()

    }
    
    
    
}
