//
//  PLFilterVC+Table.swift
//  Popular
//
//  Created by Appzoc on 07/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLFilterLeftCell: UITableViewCell{
    @IBOutlet var optionNameLBL: UILabel!
    @IBOutlet var redIndicatorView: BaseView!
    
    func setAsSelected(){
        self.optionNameLBL.textColor = UIColor.black
        self.backgroundColor = UIColor.white
    }
    
    func setDeseleted(){
        self.optionNameLBL.textColor = UIColor.from(hex: "919191")
        self.backgroundColor = UIColor.from(hex: "F4F4F4")
    }
}

class PLFiterTickCell: UITableViewCell{
    @IBOutlet var tickImage: UIImageView!
    @IBOutlet var subOptionLBL: UILabel!
    
    func setAsSelected(){
        let checkedImage = UIImage(named: "checked")!
        let unCheckedImage = UIImage(named: "unchecked")!
        if tickImage.image == checkedImage{
            tickImage.image = unCheckedImage
        }else{
            tickImage.image = checkedImage
        }
    }
    
    func setState(state:Bool){
        if state{
            tickImage.image = UIImage(named: "checked")!
        }else{
            tickImage.image = UIImage(named: "unchecked")!
        }
    }
}

class PLFilterSliderCell: UITableViewCell{
    
}

extension PLFilterVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == menuTV{
            return menuDataSource.item.count
        }else{
            switch lisType{
            case .brand:
                return mainSourceData?.brand.count ?? 0
//            case .model:
//                return mainSourceData?.carModel.count ?? 0
            case .location:
                return mainSourceData?.location.count ?? 0
            case .exteriorColor:
                return mainSourceData?.color.count ?? 0
            case .transmission:
                return mainSourceData?.transmission.count ?? 0
            case .fuelType:
                return mainSourceData?.fuelType.count ?? 0
            case .category:
                return mainSourceData?.category.count ?? 0
            case .feature:
                return mainSourceData?.feature.count ?? 0
            case .price:
                return 1
            case .year:
                return 1
            case .kilometers:
                return 1
            case .ownership:
                return mainSourceData?.ownership.count ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == menuTV{
            let leftSideCell = tableView.dequeueReusableCell(withIdentifier: "PLFilterLeftCell") as! PLFilterLeftCell
            leftSideCell.optionNameLBL.text = menuDataSource.item[indexPath.row].mainLabel
            leftSideCell.redIndicatorView.isHidden = !(menuDataSource.item[indexPath.row].isIndicatorVisible)
            if let currentIndex = leftCurrentSelectedIndex, currentIndex == indexPath{
                leftSideCell.setAsSelected()
            }else{
                leftSideCell.setDeseleted()
            }
            //print("Menu indicator visible ",menuDataSource.item[indexPath.row].isIndicatorVisible)
            leftSideCell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return leftSideCell
        }else{
            let tickMarkCell = tableView.dequeueReusableCell(withIdentifier: "PLFiterTickCell") as! PLFiterTickCell
            let sliderCell = tableView.dequeueReusableCell(withIdentifier: "VerticalSlider") as! VerticalSlider
            tickMarkCell.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
            sliderCell.separatorInset = UIEdgeInsets(top: 0, left: sliderCell.bounds.width + 500, bottom: 0, right: 0)
            sliderCell.mainDataDelegate = self
            //guard let mainData = mainSourceData else{ return tickMarkCell }
            switch lisType{
                case .brand:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.brand[indexPath.row].name
                    tickMarkCell.setState(state: mainSourceData?.brand[indexPath.row].isSelected ?? false)
                    return tickMarkCell
//                case .model:
//                    tickMarkCell.subOptionLBL.text = mainSourceData?.carModel[indexPath.row].model
//                    tickMarkCell.setState(state: mainSourceData?.carModel[indexPath.row].isSelected ?? false)
//                    return tickMarkCell
                case .location:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.location[indexPath.row].name
                    tickMarkCell.setState(state: mainSourceData?.location[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .exteriorColor:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.color[indexPath.row].color
                    tickMarkCell.setState(state: mainSourceData?.color[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .transmission:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.transmission[indexPath.row].transmission
                    tickMarkCell.setState(state: mainSourceData?.transmission[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .fuelType:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.fuelType[indexPath.row].name
                    tickMarkCell.setState(state: mainSourceData?.fuelType[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .category:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.category[indexPath.row].name
                    tickMarkCell.setState(state: mainSourceData?.category[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .feature:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.feature[indexPath.row].name
                    tickMarkCell.setState(state: mainSourceData?.feature[indexPath.row].isSelected ?? false)
                    return tickMarkCell
                case .price:
                    sliderCell.min = mainSourceData?.price.min.CGFloatValue ?? -1
                    sliderCell.max = mainSourceData?.price.max.CGFloatValue ?? -1
                    sliderCell.currentMinValue = mainSourceData?.price.currentMin ?? mainSourceData?.price.min.CGFloatValue ?? 1
                    sliderCell.currentMaxValue = mainSourceData?.price.currentMax ?? mainSourceData?.price.max.CGFloatValue ?? 1000000
                    sliderCell.setUpRangeSlider()
                    sliderCell.setUpView(type: lisType)
                    return sliderCell
                case .year:
                    sliderCell.min = mainSourceData?.year.min.CGFloatValue ?? -1
                    sliderCell.max = mainSourceData?.year.max.CGFloatValue ?? -1
                    sliderCell.currentMinValue = mainSourceData?.year.currentMin ?? mainSourceData?.year.min.CGFloatValue ?? 1
                    sliderCell.currentMaxValue = mainSourceData?.year.currentMax ?? mainSourceData?.year.max.CGFloatValue ?? 1000
                    sliderCell.setUpRangeSlider()
                    sliderCell.setUpView(type: lisType)
                    return sliderCell
                case .kilometers:
                    sliderCell.min = mainSourceData?.kilometer.min.CGFloatValue ?? -1
                    sliderCell.max = mainSourceData?.kilometer.max.CGFloatValue ?? -1
                    sliderCell.currentMinValue = mainSourceData?.kilometer.currentMin ?? mainSourceData?.kilometer.min.CGFloatValue ?? 1000
                    sliderCell.currentMaxValue = mainSourceData?.kilometer.currentMax ?? mainSourceData?.kilometer.max.CGFloatValue ?? 1000000
                    //print("Min ",mainSourceData?.kilometer.currentMin as Any,"Max ",mainSourceData?.kilometer.currentMax as Any)
                    sliderCell.setUpRangeSlider()
                    sliderCell.setUpView(type: lisType)
                    return sliderCell
                case .ownership:
                    tickMarkCell.subOptionLBL.text = mainSourceData?.ownership[indexPath.row]
                    tickMarkCell.setState(state: mainSourceData?.selectedOwnerShip[indexPath.row].isSelected ?? false)
                    return tickMarkCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == menuTV{
            return 45
        }else{
            let tickCellHeight:CGFloat = 45
            let sliderCellHeight = listTV.bounds.height
            switch lisType{
            case .kilometers, .price, .year:
                return sliderCellHeight
            default:
                return tickCellHeight
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == menuTV{
            _ = tableView.cellForRow(at: indexPath) as! PLFilterLeftCell
            //cell.setAsSelected()
            lisType = CurrentListType(rawValue: indexPath.row) ?? CurrentListType(rawValue: 0)!
            //print("List Type ",lisType)
            if let currentIndex = leftCurrentSelectedIndex, currentIndex == indexPath{

            }else{
                self.listTV.contentOffset = CGPoint(x: 0, y: 0)
                DispatchQueue.main.async {
                    self.listTV.reloadData()
                }
            }
            leftCurrentSelectedIndex = indexPath
            menuTV.reloadData()
        }else{
            guard let _ = mainSourceData else {
                //print("Missing Data")
                return
            }
            guard let tickMarkCell = tableView.cellForRow(at: indexPath) as? PLFiterTickCell else {return}
            switch lisType{
                case .brand:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.brand[indexPath.row].isSelected{
                        mainSourceData!.brand[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.brand[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.brand.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
//                case .model:
//                    tickMarkCell.setAsSelected()
//                    if mainSourceData!.carModel[indexPath.row].isSelected{
//                        mainSourceData!.carModel[indexPath.row].isSelected = false
//                    }else{
//                        mainSourceData!.carModel[indexPath.row].isSelected = true
//                    }
//                    if mainSourceData?.carModel.filter({$0.isSelected == true}).count == 0{
//                        menuDataSource.setIndicator(type: lisType, isVisible: false)
//                    }else{
//                        menuDataSource.setIndicator(type: lisType, isVisible: true)
//                    }
//                    let selectedCell = menuTV.indexPathForSelectedRow
//                    menuTV.reloadData()
//                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .location:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.location[indexPath.row].isSelected{
                        mainSourceData!.location[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.location[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.location.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .exteriorColor:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.color[indexPath.row].isSelected{
                        mainSourceData!.color[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.color[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.color.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .transmission:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.transmission[indexPath.row].isSelected{
                        mainSourceData!.transmission[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.transmission[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.transmission.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .fuelType:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.fuelType[indexPath.row].isSelected{
                        mainSourceData!.fuelType[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.fuelType[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.fuelType.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .category:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.category[indexPath.row].isSelected{
                        mainSourceData!.category[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.category[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.category.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .feature:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.feature[indexPath.row].isSelected{
                        mainSourceData!.feature[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.feature[indexPath.row].isSelected = true
                    }
//                    print("Selected ListType : ",mainSourceData!.feature[indexPath.row].name)
//                    print("Selected ListType ID : ",mainSourceData!.feature[indexPath.row].id)
                    if mainSourceData?.feature.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
                case .price:
                    break
                   // print("Slider Control")
                case .year:
                    break
                    //print("Slider Control")
                case .kilometers:
                    break
                   // print("Slider Control")
                case .ownership:
                    tickMarkCell.setAsSelected()
                    if mainSourceData!.selectedOwnerShip[indexPath.row].isSelected{
                        mainSourceData!.selectedOwnerShip[indexPath.row].isSelected = false
                    }else{
                        mainSourceData!.selectedOwnerShip[indexPath.row].isSelected = true
                    }
                    if mainSourceData?.selectedOwnerShip.filter({$0.isSelected == true}).count == 0{
                        menuDataSource.setIndicator(type: lisType, isVisible: false)
                    }else{
                        menuDataSource.setIndicator(type: lisType, isVisible: true)
                    }
                    let selectedCell = menuTV.indexPathForSelectedRow
                    menuTV.reloadData()
                    menuTV.selectRow(at: selectedCell, animated: false, scrollPosition: .none)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == menuTV{
           // let cell = tableView.cellForRow(at: indexPath) as! PLFilterLeftCell
           // cell.setDeseleted()
        }else{

        }
    }
    
//    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
//        if tableView == menuTV{
//            let cell = tableView.cellForRow(at: indexPath) as! PLFilterLeftCell
//            cell.setDeseleted()
//        }else{
//
//        }
//        return indexPath
//    }
    
}
