//
//  PLPopUpEnquiryVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 14/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

enum PLEnquiryType: Int {
    case usedCarSendEnquiry
    case usedCarTestDrive
    case learnDrivingContactUs
    case learnDrivingTheory
    case learnDrivingPractical
    case learnDrivingCources
    case none
}


class PLPopUpEnquiryVC: UIViewController, SideOptionsDelegate, DateTimeDelegate {
    
    var popUPType = ""
    var typeID = -1
    
    var locArry = [String:Any]()
    
    final var exchangable: Int = 0 // 1 for true else 0
    final var carModelId: String = "0"
    final var enquiryType: PLEnquiryType = .none
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var popUpTypeLB: UILabel!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var mobilenumberTF: UITextField!
    @IBOutlet var locationTF: UITextField!
    
    private var callBackDate: String = ""
    private var callBackStartTime: String = ""
    private var callBackEndTime: String = ""

    
    var postParaMeters = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if enquiryType == .usedCarTestDrive {
            self.popUpTypeLB.text = "Test Drive Enquiry"
            self.typeID = 7
        }else if enquiryType == .usedCarSendEnquiry {
            self.popUpTypeLB.text = "Send Enquiry"
            self.typeID = 7
        }else if enquiryType == .learnDrivingContactUs || enquiryType == .learnDrivingTheory || enquiryType == .learnDrivingPractical || enquiryType == .learnDrivingCources {
            self.popUpTypeLB.text = "Send Enquiry"
            self.locationTF.placeholder = "Location"
            self.typeID = 4

        }else {
            
        }
        
        getLocation()
        
        self.mobilenumberTF.text = PLAppState.session.userMobileNumber
        self.nameTF.text = PLAppState.session.userName
        
        //print("PLAppState.session.userMobileNumber",PLAppState.session.userMobileNumber,"PLAppState.session.userName",PLAppState.session.userName)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mobilenumberTF.text = PLAppState.session.userMobileNumber
        self.nameTF.text = PLAppState.session.userName

    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //http://app.appzoc.com/popular/backend/api/getBranches
    
    @IBAction func dismisPopUpBT(_ sender: UIButton) {
        
        self.didMissVC()
    }
    
    @IBAction func selectLocationBT(_ sender: UIButton) {
        if !locArry.isEmpty {
            showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! JsonArray, isStringId: false))
        }else {
            getLocation(isFromLocationBTN: true)
        }
        
        
    }
    
    @IBAction func callBackTapped(_ sender: UIButton) {
        showDateTimePopUp(presentOn: self, delegate: self)
    }
    
    //Mark:- delegate from the date and
    func selectedDateAndTime(dateString: String, startTimeString: String, endTimeString: String) {
        callBackDate = dateString
        callBackStartTime = startTimeString
        callBackEndTime = endTimeString
    }
    
    //Mark:- delegate from side menu options
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
       // print(data.id,data.name)
        self.locationTF.text = data.name
        
        postParaMeters["locationid"] = data.id
        
    }
    
    
    
    @IBAction func submitBT(_ sender: UIButton) {
        
    
        if nameTF.text?.count != 0
        {
            if mobilenumberTF.text?.count != 0
            {
                if locationTF.text?.count != 0
                {
                    postEnq()
                }
                else
                {
                    showBanner(message: "Select Location")
                }
            }
            else
            {
                showBanner(message: "Enter Mobile Number")
            }
        }
        else
        {
            showBanner(message: "Enter Name")
        }
        
        
    }
    
    
    
    func getLocation(isFromLocationBTN: Bool = false)
    {
        var url: String = ""
        if enquiryType == .learnDrivingContactUs || enquiryType == .learnDrivingTheory || enquiryType == .learnDrivingPractical || enquiryType == .learnDrivingCources {
            url = "get/locations?type=\(self.typeID)"//getdrivingBranches"
        }else if enquiryType == .usedCarSendEnquiry || enquiryType == .usedCarTestDrive {
            url = "get/locations?type=\(self.typeID)"
        }
        //print("TypeID:",self.typeID)

        
        BaseThread.asyncMain {
            WebServices.getMethodWithOutActivity(url:url ,
                                      parameter: ["":""],
                                      CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        self.locArry = result
                        //print("self.LocArray",self.locArry)
                        if isFromLocationBTN {
                            BaseThread.asyncMain {
                                self.selectLocationBT(UIButton())
                            }
                        }
                        
                    }
                    else
                    {
                        
                        
                        
                    }
            })
            
            
            
        }
    }
    
}


extension PLPopUpEnquiryVC
{
    
    func postEnq()
    {
        
        postParaMeters["userid"] = PLAppState.session.userID
        postParaMeters["name"] = nameTF.text
        postParaMeters["mobile_number"] = mobilenumberTF.text!
        
//        if enquiryType != .learnDrivingContactUs || enquiryType != .learnDrivingTheory || enquiryType != .learnDrivingPractical || enquiryType != .learnDrivingCources {
//            postParaMeters["carmodelid"] = carModelId
//
//        }
        
        if enquiryType == .usedCarSendEnquiry || enquiryType == .usedCarTestDrive {
            postParaMeters["carid"] = carModelId
            postParaMeters["exchangeble"] = exchangable
        }

        if callBackDate.isNotEmpty && callBackStartTime.isNotEmpty && callBackEndTime.isNotEmpty {
            postParaMeters["call_date"] = callBackDate
            postParaMeters["from_call_time"] = callBackStartTime
            postParaMeters["to_call_time"] = callBackEndTime
        }
        
        if enquiryType == .usedCarSendEnquiry {
            postParaMeters["type"] = 2
        }else if enquiryType == .usedCarTestDrive {
            postParaMeters["type"] = 13
        }else if enquiryType == .learnDrivingContactUs {
            postParaMeters["type"] = 15
        }else if enquiryType == .learnDrivingTheory {
            postParaMeters["type"] = 9
        }else if enquiryType == .learnDrivingPractical {
            postParaMeters["type"] = 10
        }else if enquiryType == .learnDrivingCources {
            postParaMeters["type"] = 11
        }

       // print("Parmeter_carmodelEnquiry",self.postParaMeters)
        
        WebServices.postMethod(url:"carmodelEnquiry" ,
                               parameter: self.postParaMeters,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    BaseThread.asyncMain {
                        let nextVC =  self.storyBoardPopUps?.instantiateViewController(withIdentifier: "PLThankingVC") as! PLThankingVC
                        // dismiss type
                        if self.enquiryType == .usedCarSendEnquiry {
                            nextVC.dismissType = .tripple
                        }else if self.enquiryType == .usedCarTestDrive {
                            nextVC.dismissType = .double
                        }else if self.enquiryType == .learnDrivingTheory || self.enquiryType == .learnDrivingContactUs || self.enquiryType == .learnDrivingPractical || self.enquiryType == .learnDrivingCources {
                            self.view.isHidden = true
                            nextVC.dismissType = .double

                        }else {
                            nextVC.dismissType = .single
                        }
                        
                        self.present(nextVC)
                    }

                }
                else
                {
                    
                }
        })
        
        
        
        
        
        
    }
    
    func didMissVC()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
