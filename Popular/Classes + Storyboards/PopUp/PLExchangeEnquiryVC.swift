//
//  PLExchangeEnquiryVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 11/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLExchangeEnquiryVC: UIViewController {

    @IBOutlet var statusLBL: UILabel!
    @IBOutlet var selectionSwitch: UISwitch!
    
    final var carModelId: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        selectionSwitch.transform = CGAffineTransform(scaleX: 1, y: 1)
        selectionSwitch.isSelected = false
        statusLBL.text = "No"
        selectionSwitch.alpha = 0.5
        statusLBL.alpha = 0.5
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selected(_ sender: UISwitch) {
        if sender.isSelected {
            
            statusLBL.text = "No"
            sender.alpha = 0.5
            statusLBL.alpha = 0.5
            sender.isSelected = false
            
        } else {
            statusLBL.text = "Yes"
            sender.alpha = 1
            statusLBL.alpha = 1
            sender.isSelected = true
        }
    }
    
    @IBAction func SubmitPressed(_ sender: UIButton) {
        let nextVC =  self.storyBoardPopUps!.instantiateViewController(withIdentifier: "PLPopUpEnquiryVC") as! PLPopUpEnquiryVC
        nextVC.enquiryType = .usedCarSendEnquiry
        nextVC.carModelId = self.carModelId
        if selectionSwitch.isSelected {
            nextVC.exchangable = 1
        }else{
            nextVC.exchangable = 0
        }
        self.present(nextVC)

    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */




}


