//
//  PLCoursesModel.swift
//  Popular
//
//  Created by Appzoc-Macmini on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation

class PLCoursesModel {
    var course: String = ""
    var classroom_training: Bool = false
    var simulator_training: Bool = false
    var practicals: Bool = false
    var duration: String = ""
    
    // This property is Used for Expanding Cell
    
    init(object: Json?) {
        guard let object = object else { return }
        course = object["course"] as? String ?? ""
        duration = object["duration"] as? String ?? ""
        
        if let value = object["classroom_training"] as? String {
            if value == "true" {
                classroom_training = true
            }else {
                classroom_training = false
            }
        }else {
            classroom_training = false
        }
        
        if let value = object["simulator_training"] as? String {
            if value == "true" {
                simulator_training = true
            }else {
                simulator_training = false
            }
        }else {
            simulator_training = false
        }
        
        if let value = object["practicals"] as? String {
            if value == "true" {
                practicals = true
            }else {
                practicals = false
            }
        }else {
            practicals = false
        }
        
    }
    
    class func getValues(fromArray: JsonArray) -> [PLCoursesModel] {
        var result: [PLCoursesModel] = []
        fromArray.forEach { (item) in
            result.append(PLCoursesModel(object: item))
        }
        return result
    }
    
    
    
}
