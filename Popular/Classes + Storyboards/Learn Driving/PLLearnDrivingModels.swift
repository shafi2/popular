//
//  PLLearnDrivingModels.swift
//  Popular
//
//  Created by Appzoc-Macmini on 05/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation

//Mark:- Contact us model
class PLLearnDrivingContactModel {
    //var id:Int = 0
    var name:String = ""
    var branches:[PLLearnDrivingContactModel.BranchModel] = []
    init(dictionary: Json?) {
        guard let dictionary = dictionary else { return }
        //id = dictionary["id"] as? Int ?? 0
        name = dictionary["name"] as? String ?? ""
        if let branchArray = dictionary["branches"] as? JsonArray {
            branches = BranchModel.getValues(fromArray: branchArray)
        }
    }
    
    class func getValues(fromArray: JsonArray) -> [PLLearnDrivingContactModel] {
        
        var result:[PLLearnDrivingContactModel] = []
        fromArray.forEach { (item) in
            result.append(PLLearnDrivingContactModel(dictionary: item))
        }
        return result
    }
    
}

/// Branches Model
extension PLLearnDrivingContactModel {
    class BranchModel  {
        var address:String = ""
        var branch_head:String = ""
        var head_email:String = ""
        var head_mobile:String = ""

        //var id:Int = 0
        //var name:String = ""
        //var latitude:String = ""
        //var longitude:String = ""
        //var typeid:Int = 0
        //var location_id:Int = 0
        //var main:Int = 0
       //var location:String = ""
        //var map_link:String = ""

        init(dictionary: Json?) {
            guard let dictionary = dictionary else { return }
            address = dictionary["address"] as? String ?? ""
            branch_head = dictionary["branch_head"] as? String ?? ""
            head_email = dictionary["head_email"] as? String ?? ""
            head_mobile = dictionary["head_mobile"] as? String ?? ""

//            id = dictionary["id"] as? Int ?? 0
//            name = dictionary["name"] as? String ?? ""
//            latitude = dictionary["latitude"] as? String ?? ""
//            longitude = dictionary["longitude"] as? String ?? ""
//            typeid = dictionary["typeid"] as? Int ?? 0
//            location_id = dictionary["location_id"] as? Int ?? 0
//            main = dictionary["main"] as? Int ?? 0
//            location = dictionary["location"] as? String ?? ""
//            map_link = dictionary["map_link"] as? String ?? ""
            
        }
        
        class func getValues(fromArray: JsonArray) -> [BranchModel] {
            
            var result:[BranchModel] = []
            fromArray.forEach { (item) in
                result.append(BranchModel(dictionary: item))
            }
            return result
        }

    }
}
