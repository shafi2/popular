//
//  PLCompareVC+Table.swift
//  Popular
//
//  Created by Appzoc on 06/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit


class PLCompareHeaderCell: UITableViewCell{
    @IBOutlet var headerLBL: UILabel!
    @IBOutlet var headerBTNRef: BaseButton!
    @IBOutlet var arrowImage: UIImageView!
    
}

class PLCompareValueCell: UITableViewCell{
    @IBOutlet var valueDescriptionLBL: UILabel!
    @IBOutlet var leftValueLBL: UILabel!
    @IBOutlet var rightValueLBL: UILabel!
    
    func configureLeftWith(key:String, value:Any){
            valueDescriptionLBL.text = key
            leftValueLBL.text = "\(value)"
    }
    
    func configureRightWith(value:Any){
            rightValueLBL.text = "\(value)"
    }
}

class PLCompareBooleanCell: UITableViewCell{
    @IBOutlet var booleanDescriptionLBL: UILabel!
    @IBOutlet var leftBoolImage: UIImageView!
    @IBOutlet var rightBoolImage: UIImageView!
    
    func configureWith(data:String, presentIn:PLCompareVC.FeaturePresent){
        let presentImg = UIImage(named: "1x_0005_green_tick") ?? UIImage()
        let notPresentImage = UIImage(named: "not-present") ?? UIImage ()
        let nullImage = UIImage(named: "-") ?? UIImage()
        switch presentIn {
        case .leftOnly:
            leftBoolImage.image = presentImg
            rightBoolImage.image = notPresentImage
        case .rightOnly:
            leftBoolImage.image = notPresentImage
            rightBoolImage.image = presentImg
        case .both:
            leftBoolImage.image = presentImg
            rightBoolImage.image = presentImg
        case .null:
            leftBoolImage.image = presentImg
            rightBoolImage.image = nullImage
        }
        booleanDescriptionLBL.text = data
    }
}


extension PLCompareVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if compareData.count > 0{
            return 2
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if overviewSectionCollapsed{
                return 0
            }else{
                return compareData[section].overview.count
            }
        }else{
            if featuresSectionCollapsed{
                return 0
            }else{
                return commonSet.count + leftOnlySet.count + rightOnlySet.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let valueCell = tableView.dequeueReusableCell(withIdentifier: "PLCompareValueCell") as! PLCompareValueCell
            for (index, value) in compareData[0].overview.enumerated(){
                if index == indexPath.row{
                    valueCell.configureLeftWith(key: value.key, value: value.value)
                }
            }
            if compareData.count > 1{
                for (index, value) in compareData[1].overview.enumerated(){
                    if index == indexPath.row{
                        valueCell.configureRightWith(value: value.value)
                    }
                }
            }else{
                valueCell.configureRightWith(value: "-")
            }
            return valueCell
        }else{
            let boolCell = tableView.dequeueReusableCell(withIdentifier: "PLCompareBooleanCell") as! PLCompareBooleanCell
            
            if rightOnlySet.isEmpty{
                let index = indexPath.row
                let setIndex = commonSet.index(commonSet.startIndex, offsetBy: index)
                boolCell.configureWith(data: commonSet[setIndex], presentIn: .null)
            }else if indexPath.row < commonSet.count{
                let setIndex = commonSet.index(commonSet.startIndex, offsetBy: indexPath.row)
                boolCell.configureWith(data: commonSet[setIndex], presentIn: .both)
            }else if indexPath.row - commonSet.count < leftOnlySet.count{
                let index = indexPath.row - commonSet.count
                let setIndex = leftOnlySet.index(leftOnlySet.startIndex, offsetBy: index)
                boolCell.configureWith(data: leftOnlySet[setIndex], presentIn: .leftOnly)
            }else{
                let index = indexPath.row - commonSet.count - leftOnlySet.count
                let setIndex = rightOnlySet.index(rightOnlySet.startIndex, offsetBy: index)
                boolCell.configureWith(data: rightOnlySet[setIndex], presentIn: .rightOnly)
            }
            /*
            //Left Check
            if indexPath.row < compareData[0].features.count{
                let feature = compareData[0].features[indexPath.row]
                var count = 0
                for item in compareData[1].features{
                    if item == feature{
                        count += 1
                    }
                }
                if count > 0{
                    boolCell.configureWith(data: feature, presentIn: .both)
                }else{
                    boolCell.configureWith(data: feature, presentIn: .leftOnly)
                }
            }else{
            //Right Check
                for feature in compareData[1].features{
                    var count = 0
                    for item in compareData[0].features{
                        if item == feature{
                            count += 1
                        }
                    }
                    if count > 0{
                        boolCell.configureWith(data: feature, presentIn: .both)
                    }else{
                        boolCell.configureWith(data: feature, presentIn: .rightOnly)
                    }
                }
            }
            */
            return boolCell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "PLCompareHeaderCell") as! PLCompareHeaderCell
        if section == 0{
            headerCell.headerLBL.text = "Overview"
            headerCell.headerBTNRef.tag = section
            if overviewSectionCollapsed{
                headerCell.arrowImage.image = UIImage(named: "down-arrow")
            }else{
                headerCell.arrowImage.image = UIImage(named: "up-arrow")
            }
        }else{
            headerCell.headerLBL.text = "Features"
            headerCell.headerBTNRef.tag = section
            if featuresSectionCollapsed{
                headerCell.arrowImage.image = UIImage(named: "down-arrow")
            }else{
                headerCell.arrowImage.image = UIImage(named: "up-arrow")
            }
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewWillLayoutSubviews()
    }
}
