//
//  PLCompareVC+DataTypes.swift
//  Popular
//
//  Created by Appzoc on 06/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

extension PLCompareVC{
    //MARK:- Class Specific Data Types
    enum CarState{
        case present
        case absent
    }
    
    enum FeaturePresent{
        case leftOnly
        case rightOnly
        case both
        case null
    }
    
    struct CompareCarModel{
        let usedCarID:Int
        let brand:String
        let model:String
        let expectedPrice:String
        let year:Int
        let engineCapacity:String
        let overview:[String:Any]
        let features:[String]
        let image:String
        
        init(json:[String:Any]) {
            usedCarID = json["usedcarid"] as? Int ?? -1000
            brand = json["brand"] as? String ?? "null"
            model = json["model"] as? String ?? "null"
            expectedPrice = json["expected_price"] as? String ?? "null"
            year = json["year"] as? Int ?? 0
            engineCapacity = json["engine_capacity"] as? String ?? "null"
            overview = json["overview"] as? [String:Any] ?? [:]
            features = json["features"] as? [String] ?? ["null"]
            image = json["images"] as? String ?? "null"
        }

    }
}
