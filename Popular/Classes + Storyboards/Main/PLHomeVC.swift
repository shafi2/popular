//
//  PLHomeVC.swift
//  Popular
//
//  Created by Appzoc on 24/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLHomeMenuCVC: UICollectionViewCell {
    
    @IBOutlet var baseView: BaseView!
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuTitleLBL: UILabel!
}


class PLHomeOfferCVC: UICollectionViewCell {
    @IBOutlet var offerNameLBL: UILabel!
    @IBOutlet var offerTitleLBL: UILabel!
    @IBOutlet var offerDescriptionLBL: UILabel!
    
    func setUpcell(with data:PLHomeVC.OfferList){
        offerNameLBL.text = data.type
        offerTitleLBL.text = data.title
        offerDescriptionLBL.text = data.description
    }
}

class PLHomeVC: UIViewController, InterfaceSettable, RefreshHomePage {
    
    @IBOutlet var profileImage: BaseImageView!
    @IBOutlet var profileNameLBL: UILabel!
    @IBOutlet var menuCV: UICollectionView!
    @IBOutlet var offerCV: UICollectionView!
    @IBOutlet var customerCareView: UIView!
    @IBOutlet var cartCountLBL: UILabel!
    @IBOutlet var customerCareLBL: UILabel!
    
    fileprivate let menuTitles = ["New Cars", "Insurance", "Used Cars", "Services", "Referrals", "Find Us", "Driving School", "Tips", "Accessories", "Life Style"]
    
    fileprivate let menuImages = [#imageLiteral(resourceName: "new-cars"), #imageLiteral(resourceName: "insurance"),#imageLiteral(resourceName: "used-cars"),#imageLiteral(resourceName: "service"),#imageLiteral(resourceName: "referals"),#imageLiteral(resourceName: "find-us"),#imageLiteral(resourceName: "drivingschool"),UIImage(named: "tips-icon-home"),#imageLiteral(resourceName: "accessories"),#imageLiteral(resourceName: "life-style")]
    fileprivate let smallMenuItems = [0,3,4,7,8]
    fileprivate var offersArray:[OfferList] = []
    fileprivate var currentIndexPath = IndexPath(item: 0, section: 0)
    fileprivate var offerTimer = Timer()
    fileprivate var customerCareTimer = Timer()
    fileprivate var isShownCCView = false
    private var offerCVFlowLayout: CenteredCollectionViewFlowLayout!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showForceUpdate), name: .forceUpdateNotify, object: nil)
        PLAppState.session.homeDelegate = self
        menuCV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 1)
        cartCountLBL.layer.cornerRadius = cartCountLBL.frame.width / 2
        cartCountLBL.layer.masksToBounds = true
        
        offerCVFlowLayout = offerCV.collectionViewLayout as! CenteredCollectionViewFlowLayout
        offerCV.decelerationRate = UIScrollViewDecelerationRateFast
        offerCVFlowLayout.itemSize = CGSize(
            width: offerCV.frame.width * 0.7,
            height: offerCV.frame.height * 0.85
        )
        offerCVFlowLayout.minimumLineSpacing = 20
        checkSessionExpiry()
        getOfferList()
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        cartCountLBL.isHidden = true
        setUpInterface()
        getCountWeb()
        if UpdateManager.main.forceUpdateAvailable{
            showForceUpdate()
        }
        if PLAppState.session.isLaunchByNotification == true{
            PLAppState.session.isLaunchByNotification = false
            segueToNotificationList()
        }
    }
    
    @objc func showForceUpdate(){
        UpdateManager.main.showUpdateAlert(on: self)
        
        if PLAppState.session.isLaunchByNotification == true{
            PLAppState.session.isLaunchByNotification = false
            segueToNotificationList()
            //notificationTapped(nil)
        }else
        { setUpInterface()
            getCountWeb() 
        }
    }
    
    func setUpInterface() {
        customerCareLBL.text = emergancyNumberFormatted
        offerTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
        customerCareTimer = Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(showHideCustomerCareView), userInfo: nil, repeats: true)
        if PLAppState.session.isLoggedIn{
            self.profileNameLBL.text = "Hi \(PLAppState.session.userName.capitalized) !"
        }else{
            self.profileNameLBL.text = "Hi Guest !"
        }

    }
    
    func refreshHomeOnLogin() {
        self.setUpInterface()
    }
    
    func checkSessionExpiry(){
        guard PLAppState.session.isLoggedIn == true else { return }
        WebServices.postMethod(url: "checklogoutstatus", parameter: ["user_id":PLAppState.session.userID]) { (isComplete, json) in
            if isComplete{
                let message = json["message"] as? String ?? ""
                print("Message ",message," \n Token ",PLAppState.session.deviceTocken)
                if message != PLAppState.session.deviceTocken{
                    let alertVC = UIAlertController(title: "Session Expired", message: "Your session has expired. Please Login to continue.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        PLAppState.session.logout()
                        alertVC.dismiss(animated: true, completion: {
                            self.setUpInterface()
                        })
                    })
                    alertVC.addAction(okAction)
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func getOfferList(){
        WebServices.getMethodWith(url: "getoffers", parameter: nil) { (isComplete, json) in
            if isComplete{
                if let jsonData = serializeJSON(json: json["data"] as Any){
                    do{
                        let decoder = JSONDecoder()
                        let parsedData = try decoder.decode([OfferList].self, from: jsonData)
                        self.offersArray = parsedData
                        self.offerCV.reloadData()
                    }catch{
                        print(error)
                    }
                }
            }
        }
    }
    
    private func getCountWeb(){
        guard PLAppState.session.userID != 0 else { return }
        
        WebServices.getMethodWithOutActivity(url: "get/counts/\(PLAppState.session.userID.description)", parameter: nil) { (isComplete, response) in
            guard isComplete else { return }
            if let cartValue = response["cart_count"] as? Int {
                BaseThread.asyncMain {
                    if cartValue > 99 {
                        self.cartCountLBL.text = "99+"
                    }else {
                        self.cartCountLBL.text = cartValue.description
                    }
                    self.cartCountLBL.isHidden = cartValue.isZero
                }
            }
        
        
        }

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        offerTimer.invalidate()
        customerCareTimer.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func profileTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn{
            let segueVC = storyboard!.instantiateViewController(withIdentifier: "PLMyProfileVC")
            present(segueVC)
        }else{
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC)
        }
    }
    
    @IBAction func cartTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn {
            let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestyleCartVC") as! PLLifestyleCartVC
            self.present(segueVC)

        }else {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC)

        }
    }
    
    @IBAction func notificationTapped(_ sender: UIButton?) {
        if PLAppState.session.isLoggedIn {
            let segueVC = UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "PLNotificationVC") as! PLNotificationVC
            self.present(segueVC)
        }else {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC)
        }
    }
    
    func segueToNotificationList(){
        let segueVC = UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "PLNotificationVC") as! PLNotificationVC
        DispatchQueue.main.async {
            self.present(segueVC)
        }
    }
    
    @IBAction func callTapped(_ sender: UIButton) {
       // print("Call Tapped")
        makeCallTo(number: emergancyNumber)
    }
    
}



// general functions
extension PLHomeVC {
    
    @objc private func showHideCustomerCareView(){
        if isShownCCView {
            UIView.animate(withDuration: 3.2, animations: {
                self.customerCareView.frame.origin.x += self.customerCareView.frame.width
            })
            isShownCCView = false
        }else{
            UIView.animate(withDuration: 3.2, animations: {
                self.customerCareView.frame.origin.x -= self.customerCareView.frame.width
            })
            isShownCCView = true
        }
    }
    
    
}

// offers and menu collection view data source handling
extension PLHomeVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == menuCV ? menuTitles.count : offersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == menuCV {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLHomeMenuCVC", for: indexPath) as? PLHomeMenuCVC else { return UICollectionViewCell() }
//            if indexPath.row >= 9{
//                cell.alpha = 0.5
//            }
            if smallMenuItems.contains(indexPath.item) {
                cell.baseView.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.6901960784, blue: 0.7098039216, alpha: 1)
            }else {
                if indexPath.item == 2 || indexPath.item == 6 {
                    cell.baseView.backgroundColor = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                }else {
                    cell.baseView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.3058823529, blue: 0.1960784314, alpha: 1)
                }
            }
            cell.menuTitleLBL.text = menuTitles[indexPath.item]
            cell.menuImage.image = menuImages[indexPath.item]
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLHomeOfferCVC", for: indexPath) as! PLHomeOfferCVC
            cell.setUpcell(with: offersArray[indexPath.row])
            return cell

        }
    }
    
}


// offers and menu collection view delegate handling
extension PLHomeVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == menuCV {
            //print("selectedMenu",indexPath.item)
            switch indexPath.item {
            case 0:
                let segueVC = self.storyBoardNewCars!.instantiateViewController(withIdentifier: "PLNewCarListVC")
                present(segueVC, animated: true, completion: nil)
            case 1:
                if PLAppState.session.isLoggedIn {
                    let segueVC = self.storyBoardInsurance!.instantiateViewController(withIdentifier: "PLInsuranceVC")
                    present(segueVC, animated: true, completion: nil)
                }else {
                    showLoginPopUp()
                }

            case 2:
                let segueVC = self.storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLUsedCarsHomeVC")
                present(segueVC)
                
            case 3:
                let segueVC = self.storyBoardService!.instantiateViewController(withIdentifier: "PLServiceHome")
                present(segueVC)
            case 4:
                //return
                let segueVC = self.storyBoardReferals!.instantiateViewController(withIdentifier: "PLReferalHome")
                present(segueVC)
            case 5:
                let segueVC = self.storyBoardFindUs!.instantiateViewController(withIdentifier: "PLFindUsVC")
                present(segueVC)
            case 6:
                let segueVC = self.storyBoardLearnDriving!.instantiateViewController(withIdentifier: "PLLearnDrivingVC")
                present(segueVC)
            case 7:
//                showBanner(message: "Coming Soon")
//                return
                let segueVC = self.storyBoardAssist!.instantiateViewController(withIdentifier: "PLAssistVC")
                present(segueVC)
            case 8:
                showBanner(message: "Coming Soon")
                return
                let segueVC = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "PLAccesoriesVC")
                self.present(segueVC)
            case 9:
                showBanner(message: "Coming Soon")
                return
                let segueVC = self.storyBoardLifestyle!.instantiateViewController(withIdentifier: "PLLifestyleVC")
                present(segueVC)
            default:
                break
                //print("Not Implemented")
            }
        }else{
            let vc = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "PLOffersListVC") as! PLOffersListVC
            self.present(vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == menuCV {
            let frameHeight = collectionView.frame.height
            let frameWidth = (frameHeight * 0.49).rounded()
            let width = frameWidth > 107 ? frameWidth : 107
            let height = smallMenuItems.contains(indexPath.item) ? frameHeight * 0.398 : frameHeight * 0.6
            return CGSize(width: width, height: height)
        }else{
            return CGSize(width: collectionView.frame.width * 0.7, height:  collectionView.frame.height * 0.85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == menuCV ? 1 : 0
    }

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == offerCV {
//            var visibleRect = CGRect()
//            visibleRect.origin = offerCV.contentOffset
//            visibleRect.size = offerCV.bounds.size
//            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//            guard let indexPath = offerCV.indexPathForItem(at: visiblePoint) else { return }
//            currentIndexPath = indexPath
            
            let item = offerCVFlowLayout.currentCenteredPage ?? 0
            currentIndexPath = IndexPath(item: item, section: 0)
        }
        

        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView == offerCV {
            let item = offerCVFlowLayout.currentCenteredPage ?? 0
            currentIndexPath = IndexPath(item: item, section: 0)

        }
    }

    
    @objc func scrollToNextCell(){
        guard offersArray.count > 0 else { return }
        
        if offersArray.count > currentIndexPath.item + 1{
            let nextIndexpath = IndexPath(item: currentIndexPath.item + 1, section: 0)
            offerCV.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
            currentIndexPath = nextIndexpath
        }else{
            currentIndexPath = IndexPath(item: 0, section: 0)
            offerCV.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
  
    struct OfferList:Codable{
        let id:Int
        let title:String
        let description:String
        let type:String
    }
    
}
