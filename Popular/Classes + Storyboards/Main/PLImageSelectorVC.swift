//
//  PLImageSelectorVC.swift
//  Popular
//
//  Created by Appzoc on 06/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import CameraManager
import OpalImagePicker
import AVFoundation
import NotificationBannerSwift

class PLImageSelectorCVC: UICollectionViewCell {
    
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var deleteImageBTN: BaseButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
protocol ImageSelectorDelegate {
    func imageSelector(isSelected: Bool, didFinishSelecting images: [UIImage])
}
class PLImageSelectorVC: UIViewController, InterfaceSettable, OpalImagePickerControllerDelegate {
    
    @IBOutlet var previewView: UIView!
    @IBOutlet var chooseVehicleLBL: UILabel!
    @IBOutlet var myVehicleCV: UICollectionView!
    
    
    final var delegate: ImageSelectorDelegate?
    
    fileprivate var allowedImagesCount = 5
    fileprivate let cameraHandler = CameraManager()
    fileprivate var imageSource: [UIImage] = []
    
    fileprivate lazy var tempImageCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraHandler.stopCaptureSession()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpInterface() {
        cameraHandler.showAccessPermissionPopupAutomatically = false
        cameraHandler.writeFilesToPhoneLibrary = true
        cameraHandler.flashMode = .auto
        let cameraState = cameraHandler.currentCameraStatus()
        switch cameraState {
        case .ready:
            self.startCamera()
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (accessAllowed) in
                if accessAllowed {
                    self.startCamera()
                }
                else{
                    showBanner(title: "Access denied", message: "Please allow access to camera to take photo")
                }
            }
            break
        case .accessDenied:
            showBanner(title: "Access denied", message: "Please allow access to camera to take photo")
            break
        case .noDeviceFound:
            showBanner(title: "", message: "Camera not available")
        }
        
    }
    
    fileprivate func startCamera()
    {
        cameraHandler.addPreviewLayerToView(previewView, newCameraOutputMode: .stillImage)
        cameraHandler.showErrorBlock = {(erTitle: String, erMessage: String) -> Void in
            showBanner(title: "Error", message: "Camera not available")
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        if imageSource.count < allowedImagesCount {
            imageSource.append(contentsOf: images)
        }else{
            showBanner(message: "Maximum " + self.allowedImagesCount.description + " can be added")
        }
        picker.dismiss(animated: true, completion: nil)
        cameraHandler.resumeCaptureSession()
        myVehicleCV.reloadData()
    }


    @IBAction func closeTapped(_ sender: UIButton) {
        if imageSource.isEmpty {
            delegate?.imageSelector(isSelected: false, didFinishSelecting: imageSource)
        }else{
            delegate?.imageSelector(isSelected: true, didFinishSelecting: imageSource)
        }
        dismiss(animated: true, completion: nil)
    }

    @IBAction func captureImageTapped(_ sender: UIButton) {
        cameraHandler.capturePictureWithCompletion({ (image, error) -> Void in
            if error == nil {
                guard let image = image else { return }
                if self.imageSource.count < self.allowedImagesCount {
                    self.imageSource.append(image)
                    self.myVehicleCV.reloadData()
                }else{
                    showBanner(message: "Maximum " + self.allowedImagesCount.description + " can be added")
                }
            }
            else {
                showBanner(title: "Error", message: "Camera not available")
            }
        })

    }
    
    @IBAction func chooseGalleryTapped(_ sender: BaseButton) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = allowedImagesCount - imageSource.count
        present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func deleteImageTapped(_ sender: BaseButton) {
        
        if !imageSource.isEmpty, let index = sender.indexPath?.item, imageSource.count > index {
            //print("image deleted")
            imageSource.remove(at: index)
            myVehicleCV.reloadData()
        }else{
            //print("not deleted")
        }
    }
    
    
}


// collection view datasource
extension PLImageSelectorVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLImageSelectorCVC", for: indexPath) as? PLImageSelectorCVC else { return UICollectionViewCell() }
        cell.deleteImageBTN.indexPath = indexPath
        cell.vehicleImage.image = imageSource[indexPath.item]
        return cell
    }
    
    
}


// collection view delegates
extension PLImageSelectorVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("SelectorVC_selectedIndex",indexPath)
    }
}

