//
//  PLLoginVC.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import TextFieldEffects

class PLLoginVC: UIViewController, InterfaceSettable {
    
    @IBOutlet var nameTF: HoshiTextField!
    @IBOutlet var mobileTF: HoshiTextField!
    
    fileprivate var userName = ""
    fileprivate var password = ""
    fileprivate let maxMobileDigits = 10
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpInterface() {
        
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLOTPVerificationVC") as! PLOTPVerificationVC
        segueVC.mobileNumber = self.mobileTF.text.unwrap
        self.present(segueVC, animated: true, completion: nil)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        if isValidData() {
            getLoginWeb()
        }
    }
    
    private func isValidData() -> Bool {
        
        if !BaseValidator.isValid(email: nameTF.text) {
            showBanner(message: "Invalid Email")
            return false
        }
        
        if !BaseValidator.isValid(phone: mobileTF.text) {
            showBanner(message: "Invalid Mobile Number")
            return false
        }
        
        return true
    }
    
}

// Mark:- Handling text fields
extension PLLoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxMobileDigits
    }

}

// Mark:- Handling web service
extension PLLoginVC {
    private func getLoginWeb(){
        BaseThread.asyncMain {
            let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLOTPVerificationVC") as! PLOTPVerificationVC
            segueVC.mobileNumber = self.mobileTF.text.unwrap
            self.present(segueVC, animated: true, completion: nil)
        }
    }
}










