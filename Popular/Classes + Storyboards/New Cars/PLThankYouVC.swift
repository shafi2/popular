//
//  PLThankYouVC.swift
//  Popular
//
//  Created by Appzoc on 04/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLThankYouVC: UIViewController {
    
    var dismisType = 0
    
    var isFromNewCarBooking:Bool = false
    
    @IBOutlet var contactImage: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromNewCarBooking{
            DispatchQueue.main.async {
                self.contactImage.image = UIImage(named: "Group 894")
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func continueBT(_ sender: UIButton) {
        
        if dismisType == 0
        {
            if let parentVC = self.presentingViewController?.presentingViewController as? PLReferalHome {
                parentVC.reloadVCFromPopUps()
            }

            presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        else
        {
            if let parentVC = self.presentingViewController?.presentingViewController?.presentingViewController as? PLReferalHome {
                parentVC.reloadVCFromPopUps()
            }

             presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        

        
        
    }
    
}
