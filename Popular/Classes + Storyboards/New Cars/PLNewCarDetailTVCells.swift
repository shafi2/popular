//
//  PLNewCarDetailTVCells.swift
//  Popular
//
//  Created by admin on 06/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLNewCarDetailHeaderCell:UITableViewCell {
    @IBOutlet weak var titileLBL: UILabel!
    @IBOutlet weak var arrowIMGV: UIImageView!
    
}

class PLSpecCVC : UICollectionViewCell {
    @IBOutlet weak var nameLBL: UILabel!
    
}

class PLNewCarDetailColorCVC : UICollectionViewCell {
    
    @IBOutlet weak var colorView: BaseView!
    
}

class PLNewCarDetailVariantCVC : UICollectionViewCell {
    @IBOutlet weak var selectedIMG: UIImageView!
    @IBOutlet weak var brandLBL: UILabel!
    @IBOutlet weak var modelLBL: UILabel!
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var priceLBL: UILabel!
    
    @IBOutlet weak var firstDescriptionLBL: UILabel!
    @IBOutlet weak var secondDescriptionLBL: UILabel!
    @IBOutlet weak var thirdDescriptionLBL: UILabel!
    @IBOutlet weak var fourthDescriptionLBL: UILabel!
    
    @IBOutlet weak var firstEnableIMG: UIImageView!
    @IBOutlet weak var secondEnableIMG: UIImageView!
    @IBOutlet weak var thirdEnableIMG: UIImageView!
    @IBOutlet weak var fourthEnableIMG: UIImageView!
}

class PLNewCarDetailImageCVC : UICollectionViewCell {
    
    @IBOutlet weak var carIMG: UIImageView!
    
}

class PLNewCarDetailImageMoreCVC : UICollectionViewCell {
    
    @IBOutlet var bagView: BaseView!
    @IBOutlet var transPview: BaseView!
    @IBOutlet weak var countLBL: UILabel!
    @IBOutlet weak var carIMG: UIImageView!
    
}

class PLNewCarDetailVideoCVC : UICollectionViewCell {
    
    @IBOutlet weak var carIMG: UIImageView!
    
}

class PLNewCarDetailVideoMoreCVC : UICollectionViewCell {
    
    @IBOutlet var transPview: BaseView!
    @IBOutlet weak var countLBL: UILabel!
    @IBOutlet weak var carIMG: UIImageView!
    @IBOutlet var bagView: BaseView!
    
}


class PLNewCarDetailCheckTVC: UITableViewCell {
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var verifiedIMG: UIImageView!
    
}

class PLNewCarDetailDataTVC: UITableViewCell {
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var valueLBL: UILabel!
    @IBOutlet var imageTick: UIImageView!
    
    func configureFoeSingleSetWith(data:NewCarVariantData.VariantDetail.Features.ParentData){
        if let key = data.key, let value = data.value{
            descriptionLBL.text = key
            if value == "Yes"{
                valueLBL.text = ""
                imageTick.isHidden = false
            }else{
                valueLBL.text = value
                imageTick.isHidden = true
            }
        }
    }
    
    func configureWith(data:NewCarVariantData.VariantDetail.Features.ParentData.Details){
        descriptionLBL.text = data.key
        if data.value == "Yes"{
            valueLBL.text = ""
            imageTick.isHidden = false
        }else{
            valueLBL.text = data.value
            imageTick.isHidden = true
        }
    }
}

class PLNewCarDetailSwitchTVC: UITableViewCell {
    @IBOutlet weak var frstToggleView: BaseView!
    @IBOutlet weak var firstToggleLBL: UILabel!
    @IBOutlet weak var firstToggleBTN: UIButton!
    @IBOutlet weak var secondToggleView: BaseView!
    @IBOutlet weak var secondToggleLBL: UILabel!
    @IBOutlet weak var secondToggleBTN: UIButton!
    
    func configureWith(data:[NewCarVariantData.VariantDetail.Features.ParentData], parent:NewCarSelectedParent, index:IndexPath){
        firstToggleLBL.text = data[0].name ?? ""
        secondToggleLBL.text = data[1].name ?? ""
        firstToggleBTN.tag = index.section
        secondToggleBTN.tag = index.section
        
        if parent.selectedParent == 0{
           frstToggleView.backgroundColor = UIColor.BlueColor()
           firstToggleLBL.textColor = UIColor.white
           secondToggleView.backgroundColor = UIColor.clear
           secondToggleLBL.textColor = UIColor.DarkColor()
        }else{
            secondToggleView.backgroundColor = UIColor.BlueColor()
            secondToggleLBL.textColor = UIColor.white
            frstToggleView.backgroundColor = UIColor.clear
            firstToggleLBL.textColor = UIColor.DarkColor()
        }
    }
}

