//
//  PLEnquiryPopUpVC.swift
//  Popular
//
//  Created by Appzoc on 03/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLEnquiryPopUpVC: UIViewController,SideOptionsDelegate, DateTimeDelegate {

    var popUPType = ""
    var typeID = -1
    var locationType = 1 // 1 for NewCar
    var isNexa:Bool = false
    
    final var carModelId: Int = 0
    
    var locArry:[LocationModel] = []
    
    final var isFromUsedCarDetail: Bool = false
    
    @IBOutlet var popUpTypeLB: UILabel!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var mobilenumberTF: UITextField!
    @IBOutlet var locationTF: UITextField!
    
    private var callBackDate: String = ""
    private var callBackStartTime: String = ""
    private var callBackEndTime: String = ""

    var postParaMeters = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popUpTypeLB.text = popUPType
        self.mobilenumberTF.text = PLAppState.session.userMobileNumber
        self.nameTF.text = PLAppState.session.userName
        getLocation()

        
        postParaMeters["userid"] = PLAppState.session.userID
        postParaMeters["name"] = PLAppState.session.userName
        postParaMeters["type"] = typeID
        
//        postParaMeters["call_date"] = "18-8-208"
//        postParaMeters["from_call_time"] = "11:45 AM"
//        postParaMeters["to_call_time"] = "11:45 PM"
        postParaMeters["carmodelid"] = carModelId
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //http://app.appzoc.com/popular/backend/api/getBranches

    @IBAction func dismisPopUpBT(_ sender: UIButton) {
        
        self.didMissVC()
    }
    
    @IBAction func selectLocationBT(_ sender: UIButton) {
        showSideOption(presentOn: self, delegate: self, source: self.locArry.map({PLSideOptionsModel(sourceName: $0.name ?? "", sourceID: $0.id ?? 0)}))
    }
    
    @IBAction func callBackTapped(_ sender: UIButton) {
        showDateTimePopUp(presentOn: self, delegate: self)
    }
    
    //Mark:- delegate from the date and
    func selectedDateAndTime(dateString: String, startTimeString: String, endTimeString: String) {
        print(startTimeString,"DateStringNewCarrrrrrrrr",dateString,"End",endTimeString)
        callBackDate = dateString
        callBackStartTime = startTimeString
        callBackEndTime = endTimeString

    }
    
    //Mark:- delegate from side menu options
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
        //print(data.id,data.name)
        self.locationTF.text = data.name
        
        postParaMeters["locationid"] = data.id
        
    }

    
    
    @IBAction func submitBT(_ sender: UIButton) {
        
        
        if nameTF.text?.count != 0
        {
            if mobilenumberTF.text?.count != 0
            {
                if locationTF.text?.count != 0
                {
                    postEnq()
                }
                else
                {
                  showBanner(message: "Select Location ")
                }
            }
            else
            {
              showBanner(message: "Enter Mobile Number ")
            }
        }
        else
        {
           showBanner(message: " Enter Name ")
        }
        
        
    }
    
    func parseLocationData(data:[[String:Any]]){
        let decoder = JSONDecoder()
        if let trueData = serializeJSON(json: data){
            do{
                let parsedData = try decoder.decode([LocationModel].self, from: trueData)
                self.locArry = parsedData
            }catch{
                print(error)
            }
        }
    }
    
    
    func getLocation()
    {
        if isNexa{
                BaseThread.asyncMain {
                    WebServices.getMethodWith(url:"get/locations?type=nexa" ,
                        parameter: ["":""],
                        CompletionHandler:
                        { (isFetched, result) in
                            if isFetched
                            {
                                if let dataSection = result["data"] as? [[String:Any]]{
                                    self.parseLocationData(data: dataSection)
                                }
                            }
                            else
                            {
                            }
                    })
                }
        }else{
            BaseThread.asyncMain {
                WebServices.getMethodWith(url:"get/locations?type=\(self.locationType)" ,
                                          parameter: ["":""],
                                                      CompletionHandler:
                    { (isFetched, result) in
                        if isFetched
                        {
                            if let dataSection = result["data"] as? [[String:Any]]{
                                self.parseLocationData(data: dataSection)
                            }
                        }
                        else
                        {
                        }
                })
            }
        }
    }
    
}
extension PLEnquiryPopUpVC
{
    
    func postEnq()
    {
        
        if callBackDate.isNotEmpty && callBackStartTime.isNotEmpty && callBackEndTime.isNotEmpty {
            postParaMeters["call_date"] = callBackDate
            postParaMeters["from_call_time"] = callBackStartTime
            postParaMeters["to_call_time"] = callBackEndTime
        }

        postParaMeters["userid"] = PLAppState.session.userID
        postParaMeters["name"] = nameTF.text
        postParaMeters["type"] = typeID
        postParaMeters["mobile_number"] = mobilenumberTF.text
        
        
        //        postParaMeters["call_date"] = "18-8-208"
        //        postParaMeters["from_call_time"] = "11:45 AM"
        //        postParaMeters["to_call_time"] = "11:45 PM"
        postParaMeters["carmodelid"] = carModelId


        WebServices.postMethod(url:"carmodelEnquiry" ,
                               parameter: self.postParaMeters,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    //self.didMissVC()
                    BaseThread.asyncMain {
                        
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        nextVC.isFromNewCarBooking = true
                        if self.typeID == -2
                        {
                            nextVC.dismisType = 1
                        }
                        else
                        {
                            nextVC.dismisType = 0
                        }
                        self.present(nextVC, animated: true, completion: {
                            DispatchQueue.main.async {
                                UIView.animate(withDuration: 0.3, animations: {
                                    self.view.alpha = 0
                                })
                            }
                        })
                    }
                }
                else
                {
                    
                    
                    
                }
        })
        
    }
    
    
    
    func didMissVC()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     id: 56,
     name: "Thrissur",
     type: 1,
     email_ids: "a@gmail.com",
     phone_nos: "2",
     nexa: 1,
     created_at: "2018-09-21 11:12:34",
     updated_at: "2018-09-20 04:55:19"
 
 
 */
    
//    struct LocationModel: Codable{
//        let id:Int = 0
//        let name:String = ""
//        let type:Int = 0
//        let email_ids:String = ""
//        let phone_nos:String = ""
//        let nexa:Int = 0
//
    struct LocationModel: Codable{
        let id:Int?
        let name:String?
        let type:Int?
        let email_ids:String?
        let phone_nos:String?
        let nexa:Int?
    }
}

