//
//  PLBookNowVC.swift
//  Popular
//
//  Created by Appzoc on 04/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLBookNowVC: UIViewController {
    
    var NewCarId = "0"
    var Variantid = "0"
    
    var postParaMeters = [String:Any]()

    @IBOutlet var priceLB: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postParaMeters["userid"] = PLAppState.session.userID
        postParaMeters["newcarid"] = NewCarId
        postParaMeters["variantid"] = Variantid
        getBookingprice()
        
        
        // Do any additional setup after loading the view.
        
        //bookingnow
    }
    
     override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissBT(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func continueBT(_ sender: UIButton) {
        
        let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLEnquiryPopUpVC") as! PLEnquiryPopUpVC
        
        nextVC.popUPType = "Booking Enquiry"
        nextVC.typeID = -2
        
        self.present(nextVC, animated: true, completion: nil)
    }

    
    func getBookingprice()
    {
        
        
        BaseThread.asyncMain {
            
            
            
            WebServices.postMethod(url:"bookingnow" ,
                                   parameter: self.postParaMeters,
                                   CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        let da = result["data"] as! [String:Any]
                        self.priceLB.text = da["price"] as! String + "/-"
                    }
                    else
                    {
                        
                        showBanner(message: "Server Error. Please try after sometime.")
                        
                    }
            })
            
            
            
        }
        
    }

}
