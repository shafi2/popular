//
//  NewCarModel.swift
//  Popular
//
//  Created by Appzoc on 28/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation



class NewCarListDataModel {
    var carListData = [newCarListData]()
    var favouritecount = -1
    var banner = [String]()
    
    func getNewCarList(dataDict:[String:Any]) -> NewCarListDataModel {
        
        let data = dataDict["data"] as? [[String:Any]] ?? []
        
        favouritecount = dataDict["favouritecount"] as? Int ?? -1
        
        banner = dataDict["banner"] as? [String] ?? ["No data"]
        
        
        
        for temp in data {
            
            let dataTemp = newCarListData()
            dataTemp.isnexa = temp["nexa"] as? String ?? "" //?? "No data"
            

            dataTemp.price = temp["price"] as? String ?? "" //?? "No data"
            dataTemp.id = temp["id"] as? Int ?? 0 //?? ""
            dataTemp.name = temp["name"] as? String ?? "" //?? ""
            dataTemp.price = temp["price"] as? String ?? "" //?? ""
            dataTemp.mileage = temp["mileage"] as? String ?? "" //?? ""
            dataTemp.fuel_type = temp["fuel_type"] as? String ?? "" //?? ""
            dataTemp.transmission = temp["transmission"] as? String ?? "" //?? ""
            dataTemp.engine = temp["engine"] as? String ?? "" //?? ""
            dataTemp.status = temp["status"] as? Int ?? 0
            dataTemp.isFav = temp["favorite"] as? Int ?? 0
            
            //color with code
            let ban = temp["colors"] as? [[String:Any]] ?? []
            for items in ban
            {
                let idNameTemp = colorWithCode(dataDic: items)
                dataTemp.colorImageas.append(idNameTemp)
            }
            
            //gallery images
            print(temp["images"].debugDescription)
            let ban1 = temp["images"] as? [[String:Any]] ?? []
            for items in ban1
            {
                let idNameTemp = galImages(dataDic: items)
                dataTemp.GalImages.append(idNameTemp)
            }
            
            //videos
            let ban2 = temp["videos"] as? [[String:Any]] ?? []
            for items in ban2
            {
                
                dataTemp.videos.append(items["path"] as? String ?? "")
            }
            
            let ban3 = temp["variants"] as? [[String:Any]] ?? []
            for items in ban3
            {
                let idNameTemp = variants(dataDic: items)
                dataTemp.varients.append(idNameTemp)
            }
            
            
            carListData.append(dataTemp)
        }
        
        
        
        
        return self
    }
    
    
}

class newCarListData {
    
    var id = -1
    var name = " "
    var isnexa = " "
    
    var images = " "


    var imageCount = -1
    var isfavourite = false
    var price = " "
    var mileage = " "
    var fuel_type = " "
    var transmission = " "
    var engine = " "
    var status = -1
    var FirstImage = ""
    var colorImageas = [colorWithCode]()
    var videos = [String]()
    var GalImages = [galImages]()
    var varients = [variants]()
    var isFav = -1
}

class colorWithCode
{
    
    var code = ""
    var url = ""
    
    init(dataDic:[String:Any]) {
        self.code = dataDic["color_code"] as? String ?? ""
        self.url = dataDic["img"] as? String ?? ""
    }
}

class galImages
{
    
    var id = -1
    var model_id = -1
    var path = ""
    var part = 0
    
    
    init(dataDic:[String:Any]) {
        self.id = dataDic["id"] as? Int ?? 0
        self.model_id = dataDic["model_id"] as? Int ?? 0
        self.path = dataDic["path"] as? String ?? ""
        self.part = dataDic["part"] as? Int ?? 0
    }
}

class variants
{
    var id = -1
    var tittle = ""
    var desc = ""
    var price = ""
    var stat = -1
    var key1 = [String]()
    
    var keyFor1 = ""
    var keyFor2 = ""
    var keyFor3 = ""
    var keyFor4 = ""

    
    init(dataDic:[String:Any]) {
        
        
        self.id = dataDic["id"] as? Int ?? 0
        self.tittle = dataDic["name"] as? String ?? ""
        self.desc = dataDic["description"] as? String ?? ""
        self.price = dataDic["price"] as? String ?? ""
        self.stat = dataDic["status"] as? Int ?? 0
        
        self.keyFor1 = dataDic["highlight_0"] as? String ?? ""
        self.keyFor2 = dataDic["highlight_1"] as? String ?? ""
        self.keyFor3 = dataDic["highlight_2"] as? String ?? ""
        self.keyFor4 = dataDic["highlight_3"] as? String ?? ""
        
        let bb = dataDic["highlight"] as? [[String:Any]] ?? []
        
        
        for items in bb
        {
     
            self.key1.append(items["values"] as? String ?? "")
            
           
        }
        
        
    }
}






/// Varient Details




class varientDetails
{
    
    var id = 1
    var model_id = -1
    var name = ""
    var description = ""
    var price = ""
    var status = -1
    var featur = [features]()
    var metallic = ""
    var non_metallic = ""
    
    
    init(dataDic:[String:Any]) {
        self.id = dataDic["id"] as? Int ?? 0
        self.model_id = dataDic["model_id"] as? Int ?? 0
        self.name = dataDic["name"] as? String ?? ""
        self.description = dataDic["description"] as? String ?? ""
        self.price = dataDic["price"] as? String ?? ""
        self.status = dataDic["status"] as? Int ?? 0
        self.metallic = dataDic["metallic"] as? String ?? ""
        self.non_metallic = dataDic["non_metallic"] as? String ?? ""
        
        let ban3 = dataDic["features"] as? [[String:Any]] ?? []
        for items in ban3
        {
            let idNameTemp = features(dataDic: items)
            self.featur.append(idNameTemp)
        }
    
    }
}

class features
{
    
    var id = -1
    var variant_id = -1
    var name = ""
    var parentDetails = -1
    var featureDetail = [featureDetails]()
    var keyValAr = [tittleNamekey]()
    
     init(dataDic:[String:Any]) {
        
        
        self.id = dataDic["id"] as? Int ?? 0
        self.variant_id = dataDic["variant_id"] as? Int ?? 0
        self.name = dataDic["name"] as? String ?? ""
        self.parentDetails = dataDic["parent_details"] as? Int ?? 0
        //parent_details
        
        
        if self.parentDetails == 0
        {
            let ban3 = dataDic["parent"] as? [[String:Any]] ?? []
            for items in ban3
            {
                let idNameTemp = tittleNamekey(dataDic: items)
                self.keyValAr.append(idNameTemp)
            }
        }
        else
        {
            let ban3 = dataDic["parent"] as? [[String:Any]] ?? []
            for items in ban3
            {
                let idNameTemp = featureDetails(dataDic: items)
                self.featureDetail.append(idNameTemp)
            }
        }
        
        
        
        
    }
    
    
}

class featureDetails
{
    var id = -1 
    var feature_id = -1
    var name = " "
    var allDetails = [tittleNamekey]()


    
    init(dataDic:[String:Any]) {
        
        self.id = dataDic["id"] as? Int ?? 0
        self.feature_id = dataDic["feature_id"] as? Int ?? 0
        self.name = dataDic["name"] as? String ?? ""
        
        let ban3 = dataDic["details"] as? [[String:Any]] ?? [[String:Any]]()
        for items in ban3
        {
            let idNameTemp = tittleNamekey(dataDic: items)
            self.allDetails.append(idNameTemp)
        }
    }
}



class tittleNamekey
{

    var key = ""
    var val = ""
    
    init(dataDic:[String:Any]) {
        
        
        self.key = dataDic["kei"] as? String ?? ""
        self.val = dataDic["values"] as? String ?? ""

    }
}



extension PLNewCarDetailVC
{
    func getVarientDummy(idFor:Int)
    {
        //getMethodWithOutActivity
        WebServices.getMethodWith(url:"get/new_cars?variant_id=\(idFor)", parameter: ["":""] ,
                                  CompletionHandler:
            { (isFetched, result) in

                //VarientSource = [varientDetails]()

                guard let dt = result["data"] as? [String:Any] else { return }
                
                guard let vari = dt["variant_details"] as? [[String:Any]] else { return }
                
                
                VarientSource.removeAll()
                
                for items in vari
                {

                    let idNameTemp = varientDetails(dataDic: items)
                    VarientSource.append(idNameTemp)

                }
                
                 BaseThread.asyncMain {
                    self.setDataForSpecifications()
                    self.specTBL.reloadData()
                }
                
                



                //self.SaveToDatafrom(dSour:VarientSource,vid:VarientSource[0].id)

                //let d = v1[0].featur[1].featureDetail[0].allDetails[1].val

//                for dd in d
//                {
//                  print(dd)
//                }



        })

    }
    
    

    
  
}


extension PLNewCarDetailVC
{
    func getVarient(idFor:Int)
    {
        
        if self.ifdatainDb(idFor: idFor)
        {

            
            
            do{
                
                let tasks1 = try context.fetch(Varients.fetchRequest())
                
                
                
                
                if  let dd = tasks1[1] as? Varients{
//                    for i in tasks1
//                    {
//
//                        let o = i as! Varients
//
//                        print(o.vid)
//                    }
                
                    _ = dd.vid
                    let ddata = dd.varient
                    if let _ = ddata{
                        let jj = try JSONSerialization.jsonObject(with: ddata!, options: []) as? [String:Any]
                    
                        if jj != nil {
                            let temp = varientDetails(dataDic: jj!)
                            
                            VarientSource.append(temp)

                        }else {
                            print("parsing error-newcar model")
                        }
                    }
                }
            }
            catch
            {
                print(error)
            }

            
        }
        else
        {
            
            WebServices.getMethodWithOutActivity(url:"get/new_cars?variant_id=\(idFor)", parameter: ["":""] ,
                                                 CompletionHandler:
                { (isFetched, result) in
                    
                    //VarientSource = [varientDetails]()
                    
                    guard let dt = result["data"] as? [String:Any] else { return }
                    guard let vari = dt["variant_details"] as? [[String:Any]] else { return }
                    
                    for items in vari
                    {
                        
                        let idNameTemp = varientDetails(dataDic: items)
                        VarientSource.append(idNameTemp)
                        
                    }
                    self.SaveToDatafrom(dSour:vari[0],vid:VarientSource[0].id)
                    
                    
                    
            })
            
        }
        

        
    }
    
    
    func SaveToDatafrom(dSour:[String:Any],vid:Int)
    {
        
        
        
        do{
            
            let jsonData = try JSONSerialization.data(withJSONObject: dSour, options: .prettyPrinted)

            let variet =  Varients(context: self.context)

            
            
            variet.vid = Int16(vid)
            variet.varient = jsonData
            
            
            
            try self.context.save()
            
            //self.setDataFrom()
            
        }catch{
            
            print(error)
            
        }
        
        
        
        
    }
    
    
    func ifdatainDb(idFor:Int) -> Bool
    {
    
        
        do{
            
            let tasks1 = try context.fetch(Varients.fetchRequest())
            
            
            let cc = tasks1.count
            
            if cc > 0
            {
                
                
                for ii in tasks1
                {
                    if let dd = ii as? Varients{
                        print(dd.vid)
                        
                        if dd.vid == idFor
                        {
                            return true
                        }
                    }
                   return false
                }
                

                
                
            }
            else
            {
                return false
            }
            
            
            
            
        }
        catch
        {
            print(error)
            return false
        }

        
  return false
        
    }
    
}

