//
//  PLNewCarDetailVC.swift
//  Popular
//
//  Created by admin on 05/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher
import AVFoundation
import CoreData

var VarientSource = [varientDetails]()

class PLNewCarDetailVC: UIViewController {
    
     var iArrayIndex = 0
    
    var ii = 0
    
    var ff = 0
    
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet var favImageIV: UIImageView!
    
    ///Data from frirst View
    
    var ImageString = ""
    var CarName = ""
    var carPrice = ""
    var isNex = " "
    var ind = -1
    var isFav = -1
    
/////////////////////////////
    
    @IBOutlet weak var specListCLN: UICollectionView!
    @IBOutlet weak var colorCLN: UICollectionView!
    @IBOutlet weak var variantCLN: UICollectionView!
    @IBOutlet weak var imageCLN: UICollectionView!
    @IBOutlet weak var videoCLN: UICollectionView!
    
    @IBOutlet var galleryEmptyIndicatorLBL: UILabel!
    @IBOutlet var videoEmptyIndicatorLBL: UILabel!
    
    
    
    @IBOutlet weak var imageHeaderView: BaseView!
    @IBOutlet weak var colorView: BaseView!
    @IBOutlet weak var specificationView: BaseView!
    @IBOutlet weak var variantView: BaseView!
    @IBOutlet weak var gallaryView: BaseView!
    @IBOutlet weak var vehiclePriceView: BaseView!
    
    
    
    @IBOutlet weak var TestDriveView: UIView!
    
    @IBOutlet weak var specTBL: UITableView!
    
    
    @IBOutlet weak var varaintLbl: UILabel!
    @IBOutlet weak var modelLbl: UILabel!
    @IBOutlet weak var modelAnchoView: UIView!
    @IBOutlet weak var varaintAnchorView: UIView!
    @IBOutlet weak var TestDriveBubbleView: UIView!
    @IBOutlet weak var variantDropDownArrowIMG: UIImageView!
    @IBOutlet weak var modelDropDownArrowIMG: UIImageView!
    
    
    @IBOutlet weak var titleCarLBL: UILabel!
    @IBOutlet weak var favCountLBL: BaseLabel!
    @IBOutlet weak var favBTN: BaseButton!
    
    @IBOutlet weak var priceLBL: UILabel!
    @IBOutlet weak var carNameLBL: UILabel!
    
    @IBOutlet weak var carIMG: UIImageView!
    @IBOutlet weak var mileageLBL: UILabel!
    @IBOutlet weak var fuelTypeLBL: UILabel!
    @IBOutlet weak var transmissionTypeLBL: UILabel!
    @IBOutlet weak var engineTypeLBL: UILabel!
    
    
    @IBOutlet weak var poweredbyView: UIView!
    
    @IBOutlet weak var PriceOfLBL: UILabel!
    @IBOutlet weak var mettalicPriceLBL: UILabel!
    @IBOutlet weak var nonMetallicPrice: UILabel!
    @IBOutlet weak var gallaryImageLBL: UILabel!
    @IBOutlet weak var gallaryVideoLBL: UILabel!
    
    @IBOutlet weak var tableFooterContainerView: UIView!
    
    //MARK:- Stored Properties  
    var newCarID = Int()
    var isHidden = [false,false,false,false,false, false]
    var storedOffsets = [Int: CGFloat]()
    
    var allKeys0 = [Int: [String]]()
    var allValues0 = [Int: [String]]()
    
    var allKeys1 = [Int: [String]]()
    var allValues1 = [Int: [String]]()
    
    
    var count = [0:6,1:4,2:3,3:5,4:3,5:1]
    
    var specClnDatasource = ["Overview", "Car Models","Specifications & Features", "Check Vehicle Price", "Gallery"]
    var sectionHeaderDataSource = ["Performance", "Suspension & Brakes", "Diemension & Capacity", "Weight", "Saftey", "Exterior"]
    
    var tableDataSource = [String:Any]()
    var perfom = ["Engine Type":"engine_type"]
    var selectedSpecCVC = 0
    var selectedColorCVC = 0
    var selectedVariantCVC = 0
    var selectedImageCVC = 0
    var selectedVideoCVC = 0
   
    var selectedVarinatType = ""
    var selectedToggle = [Int:Any]()
    var parameterNewCarDetail = [String:Any]()
    var detailDataModel = PLNewcarsDetailModel()
    var variantData = PLNewcarsDetailModel.Variant()
    var variantList = [Int:String]()
    
    var performanceKeys = [String]()
    var performanceVals = [String]()
    
    var suspensionKeys = [String]()
    var suspensionVal = [String]()
    
    var brakesKeys = [String]()
    var brakesVal = [String]()
    
    var shouldCheckScroll:Bool = true
    
    
    var dimensionKeys = [String]()
    var capacityKeys = [String]()
    var weightKeys = [String]()
    var safetyKeys = [String]()
    var featureKeys = [String]()
    var featureArray = [[String:String]]()
    var gallaryImgArray = [String]()
    var gallaryVideoArray = [String]()
    
    var collectionViewUIEnableFlag:Bool = true //false
    
    fileprivate var initialVehiclePriceViewY: CGFloat = 0//1647
    fileprivate var initialGallaryViewY: CGFloat = 0//2238
    fileprivate var vehiclePriceViewRect = CGRect()
    fileprivate var vehicleGalleryViewRect = CGRect()
    
    //MARK: New Data Models Holders
    var varientData:NewCarVariantData = NewCarVariantData()
    var selectedParent:[NewCarSelectedParent] = []
    
    //MARK:- View Life Cycle

    override func viewWillAppear(_ animated: Bool) {
     
        setUpInterface()
        
      //setDataForSpecifications()
        
    }
    
    func populateParentList(){
        self.selectedParent.removeAll()
        for item in varientData.variantDetail{
            for _ in item.features{
                let parent = NewCarSelectedParent(selectedParent: 0, sectionSelected: false)
                self.selectedParent.append(parent)
            }
        }
    }
    
    func setUpInterface(){
        tableDataSource["Performance"] = [perfom]
        //self.getNewCarsDetail()
        
      //  print("Will apper  ")
        
        let imageURLF = imageURL + ImageString
        
        self.carIMG.kf.setImage(with: URL(string:imageURLF), options: [.requestModifier(getImageAuthorization())])
        
        
        
        self.carIMG.contentMode = UIViewContentMode.scaleAspectFill
        
        self.priceLBL.text = "Ex Showroom Price ₹ " + carPrice
        self.carNameLBL.text = CarName
        
        if dataSource.carListData.count > ind{
            isFav = dataSource.carListData[ind].isFav
        }else{
            isFav = 0
        }
        
        
        if isNex == "0"
        {
            poweredbyView.isHidden = true
        }
        else if isNex == "1"
        {
            poweredbyView.isHidden = false
        }
        else
        {
            poweredbyView.isHidden = true
        }
        
        if isFav == 0
        {
            self.favImageIV.image = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline.png")
        }
        else
        {
            self.favImageIV.image = #imageLiteral(resourceName: "Liked.png")
        }
        
        //print(CarName,carPrice,isNex)
        
        if dataSource.carListData.count > ind{
            self.mileageLBL.text = dataSource.carListData[ind].mileage
            self.fuelTypeLBL.text = dataSource.carListData[ind].fuel_type
            self.transmissionTypeLBL.text = dataSource.carListData[ind].transmission
            self.engineTypeLBL.text = dataSource.carListData[ind].engine
            
            self.titleCarLBL.text = CarName
            
            self.gallaryVideoLBL.text = "Videos (\(dataSource.carListData[ind].videos.count.description)) "
            
            self.gallaryImageLBL.text = "Images (\(dataSource.carListData[ind].GalImages.count.description))"
        
        
        }
        
       // self.favCountLBL.text = "0"
        
        
        let id = PLAppState.session.userID
        if id != 0
        {
            getFavCount(uidFor:id)
        }
        
        if dataSource.carListData.count > ind,dataSource.carListData[ind].varients.count != 0
        {
            if let variantData = CoreDataManager.sharedManager.getcarEventForId(id: Int64(dataSource.carListData[ind].varients[0].id)){
                getDataForVariantFromDataBase(variantData: variantData)
                if let detail = variantData.details{
                    print("Details **& : ",detail)
                }
            }else{
                getDataForVariant(id: dataSource.carListData[ind].varients[0].id)
            }
            //getDataForVariant(id: dataSource.carListData[ind].varients[0].id)
           // self.getVarientDummy(idFor:dataSource.carListData[ind].varients[0].id)
        }
        
    }
    
    //MARK: UIEnbale Function
    func enableCollectionViewUI(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.collectionViewUIEnableFlag = true
        }
    }
    
    func getDataForVariant(id:Int){
        WebServices.getMethodWith(url: "get/new_cars?variant_id=\(id)", parameter: nil) { (isComplete, json) in
            if isComplete{
                do{
                    if let trueJSON = json["data"] as? [String:Any]{
                        print("TrueJson ",trueJSON)
                        if let trueData = serializeJSON(json: trueJSON){
                            let decoder = JSONDecoder()
                            let parsedData = try decoder.decode(NewCarVariantData.self, from: trueData)
                            self.varientData = parsedData
                            DispatchQueue.main.async {
                                self.mettalicPriceLBL.text = ": ₹ " + (parsedData.variantDetail.first?.metallicPrice ?? "0")
                                self.nonMetallicPrice.text = ": ₹ " + (parsedData.variantDetail.first?.nonMetallicPrice ?? "0")
                            }
                            self.populateParentList()
                            self.specTBL.reloadData()
                            if let _ = parsedData.variantDetail.first{
                                do{
                                    let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                                    let variantStore = PPCarVarient(id: Int64(parsedData.variantDetail.first!.id), details: jsonData)
                                    CoreDataManager.sharedManager.inserCarVarient(carVarient: variantStore)
                                }catch{
                                    print(error)
                                }
                            }
                           // print("Suspension Values ", parsedData.variantDetail.first?.features.first?.parent.first?.details?.first?.key)
                        }
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func convertStringToJSON(stringData:String) -> [String:Any]?{
        do{
            let formattedString = stringData.data(using: .utf8)
            
            if let data = formattedString{
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    print("Done ^^*")
                    return jsonObject as? [String:Any]
            }
        }catch{
            print(error)
        }
        return nil
    }
    
    //MARK: Set From DB
    func getDataForVariantFromDataBase(variantData:PPCarVarient){
        do{
            print("Actual ** ",variantData.details!)
            if let _ = variantData.details{
                let json = try JSONSerialization.jsonObject(with: variantData.details!, options: []) as? [String:Any] ?? [:]
                if let trueJSON = json["data"] as? [String:Any]{
                    print("TrueJson ",trueJSON)
                    if let trueData = serializeJSON(json: trueJSON){
                        let decoder = JSONDecoder()
                        let parsedData = try decoder.decode(NewCarVariantData.self, from: trueData)
                        self.varientData = parsedData
                        DispatchQueue.main.async {
                            self.mettalicPriceLBL.text = ": ₹ " + (parsedData.variantDetail.first?.metallicPrice ?? "0")
                            self.nonMetallicPrice.text = ": ₹ " + (parsedData.variantDetail.first?.nonMetallicPrice ?? "0")
                        }
                        self.populateParentList()
                        self.specTBL.reloadData()
                        // print("Suspension Values ", parsedData.variantDetail.first?.features.first?.parent.first?.details?.first?.key)
                    }
                }
            }
        }catch{
            print(error)
        }
    }
    
    
    func setDataForSpecifications()
    {
  
    // Setting Tittle
        
        //
        
        
        sectionHeaderDataSource.removeAll()

        if VarientSource.count != 0
        {
            self.isHidden.removeAll()
            for i in VarientSource[0].featur
            {
                sectionHeaderDataSource.append(i.name)
                self.isHidden.append(true)
                
            }
            
            self.mettalicPriceLBL.text = ": ₹ " + VarientSource[0].metallic
            self.nonMetallicPrice.text = ": ₹ " + VarientSource[0].non_metallic
            
            
            for ddatafrom in VarientSource[0].featur
            {
                
                if ddatafrom.parentDetails == 0
                {
                    
                    
                    if ddatafrom.keyValAr.count != 0
                    {
                        
                        self.performanceKeys.removeAll()
                        self.performanceVals.removeAll()
                        for i in VarientSource[0].featur[0].keyValAr
                        {
                            self.performanceKeys.append(i.key)
                            self.performanceVals.append(i.val)
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                }
                else
                {
                    
                    
                    if ddatafrom.featureDetail.count != 0
                    {
                        self.suspensionKeys.removeAll()
                        self.suspensionVal.removeAll()
                        self.brakesKeys.removeAll()
                        self.brakesVal.removeAll()
                        
                        
                        
                        for i in ddatafrom.featureDetail[0].allDetails
                        {
  
                            
                            self.suspensionKeys.append(i.key)
                            self.suspensionVal.append(i.val)
                            
                            self.allKeys0[iArrayIndex] = self.suspensionKeys
                            self.allValues0[iArrayIndex] = self.suspensionVal
                            
                            
                        }
                        
                        for i in ddatafrom.featureDetail[1].allDetails
                        {
                            
                            
                            self.brakesKeys.append(i.key)
                            self.brakesVal.append(i.val)
                            self.allKeys1[iArrayIndex] = self.brakesKeys
                            self.allValues1[iArrayIndex] = self.brakesVal
                        }
                        
                        
//                        self.allKeys0[iArrayIndex] = self.suspensionKeys
//                        self.allValues0[iArrayIndex] = self.suspensionVal
//
//                        self.allKeys1[iArrayIndex] = self.brakesKeys
//                        self.allValues1[iArrayIndex] = self.brakesVal
                        
                       
                    }
                    
                    
                    
                    
                }
                
                iArrayIndex = iArrayIndex + 1
            }
            
            
            
            
            
            
            
            
 /*
            
          
        if VarientSource[0].featur.count != 0
        {
            
            if VarientSource[0].featur[0].parentDetails == 0
            {
                
                
                self.performanceKeys.removeAll()
                self.performanceVals.removeAll()
                
           
                
                
                for i in VarientSource[0].featur[0].keyValAr
                {
                    self.performanceKeys.append(i.key)
                    self.performanceVals.append(i.val)

                }
                
            }
    
                
                if VarientSource[0].featur[0].featureDetail.count != 0
                {
                    self.suspensionKeys.removeAll()
                    self.suspensionVal.removeAll()
                    self.brakesKeys.removeAll()
                    self.brakesVal.removeAll()
                    
                    for i in VarientSource[0].featur[0].featureDetail[0].allDetails
                    {
                        
                        
                        self.suspensionKeys.append(i.key)
                        self.suspensionVal.append(i.val)
                    }
                    
                    for i in VarientSource[0].featur[0].featureDetail[1].allDetails
                    {
                        
                        
                        self.brakesKeys.append(i.key)
                        self.brakesVal.append(i.val)
                    }
                }

                
      
        }
            
    */
            
            
        }
        else
        {
              if isNetworkAvailabel() {
            
                self.getVarientDummy(idFor:dataSource.carListData[ind].varients[0].id)
            }
            else
              {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        
        
        
        ii = 0
 
        print(self.suspensionKeys)
        print(self.suspensionVal)
        print(self.brakesKeys)
        print(self.brakesVal)
        
        
    }
    
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.setNeedsDisplay()
        self.view.layoutIfNeeded()
//        initialVehiclePriceViewY = vehiclePriceView.frame.origin.y
//        initialGallaryViewY = gallaryView.frame.origin.y
       // vehiclePriceViewRect = vehiclePriceView.frame
        vehicleGalleryViewRect = gallaryView.frame

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        specTBL.dataSource = self
        specTBL.delegate = self
        specListCLN.dataSource = self
        specListCLN.delegate = self
        colorCLN.dataSource = self
        colorCLN.delegate = self
        variantCLN.dataSource = self
        variantCLN.delegate = self
        imageCLN.dataSource = self
        imageCLN.delegate = self
        videoCLN.dataSource = self
        videoCLN.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(uerLoggedIn), name: .loggedIn, object: nil)
        
        specTBL.sectionFooterHeight = 0.0
        TestDriveBubbleView.isHidden = true
        
        sectionHeaderDataSource.removeAll()
        preLoadCarImages()
        
//        setUpInterface()
        
       // enableCollectionViewUI()
        
        
        //self.getVarient(idFor: dataSource.carListData[ind].id)
        
        
        //cell.carIMG.kf.setImage(with: URL(string: gallaryImgArray[indexPath.item]))
        
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dropdownClick))
//        modelLbl.isUserInteractionEnabled = true
//        modelLbl.addGestureRecognizer(tap)
        
        
        //let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.dropdownClick2))
        //varaintLbl.isUserInteractionEnabled = true
        
        //varaintLbl.addGestureRecognizer(tap2)
        
        
       
        
        
        // Do any additional setup after loading the view.
    }
    
    func preLoadCarImages(){
        let tempCarImageView: UIImageView = UIImageView()
        //dataSource.carListData[ind].colorImageas[indexPath.row].url
        for item in dataSource.carListData[ind].colorImageas{
            let link = URL(string: baseURLImage + item.url)
            print("Color downloaded : \(item.code)")
            tempCarImageView.kf.setImage(with: link, options: [.requestModifier(getImageAuthorization())])
        }
    }

    @objc func dropdownClick(sender:UITapGestureRecognizer) {
        
        let dropDown = DropDown()
        DropDown.appearance().cellHeight = 60
        DropDown.appearance().backgroundColor = UIColor.white
        // The view to which the drop down will appear on
        dropDown.anchorView = modelAnchoView
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        dropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            dropDown.hide()
            
        }
        dropDown.show()
        
    }
    
    @objc func dropdownClick2(sender:UITapGestureRecognizer) {
        
//        UIView.animate(withDuration: 0.5, animations: {
//            self.variantDropDownArrowIMG.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/4));
//        })
        
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        dropDown.anchorView = varaintAnchorView
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = Array(variantList.values)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            //self.varaintLbl.text = item
            self.setupShowRoomPrice(type: item)
            dropDown.hide()
//            UIView.animate(withDuration: 0.5, animations: {
//                self.variantDropDownArrowIMG.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2));
//            })
        }
        dropDown.show()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Action triggered on selection
    
    
    //MARK:- Actions
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func favoriteBTNPressed(_ sender: Any) {
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                let nextVC =  storyBoardFavourites?.instantiateViewController(withIdentifier: "PLFavouritesVC") as! PLFavouritesVC
                nextVC.newCarIndex = ind
                favouritesListType = .newCars
                self.present(nextVC, animated: true, completion: nil)
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        
    }
    
    @objc func uerLoggedIn(){
       // print("user Loged in")
        
        let id = PLAppState.session.userID
        if id != 0
        {
            getFavCount(uidFor:id)
        }
        
    }
    
    @IBAction func favAddOrRemovePressed(_ sender: BaseButton) {
        
        
        
        
        if isNetworkAvailabel() {
            
            
            let id = PLAppState.session.userID
            
            if id == 0
            {
                
                let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
                present(segueVC, animated: true, completion: nil)
            }
            else
            {
            
            if self.favImageIV.image == #imageLiteral(resourceName: "Liked") {
                
                
                    self.favImageIV.image = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline.png")
                    self.removeFromFavList(positionId: sender.tag, sender: sender)
              
                
            }
            else
            {
                
                    self.favImageIV.image = #imageLiteral(resourceName: "Liked")
                    self.addToFavList(positionId: sender.tag, sender: sender)
                
                
                
            }
                
            }
        }
        else {
            showBanner(title: "Network Error", message: "No internet connectivity")
        }
        
        
        
        
    
    
    
   }
    
    @IBAction func testDriveNowPressed(_ sender: Any) {
        
        //print("testDriveNowPressed")

        
        
        
        if isNetworkAvailabel() {
            
            
            let id = PLAppState.session.userID
            
            if id == 0
            {
                let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
                present(segueVC, animated: true, completion: nil)
            }
            else
            {
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLEnquiryPopUpVC") as! PLEnquiryPopUpVC
                
                nextVC.popUPType = "Test Drive Enquiry"
                nextVC.typeID = 1 //test drive
                nextVC.locationType = 1 // New Car Location
                nextVC.carModelId = dataSource.carListData[ind].id
                
                print("isNexa ",dataSource.carListData[ind].isnexa)
                
                if dataSource.carListData[ind].isnexa == "1"{
                    nextVC.isNexa = true
                }else{
                    nextVC.isNexa = false
                }
                self.present(nextVC, animated: true, completion: nil)
                
            }
            
        }
        else
        {
            
            showBanner(title: "Network Error", message: "No internet connectivity")
            
        }
    }
    @IBAction func bookNowPressed(_ sender: Any) {
        showBanner(message: "Coming Soon")
        return
        
       // print("bookNowPressed")


        
        
        if isNetworkAvailabel() {
            
            
            let id = PLAppState.session.userID
            
            if id == 0
            {
                let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
                present(segueVC, animated: true, completion: nil)
            }
            else
            {
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLBookNowVC") as! PLBookNowVC
                
                nextVC.NewCarId = dataSource.carListData[ind].id.description
                nextVC.Variantid = dataSource.carListData[ind].varients[selectedVariantCVC].id.description//VarientSource[ind].id.description
                
                self.present(nextVC, animated: true, completion: nil)
                
            }
            
        }
        else
        {
            
            showBanner(title: "Network Error", message: "No internet connectivity")
            
        }
    }
    
    @IBAction func toggleFirstBtnPressed(_ sender: UIButton) {
        
        selectedParent[sender.tag].selectedParent = 0
        
        UIView.transition(with: specTBL,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: { self.specTBL.reloadData() })
       
    }
    
    @IBAction func toggleSecondBtnPressed(_ sender: UIButton) {
        
        selectedParent[sender.tag].selectedParent = 1
        
        UIView.transition(with: specTBL,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.specTBL.reloadData()
                            
        })
    }
    
    @IBAction func requestRoadPricePressed(_ sender: Any) {
        
        //print("requestRoadPricePressed")
        
       
        
        
        if isNetworkAvailabel() {
            
            
            let id = PLAppState.session.userID
            
            if id == 0
            {
                let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
                present(segueVC, animated: true, completion: nil)
            }
            else
            {
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLEnquiryPopUpVC") as! PLEnquiryPopUpVC
                
                nextVC.popUPType = " Request Road Price "
                nextVC.typeID = 7 // Request Road Price
                nextVC.locationType = 1 // New Car Location
                nextVC.carModelId = dataSource.carListData[ind].id
                if dataSource.carListData[ind].isnexa == "1"{
                    nextVC.isNexa = true
                }else{
                    nextVC.isNexa = false
                }
                self.present(nextVC, animated: true, completion: nil)
                
            }
            
        }
        else
        {
            
            showBanner(title: "Network Error", message: "No internet connectivity")
            
        }
    }
    
    @IBAction func extendWarrantyPressed(_ sender: Any) {
        print("extendWarrantyPressed")
        
        
        
        
        if isNetworkAvailabel() {
            
            
            let id = PLAppState.session.userID
            
            if id == 0
            {
                let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
                present(segueVC, animated: true, completion: nil)
            }
            else
            {
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLEnquiryPopUpVC") as! PLEnquiryPopUpVC
                
                nextVC.popUPType = " Extend Warranty "
                nextVC.typeID = 8  // Extend Warranty
                nextVC.locationType = 1 // New Car Location
                nextVC.carModelId = dataSource.carListData[ind].id
                
                if dataSource.carListData[ind].isnexa == "1"{
                    nextVC.isNexa = true
                }else{
                    nextVC.isNexa = false
                }
                self.present(nextVC, animated: true, completion: nil)
                
            }
        
        }
        else
        {
         
                showBanner(title: "Network Error", message: "No internet connectivity")
            
        }
        

    }
    
    @IBAction func playPressed(_ sender: Any) {
        
        print("playPressed")
    }

}



extension PLNewCarDetailVC: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == specListCLN {
            return specClnDatasource.count
        } else if collectionView == colorCLN {
            return dataSource.carListData[ind].colorImageas.count
        }else if collectionView == variantCLN {
            return dataSource.carListData[ind].varients.count
        } else if collectionView == imageCLN {
            if dataSource.carListData[ind].GalImages.count == 0{
                DispatchQueue.main.async {
                    self.galleryEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.galleryEmptyIndicatorLBL.isHidden = true
                }
            }
            if dataSource.carListData[ind].GalImages.count > 3 {
                return 3
            } else {
                return dataSource.carListData[ind].GalImages.count
            }
        } else if collectionView == videoCLN {
            if dataSource.carListData[ind].videos.count == 0{
                DispatchQueue.main.async {
                    self.videoEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.videoEmptyIndicatorLBL.isHidden = true
                }
            }
            if dataSource.carListData[ind].videos.count > 3 {
                return 3
            } else {
                return dataSource.carListData[ind].videos.count
            }
        }
        
        
        return 5
    }
   
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == specListCLN {
            
            let size: CGSize = specClnDatasource[indexPath.row].size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Roboto-Regular", size: 17) ?? 500])
            // UIFont.systemFont(ofSize: 14.0)])
            print(CGSize(width: size.width + 200 , height: specListCLN.bounds.size.height))
            return CGSize(width: size.width + 30
                , height: specListCLN.bounds.size.height)
        }else if collectionView == colorCLN {
            return CGSize(width: 40, height: 40)//(colorCLN.cellForItem(at: indexPath)?.frame.size)!
        }else if collectionView == variantCLN {
            return CGSize(width: 166, height: 260)//(variantCLN.cellForItem(at: indexPath)?.frame.size)!
            
        } else if collectionView == imageCLN {
            return CGSize(width: 165, height: 132)//(variantCLN.cellForItem(at: indexPath)?.frame.size)!
            
        } else if collectionView == videoCLN {
            return CGSize(width: 165, height: 132)//(variantCLN.cellForItem(at: indexPath)?.frame.size)!
            
        }
        else {
             return CGSize(width: 0, height: 0)
        }



    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = UICollectionViewCell()
        
        if collectionView == specListCLN {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLSpecCVC", for: indexPath) as! PLSpecCVC
            if selectedSpecCVC == indexPath.row {
                cell.nameLBL.text = specClnDatasource[indexPath.row]
                cell.nameLBL.textColor = UIColor.white
            } else {
                cell.nameLBL.text = specClnDatasource[indexPath.row]
                cell.nameLBL.textColor = UIColor.from(hex: "ee9285")
            }
            return cell
            
        }
        
        if collectionView == colorCLN {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "color", for: indexPath) as! PLNewCarDetailColorCVC
            let hexColor = dataSource.carListData[ind].colorImageas[indexPath.item].code
            
            print(hexColor)
            cell.colorView.backgroundColor = UIColor.from(hex: hexColor)
            if selectedColorCVC == indexPath.row {
                
                cell.colorView.clipsToBounds = false
            }
            else {
                cell.colorView.clipsToBounds = true
            }
            return cell
            
        }
        
        if collectionView == variantCLN {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "variant", for: indexPath) as! PLNewCarDetailVariantCVC
            

            
            
            if selectedVariantCVC == indexPath.row {
                
                cell.selectedIMG.isHidden = false
            }
            else {
                cell.selectedIMG.isHidden = true
            }
        
            
           
            cell.brandLBL.text = "Maruti Suzuki"
             cell.modelLBL.text = dataSource.carListData[ind].varients[indexPath.row].tittle
            cell.descriptionLBL.text = dataSource.carListData[ind].varients[indexPath.row].desc
            cell.priceLBL.text = "₹ " + dataSource.carListData[ind].varients[indexPath.row].price + " Lakh"
            
            
            
            cell.firstEnableIMG.isHidden = true
            cell.secondEnableIMG.isHidden = true
            cell.thirdEnableIMG.isHidden = true
            cell.fourthEnableIMG.isHidden = true
            
            cell.firstDescriptionLBL.isHidden = true
            cell.secondDescriptionLBL.isHidden = true
            cell.thirdDescriptionLBL.isHidden = true
            cell.fourthDescriptionLBL.isHidden = true
            
            
            if dataSource.carListData[ind].varients[indexPath.row].keyFor1 != ""
            {
             cell.firstDescriptionLBL.text = dataSource.carListData[ind].varients[indexPath.row].keyFor1
                cell.firstEnableIMG.isHidden = false
                cell.firstDescriptionLBL.isHidden = false
            }
            
            if dataSource.carListData[ind].varients[indexPath.row].keyFor2 != ""
            {
                cell.secondDescriptionLBL.text = dataSource.carListData[ind].varients[indexPath.row].keyFor2
                cell.secondEnableIMG.isHidden = false
                cell.secondDescriptionLBL.isHidden = false
            }
            
            if dataSource.carListData[ind].varients[indexPath.row].keyFor3 != ""
            {
                cell.thirdDescriptionLBL.text = dataSource.carListData[ind].varients[indexPath.row].keyFor3
                cell.thirdEnableIMG.isHidden = false
                cell.thirdDescriptionLBL.isHidden = false
            }
            
            if dataSource.carListData[ind].varients[indexPath.row].keyFor4 != ""
            {
                cell.fourthDescriptionLBL.text = dataSource.carListData[ind].varients[indexPath.row].keyFor4
                cell.fourthEnableIMG.isHidden = false
                cell.fourthDescriptionLBL.isHidden = false
            }
            

            

            
            
        return cell
            
        }

        if collectionView == imageCLN {
            if dataSource.carListData[ind].GalImages.count > 3 {
            
            if indexPath.row == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageMore", for: indexPath) as! PLNewCarDetailImageMoreCVC
                

                
                let iMPath = imageURL + dataSource.carListData[ind].GalImages[indexPath.item].path
                
                
                
                
                cell.carIMG.kf.setImage(with: URL(string:iMPath), options: [.requestModifier(getImageAuthorization())])
                
                cell.countLBL.text =  "\(dataSource.carListData[ind].GalImages.count - 2) more "
                //cell.transPview.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                cell.bagView.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                cell.carIMG.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                
                return cell
            }
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! PLNewCarDetailImageCVC
                
                
                
                let iMPath = imageURL + dataSource.carListData[ind].GalImages[indexPath.item].path
                
                
                
                
                cell.carIMG.kf.setImage(with: URL(string:iMPath), options: [.requestModifier(getImageAuthorization())])
                
                
                return cell
            }
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! PLNewCarDetailImageCVC
                
                let iMPath = imageURL + dataSource.carListData[ind].GalImages[indexPath.item].path

                
                cell.carIMG.kf.setImage(with: URL(string:iMPath), options: [.requestModifier(getImageAuthorization())])
                
                return cell
            }

        }
        if collectionView == videoCLN {
            
   
            
            if dataSource.carListData[ind].videos.count > 3 {
                if indexPath.row == 2 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoMore", for: indexPath) as! PLNewCarDetailVideoMoreCVC
                    cell.carIMG.kf.setImage(with: URL(string: dataSource.carListData[ind].videos[indexPath.row]))
                    cell.countLBL.text =  "\(dataSource.carListData[ind].videos.count - 2) more"
                   // cell.transPview.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                    cell.bagView.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                    cell.carIMG.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
                    return cell
                }
                else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath) as! PLNewCarDetailVideoCVC
                     cell.carIMG.kf.setImage(with: URL(string: dataSource.carListData[ind].videos[indexPath.row]))
                    return cell
                }
                
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath) as! PLNewCarDetailVideoCVC
                
                let videoURL = dataSource.carListData[ind].videos[indexPath.item]
                //let iVPath = "https://www.youtube.com/watch?v=YB_9AYyR9Gc" //"http://app.appzoc.com/popular/backend/storage/app/" + dataSource.carListData[ind].videos[indexPath.item]
                //dataSource.carListData[ind].videos[indexPath.item]
                if let thumbnailImage = getThumbnailImage(forUrl: videoURL){
                    print("Thumbnail Image = ",thumbnailImage)
                    cell.carIMG.kf.setImage(with: URL(string: thumbnailImage))
                }
                
                //cell.carIMG.kf.setImage(with: URL(string: dataSource.carListData[ind].videos[indexPath.item]))
                return cell

            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionViewUIEnableFlag{
            let row = indexPath.row
            
            
            //let rectOfCellInTableView = specTBL.rectForRow(at: IndexPath(row: 0, section: 0))
           // let rectOfCellInSuperview = specTBL.convert(rectOfCellInTableView, from: specTBL)
            
    //            [scrollViewB convertPoint:CGPointZero fromView:viewA];
           
           let gal = specTBL.convert(CGPoint.zero, from: vehiclePriceView)
            let gal2 = specTBL.convert(CGPoint.zero, to: gallaryView)
            print(gal)
            print(gal2)
            
            if collectionView == specListCLN {
                //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLSpecCVC", for: indexPath) as! PLSpecCVC
                shouldCheckScroll = false
                selectedSpecCVC = indexPath.row
                specListCLN.reloadData()
                print("Selected Top Collection  index ", selectedSpecCVC)
                specListCLN.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                
                switch row {
                case 0:
                   specTBL.setContentOffset(CGPoint(x: 0, y: imageHeaderView.frame.origin.y), animated: false)
                   // specListCLN.reloadData()
                  //  shouldCheckScroll = true
                case 1:
                    specTBL.setContentOffset(CGPoint(x: 0, y: variantView.frame.origin.y), animated: false)
                    //specListCLN.reloadData()
                  //  shouldCheckScroll = true
                case 2:
                        let indexPathForSectionHeader = IndexPath(row: NSNotFound, section: 0)
                        DispatchQueue.main.async {
                            self.specTBL.scrollToRow(at: indexPathForSectionHeader, at: .top, animated: false)
                        }
                   // shouldCheckScroll = true
                   // specTBL.scrollRectToVisible(rectOfCellInSuperview, animated: true)
                    //specTBL.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    // specTBL.setContentOffset(CGPoint(x: 0, y: rectOfCellInSuperview.origin.y), animated: true)
    //                specTBL.setContentOffset(CGPoint(x: rectOfCellInSuperview.origin.x, y: rectOfCellInSuperview.origin.y), animated: true)
                  //  let sectionRect = specTBL.rect(forSection: 0)
                    //specTBL.scrollRectToVisible(sectionRect, animated: true)
                    //specTBL.setContentOffset(CGPoint(x: 0, y: sectionRect.origin.y), animated: true)
                    //specListCLN.reloadData()
                case 3:
                  
                    
                    specTBL.setContentOffset(CGPoint(x: 0, y: tableFooterContainerView.frame.minY), animated: false)
                   // specListCLN.reloadData()
                   // shouldCheckScroll = true
                   
                case 4:
    //                var offset = specTBL.contentOffset
    //                offset.y = specTBL.contentSize.height + specTBL.contentInset.bottom - specTBL.bounds.size.height
    //                specTBL.setContentOffset(offset, animated: true)
    //                specTBL.
                    
                    print("GalleryView Offset ",gallaryView.frame.minY)
                    //let yy = gallaryView.frame.origin.y + tableFooterContainerView.frame.height
                    let offsetPoint = gallaryView.convert(CGPoint(x: gallaryView.frame.minX, y: gallaryView.frame.minY) , to: specTBL)
                    print("Offset Point ",offsetPoint)
                    specTBL.setContentOffset(CGPoint(x: 0, y: offsetPoint.y - 440), animated: false)
                    shouldCheckScroll = true
                   // specListCLN.reloadData()
                print("5304")
                   
                    
                default:
                    print("")
                   // shouldCheckScroll = true
                }
                
           // shouldCheckScroll = true
                
            }
            
            if collectionView == colorCLN {
    //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "color", for: indexPath) as! PLNewCarDetailColorCVC
                
                
                selectedColorCVC = indexPath.row
                
                
                let iMPath = imageURL + dataSource.carListData[ind].colorImageas[indexPath.row].url

                
                self.carIMG.kf.setImage(with: URL(string:iMPath), options: [.requestModifier(getImageAuthorization())])
                
                
                
                colorCLN.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                colorCLN.reloadData()
                
               
                
            }
            
            if collectionView == variantCLN {
                selectedVariantCVC = indexPath.row
                if let variantData = CoreDataManager.sharedManager.getcarEventForId(id: Int64(dataSource.carListData[ind].varients[indexPath.item].id)){
                    getDataForVariantFromDataBase(variantData: variantData)
                    if let detail = variantData.details{
                        print("Details **& : ",detail)
                    }
                }else{
                    self.getDataForVariant(id: dataSource.carListData[ind].varients[indexPath.item].id)
                }
                
                //self.getVarientDummy(idFor:dataSource.carListData[ind].varients[indexPath.item].id)
                
                variantCLN.reloadData()
               // ii = 0
                
                ///VarientSource[0].
                
    ////            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "variant", for: indexPath)
    //            let variantChoosen = detailDataModel.variants[indexPath.item].variantname
    //
    //
    //            print(variantChoosen)
    //
    //
    //            for i in 0..<detailDataModel.variants.count {
    //
    //                if detailDataModel.variants[i].variantname == variantChoosen {
    //                   // self.variantData = detailDataModel.variants[i]
    //                    print(variantChoosen)
    //                    print(i)
    //                     selectedVariantCVC = i
    //
    //                    break
    //                }
    //            }
    //
    //
    //
    //            variantCLN.scrollToItem(at: IndexPath(item: selectedVariantCVC, section: 0) , at: .centeredHorizontally, animated: true)
    //            variantCLN.reloadData()
    //            setupViewForVariant(type: variantChoosen)
                
            }
            
            if collectionView == imageCLN {
    //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath)
    //            selectedImageCVC = indexPath.row
    //
    //            imageCLN.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    //            imageCLN.reloadData()
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLGalleryVC") as! PLGalleryVC
                
                nextVC.indX = ind
                nextVC.selectedHeader = 0
                self.present(nextVC, animated: true, completion: nil)
                
               // PLGalleryVC
                
            }
            if collectionView == videoCLN {
    //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath)
                selectedVideoCVC = indexPath.row
                
                videoCLN.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                videoCLN.reloadData()
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLGalleryVC") as! PLGalleryVC
                nextVC.selectedHeader = 2
                nextVC.indX = ind
                self.present(nextVC, animated: true, completion: nil)
            }
        }
    }
}

//MARK:- TableView
extension PLNewCarDetailVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("Sections Count ",varientData.variantDetail.first?.features.count ?? 0)
        return varientData.variantDetail.first?.features.count ?? 0 //sectionHeaderDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if varientData.variantDetail.first?.features[section].parentDetails == 0{
            if selectedParent[section].sectionSelected{
                print("Number of Rows ",varientData.variantDetail.first?.features[section].parent.count ?? 0)
                return varientData.variantDetail.first?.features[section].parent.count ?? 0
            }else{
                print("Number of Rows 0")
                return 0
            }
        }else{
            if selectedParent[section].sectionSelected{
                print("Number of Rows ",(varientData.variantDetail.first?.features[section].parent[selectedParent[section].selectedParent].details?.count ?? 0) + 1)
                return (varientData.variantDetail.first?.features[section].parent[selectedParent[section].selectedParent].details?.count ?? 0) + 1
            }else{
                print("Number of Rows 0")
                return 0
            }
        }
       // print("Number of Rows 0")
        //return 0
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "header") as! PLNewCarDetailHeaderCell
        if let headerData = varientData.variantDetail.first{
            headerCell.titileLBL.text = headerData.features[section].name //sectionHeaderDataSource[section]
            
        }else{
            headerCell.titileLBL.text = "-0"
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        headerCell.isUserInteractionEnabled = true
        headerCell.tag = section
        headerCell.addGestureRecognizer(tap)
        headerCell.backgroundView =  UIView(frame: (headerCell.bounds))
        
//        if isHidden[section] {
//           // headerCell.arrowIMGV.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
//        }else{
//            headerCell.arrowIMGV.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
//        }
        
        if selectedParent[section].sectionSelected{
            headerCell.arrowIMGV.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        }else{
            headerCell.arrowIMGV.transform = CGAffineTransform.identity
        }
        
        //headerView.addSubview(headerCell)
        
        headerView.layoutSubviews()
        headerView.layoutIfNeeded()
        return headerCell
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return CGFloat(0.00001)
//    }
    

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let toggleCell = tableView.dequeueReusableCell(withIdentifier: "PLNewCarDetailSwitchTVC") as! PLNewCarDetailSwitchTVC
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: "PLNewCarDetailDataTVC") as! PLNewCarDetailDataTVC
        _ = tableView.dequeueReusableCell(withIdentifier: "PLNewCarDetailCheckTVC") as! PLNewCarDetailCheckTVC
        
        guard let varientContext = varientData.variantDetail.first else { return detailsCell }
        
        if varientContext.features[indexPath.section].parentDetails == 0{
            detailsCell.configureFoeSingleSetWith(data: varientContext.features[indexPath.section].parent[indexPath.row])
            return detailsCell
        }else{
            if indexPath.row == 0{
                toggleCell.configureWith(data: varientContext.features[indexPath.section].parent, parent: selectedParent[indexPath.section], index: indexPath)
                return toggleCell
            }else{
                if let detail = varientContext.features[indexPath.section].parent[selectedParent[indexPath.section].selectedParent].details{
                    detailsCell.configureWith(data: detail[indexPath.row - 1])
                }
                return detailsCell
            }
        }
        //return toggleCell
    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let section = indexPath.section
//        let row = indexPath.row
//
//
//        print("cred")
//
//
//        print(VarientSource[0].featur[section].parentDetails)
//
//        if VarientSource[0].featur[section].parentDetails == 0
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
//
//
//
//
//
//
//            cell.imageTick.isHidden = true
//            cell.valueLBL.isHidden = true
//            if VarientSource[0].featur[section].keyValAr[indexPath.row].val.lowercased() == "yes"
//            {
//                cell.valueLBL.isHidden = true
//                cell.imageTick.isHidden = false
//            }
//            else
//            {
//                cell.valueLBL.isHidden = false
//                cell.imageTick.isHidden = true
//            }
//
//
//
//            cell.descriptionLBL.text = VarientSource[0].featur[section].keyValAr[indexPath.row].key
//
//            cell.valueLBL.text =  VarientSource[0].featur[section].keyValAr[indexPath.row].val
//
//            return cell
//        }
//        else
//        {
//
//
//
//            if indexPath.row == 0
//            {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "Toggle Switch") as! PLNewCarDetailSwitchTVC
//
//
//
//
//                cell.firstToggleLBL.text = VarientSource[0].featur[section].featureDetail[0].name
//                cell.firstToggleBTN.tag = section
//                cell.secondToggleBTN.tag = section
//                cell.secondToggleLBL.text = VarientSource[0].featur[section].featureDetail[1].name
//
//                if let _ = selectedToggle[section] as? [Int:Bool] {
//
//                } else {
//                    selectedToggle[section] = [1:true,2:false]
//                }
//
//                if let id = selectedToggle[section] as? [Int:Bool] {
//
//                    if id[1]! {
//
//                        cell.firstToggleLBL.textColor = UIColor.white
//                        cell.frstToggleView.backgroundColor = UIColor.BlueColor()
//
//                        cell.secondToggleLBL.textColor = UIColor.DarkColor()
//                        cell.secondToggleView.backgroundColor = UIColor.white
//
//
//
//                        ii = 0
//
//
//
//                    } else {
//
//                        cell.firstToggleLBL.textColor = UIColor.DarkColor()
//                        cell.frstToggleView.backgroundColor = UIColor.white
//
//                        cell.secondToggleLBL.textColor = UIColor.white
//                        cell.secondToggleView.backgroundColor = UIColor.BlueColor()
//
//                        ii = 1
//
//
//                    }
//                }
//
//
//
//
//                return cell
//            }
//            else
//            {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
//
//
//
//                if ii == 0
//                {
//
//                    print(indexPath.row)
//                    print(VarientSource[0].featur[section].featureDetail.count + 1)
//
//
//
//
//                    if (self.allKeys0[section] != nil)
//                    {
//                        cell.imageTick.isHidden = true
//                        cell.valueLBL.isHidden = true
//                        print("All Keys ",allKeys0[section]?.description,"Index Path ",indexPath.row)
//                        if let valueArray = self.allKeys0[section] as? [String], valueArray.count > (indexPath.row - 1){
//                            cell.descriptionLBL.text = self.allKeys0[section]?[indexPath.row - 1]
//                            cell.valueLBL.text = self.allValues0[section]?[indexPath.row - 1]
//
//                            if self.allValues0[section]?[indexPath.row - 1].lowercased() == "yes"
//                            {
//                                cell.valueLBL.isHidden = true
//                                cell.imageTick.isHidden = false
//                            }
//                            else
//                            {
//                                cell.valueLBL.isHidden = false
//                                cell.imageTick.isHidden = true
//                            }
//                        }
////                        if self.allValues0[section]?[indexPath.row - 1].lowercased() == "yes"
////                        {
////                            cell.valueLBL.isHidden = true
////                            cell.imageTick.isHidden = false
////                        }
////                        else
////                        {
////                            cell.valueLBL.isHidden = false
////                            cell.imageTick.isHidden = true
////                        }
//                    }
//
//                 /*   if suspensionVal.count != 0
//                    {
//
//                        cell.imageTick.isHidden = true
//                        cell.valueLBL.isHidden = true
//
//                        cell.descriptionLBL.text = self.allKeys0[section]?[indexPath.row - 1]
//                        cell.valueLBL.text = self.allValues0[section]?[indexPath.row - 1]
//
//                        if self.suspensionVal[indexPath.row - 1].lowercased() == "yes"
//                        {
//                            cell.valueLBL.isHidden = true
//                            cell.imageTick.isHidden = false
//                        }
//                        else
//                        {
//                            cell.valueLBL.isHidden = false
//                            cell.imageTick.isHidden = true
//                        }
//
//
//
//
//
//
//
//
//
//                    } */
//
//
//
//                }
//                else
//                {
//
//
//                    if (self.allKeys1[section] != nil)
//                    {
//
//                        cell.imageTick.isHidden = true
//                        cell.valueLBL.isHidden = true
//                        if let arrayValue = self.allValues1[section] as? [String], arrayValue.count > indexPath.row - 1{
////                            if let arrayValueAlter = self.allValues1[section] as? [String], arrayValueAlter.count > (indexPath.row - 1){
//                                if self.allValues1[section]?[indexPath.row - 1].lowercased() == "yes"
//                                {
//                                    cell.valueLBL.isHidden = true
//                                    cell.imageTick.isHidden = false
//                                }
//                                else
//                                {
//                                    cell.valueLBL.isHidden = false
//                                    cell.imageTick.isHidden = true
//                                }
//                         //   }
//
//                            cell.descriptionLBL.text = self.allValues1[section]?[indexPath.row - 1]
//
//                            cell.valueLBL.text = self.allValues1[section]?[indexPath.row - 1]
//                        }
//
//                    }
//
//
//
//                }
//
//
//
//
//
//
//
//
//
//
//                return cell
//            }
//
//
//
//
//        }
//
//
//
//
//
//
////        switch  section {
////        case 0:
////
////              print(VarientSource[0].featur[section].parentDetails)
////
////               if VarientSource[0].featur[section].parentDetails == 0
////               {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
////
////                cell.descriptionLBL.text = VarientSource[0].featur[section].keyValAr[indexPath.row].key
////
////                cell.valueLBL.text =  VarientSource[0].featur[section].keyValAr[indexPath.row].val
////
////                return cell
////               }
////              else
////               {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Toggle Switch") as! PLNewCarDetailSwitchTVC
////                cell.firstToggleLBL.text = "Suspension"
////                cell.firstToggleBTN.tag = section
////                cell.secondToggleBTN.tag = section
////                cell.secondToggleLBL.text = "Brakes"
////                if selectedToggle[0]! {
////
////                    cell.firstToggleLBL.textColor = UIColor.white
////                    cell.frstToggleView.backgroundColor = UIColor.BlueColor()
////
////                    cell.secondToggleLBL.textColor = UIColor.DarkColor()
////                    cell.secondToggleView.backgroundColor = UIColor.white
////                } else {
////
////                    cell.firstToggleLBL.textColor = UIColor.DarkColor()
////                    cell.frstToggleView.backgroundColor = UIColor.white
////
////                    cell.secondToggleLBL.textColor = UIColor.white
////                    cell.secondToggleView.backgroundColor = UIColor.BlueColor()
////                }
////
////
////                return cell
////               }
////
////
////        case 1:
////            if row == 0 {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Toggle Switch") as! PLNewCarDetailSwitchTVC
////                cell.firstToggleLBL.text = "Suspension"
////                cell.firstToggleBTN.tag = section
////                cell.secondToggleBTN.tag = section
////                  cell.secondToggleLBL.text = "Brakes"
////                if selectedToggle[0]! {
////
////                cell.firstToggleLBL.textColor = UIColor.white
////                cell.frstToggleView.backgroundColor = UIColor.BlueColor()
////
////                cell.secondToggleLBL.textColor = UIColor.DarkColor()
////                cell.secondToggleView.backgroundColor = UIColor.white
////                } else {
////
////                    cell.firstToggleLBL.textColor = UIColor.DarkColor()
////                    cell.frstToggleView.backgroundColor = UIColor.white
////
////                    cell.secondToggleLBL.textColor = UIColor.white
////                    cell.secondToggleView.backgroundColor = UIColor.BlueColor()
////                }
////
////
////                return cell
////            } else {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
////                if selectedToggle[0]! {
////
////                cell.descriptionLBL.text = suspensionKeys[indexPath.row - 1]
////                cell.valueLBL.text = suspensionKeys[indexPath.row - 1]
////
////                } else {
////
////                    cell.descriptionLBL.text = brakesKeys[indexPath.row - 1]
////                    cell.valueLBL.text = brakesVal[indexPath.row - 1]
////                }
////
////                return cell
////            }
////        case 2:
////
////            if row == 0 {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Toggle Switch") as! PLNewCarDetailSwitchTVC
////
////                cell.firstToggleLBL.text = "Dimension"
////                cell.secondToggleLBL.text = "Capacity"
////                cell.firstToggleBTN.tag = section
////                cell.secondToggleBTN.tag = section
////                if selectedToggle["Dimension"]! {
////
////                    cell.firstToggleLBL.textColor = UIColor.white
////                    cell.frstToggleView.backgroundColor = UIColor.BlueColor()
////
////                    cell.secondToggleLBL.textColor = UIColor.DarkColor()
////                    cell.secondToggleView.backgroundColor = UIColor.white
////                } else {
////
////                    cell.firstToggleLBL.textColor = UIColor.DarkColor()
////                    cell.frstToggleView.backgroundColor = UIColor.white
////
////                    cell.secondToggleLBL.textColor = UIColor.white
////                    cell.secondToggleView.backgroundColor = UIColor.BlueColor()
////                }
////
////                return cell
////            } else {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
////                if selectedToggle["Dimension"]! {
////                    let key = dimensionKeys[row-1]
////                    cell.descriptionLBL.text = key.dispayString()
////                    cell.valueLBL.text = variantData.dimension[key]
////
////                } else {
////                    let key = capacityKeys[row-1]
////                    cell.descriptionLBL.text = key.dispayString()
////                    cell.valueLBL.text = variantData.capacity[key]
////                }
////
////                return cell
////            }
////
////
////        case 3:
////
////            let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
////            let key = weightKeys[row]
////            cell.descriptionLBL.text = key.dispayString()
////            cell.valueLBL.text = variantData.weight[key]
////
////            return cell
////        case 4:
////            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! PLNewCarDetailCheckTVC
////            let key = safetyKeys[row]
////            cell.descriptionLBL.text = key.dispayString()
////
////            print(variantData.safety[key] ?? "no")
////            let flag = variantData.safety[key]?.last
////            print(flag as Any)
////            if flag == "1" {
////                cell.verifiedIMG.isHidden = false
////            } else {
////                cell.verifiedIMG.isHidden = true
////            }
////
////
////            return cell
////
////
////        case 5:
////
////            let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! PLNewCarDetailDataTVC
////
////            cell.descriptionLBL.text = "Exterior"
////            cell.valueLBL.text = variantData.exterior
////            return cell
////
////
////        default:
////            let cell = tableView.dequeueReusableCell(withIdentifier: "Test")
////
////            cell?.textLabel?.text = indexPath.row.description + indexPath.row.description
////
////
////
////            return cell!
////        }
//
//
//
//
//
//
//    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let section = sender.view!.tag
        
        _ = Int()
        
        let shouldCollapse:Bool = self.selectedParent[section].sectionSelected
        
        self.populateParentList()
        
        self.selectedParent[section].selectedParent = 0
        
        if shouldCollapse{
            self.selectedParent[section].sectionSelected = false
        }else{
            self.selectedParent[section].sectionSelected = true
        }
        
        
        DispatchQueue.main.async {
            self.specTBL.reloadData()
            self.specTBL.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .top, animated: true)
        }

        /*
        
        if VarientSource[0].featur[section].parentDetails == 0
        {
            
            id = VarientSource[0].featur[section].keyValAr.count
        }
        else
        {
            if ii == 0
            {
                
                if let co = self.allValues0[section]?.count
                {
                    id = co + 1
                }
                else
                {
                    id =  1
                }
            }
            else
            {
                
                
                if let co = self.allValues1[section]?.count
                {
                    id =  co + 1
                }
                else
                {
                    id =  1
                }
        }
            
        }
        
        
        let indexPaths = (0..<id).map { i in return IndexPath(item: i, section: section)  }
        
        isHidden[section] = !isHidden[section]
        
        print(indexPaths)
        
//        specTBL?.beginUpdates()
//
//        if isHidden[section] {
//            specTBL?.deleteRows(at: indexPaths, with: .fade)
//        } else {
//            specTBL?.insertRows(at: indexPaths, with: .fade)
//        }
//        specTBL?.endUpdates()
       // specTBL.reloadSections([section], with: .none)
        
        */
        
//        UIView.transition(with: specTBL,
//                          duration: 0.2,
//                          options: .transitionCrossDissolve,
//                          animations: {
//                            DispatchQueue.main.async {
//                                self.specTBL.reloadData()
//                            }
//        })
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//            let scrollViewHeight = scrollView.frame.size.height
//            let scrollContentSizeHeight = scrollView.contentSize.height
//            let scrollOffset = scrollView.contentOffset.y
//
//            if (scrollOffset == 0)
//            {
//                // then we are at the top
//            }
//            else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
//            {
//                // then we are at the end
//            }
//
//            let viewFrame = TestDriveView.frame
//
//            let container = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
//
//
//            // We may have received messages while this tableview is offscreen
//            if (viewFrame.intersects(container)) {
//                // Do work here
//                //print("view is visible")
//                TestDriveBubbleView.isHidden = true
//            }
//            else{
//
//                TestDriveBubbleView.isHidden = false
//                //  print("nope view is not on the screen")
//            }
//
//
//            //MARK:- For Spec CLN
//
//
//
//
//
//            _ = specTBL.headerView(forSection: 0)
//            //specTBL.rect(forSection: 0)
//            _ = specTBL.convert(CGPoint.zero, from: vehiclePriceView)
//
//            if scrollView == specTBL {
//                //let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: self.view.frame.height/2)
//                //let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/3)
//                let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
//                print("ContainerHalf : ",containerHalf.debugDescription)
//              //  print("imageHeaderView",imageHeaderView.frame.origin.y)
//                //print("vehi",vehiclePriceView.frame.origin.y)
//            //    print("galle",gallaryView.frame.origin.y)
//           //     print("specifica",specificationView.frame.origin.y)
//            //    print(containerHalf.origin,"containerHalf")
//            //    print("tableFooterContainerView",tableFooterContainerView.frame.origin.y)
//                //                    initialVehiclePriceViewY = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
//                //                    initialGallaryViewY = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y
//                //            vehiclePriceView.frame.origin.y = initialVehiclePriceViewY
//                //            gallaryView.frame.origin.y = initialGallaryViewY
//                //vehiclePriceViewRect = vehiclePriceView.frame
//
//           //--     vehicleGalleryViewRect = gallaryView.frame
//                //vehiclePriceViewRect.origin.y = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
//           //--     vehicleGalleryViewRect.origin.y = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y
//
//
//                let imageViewPoint = imageHeaderView.convert(CGPoint(x: imageHeaderView.frame.minX, y: imageHeaderView.frame.minY), to: scrollView)
//                let imageViewRect = CGRect(origin: imageViewPoint, size: imageHeaderView.frame.size)
//
//                let variantViewPoint = variantView.convert(CGPoint(x: variantView.frame.minX, y: variantView.frame.minY), to: specTBL)
//                let variantViewRect = CGRect(origin: variantViewPoint, size: variantView.frame.size)
//
//                let priceViewPoint = vehiclePriceView.convert(CGPoint(x: vehiclePriceView.frame.minX, y: vehiclePriceView.frame.minY), to: scrollView)
//                let priceViewRect = CGRect(origin: priceViewPoint, size: vehiclePriceView.frame.size)
//               // print("Vehicle pRice view point ", CGRect(origin: priceViewPoint, size: vehiclePriceView.frame.size))
//
//                let galleryViewPoint = gallaryView.convert(CGPoint(x: gallaryView.frame.minX, y: gallaryView.frame.minY), to: scrollView)
//                let galleryViewRect = CGRect(origin: galleryViewPoint, size: gallaryView.frame.size)
//
//                let specificationHeader = specTBL.headerView(forSection: 0) ?? UIView()
//                let specificationViewPoint = specificationHeader.convert(CGPoint(x: specificationHeader.frame.minX, y: specificationHeader.frame.minY), to: scrollView)
//                let specificationViewRect = CGRect(origin: specificationViewPoint, size: specificationHeader.frame.size)
//
//                print("image View Rect ",imageViewRect.debugDescription)
//                print("Variant View Rect ",variantViewRect.debugDescription)
//                print("Price point Rect ",priceViewRect.debugDescription)
//                print("Gallery View Rect ",galleryViewRect.debugDescription)
//                print("Specification View Rect ",specificationViewRect.debugDescription)
//
//
//                if (scrollView.bounds.intersects(imageViewRect)) {
//                    // Do work here
//                    print("view is visible,-image")
//                    selectedSpecCVC = 0
//                    specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
//                    specListCLN.reloadData()
//
//                }
//                if (scrollView.contentOffset.y >= 420 && scrollView.contentOffset.y <= 900 ) {  //scrollView.bounds.intersects(variantViewRect)
//                    // Do work here
//                    print("view is visible - car models")
//                    selectedSpecCVC = 1
//                    specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
//                    specListCLN.reloadData()
//
//                }
//
//                 if scrollView.contentOffset.y > 905 &&  scrollView.contentOffset.y < priceViewRect.minY {      // (scrollView.bounds.intersects(specificationViewRect))
//                    // Do work here
//                    print("view is visible-specificzs")
//                    selectedSpecCVC = 2
//                    specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
//                    specListCLN.reloadData()
//                }
//
//
//
//                if (scrollView.bounds.intersects(priceViewRect)) {
//                     // Do work here
//                     print("view is visible-vehicleprice")
//                     selectedSpecCVC = 3
//                     specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
//                     specListCLN.reloadData()
//
//                 }
//
//
//                if (scrollView.bounds.intersects(galleryViewRect)) {
//                    // Do work here
//                    print("view is visible-gallersy")
//                    selectedSpecCVC = 4
//                    specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
//                    specListCLN.reloadData()
//                }
//                print("Scroll view bounds ",scrollView.bounds, " gallery view frame ",gallaryView.frame, " PriceView frame ",vehiclePriceView.frame)
//
//            }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
        {
            // then we are at the end
        }
        
        _ = TestDriveView.frame
        
        _ = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
        
        
        // We may have received messages while this tableview is offscreen
        
//        if (viewFrame.intersects(container)) {
//            // Do work here
//            //print("view is visible")
//            TestDriveBubbleView.isHidden = true
//        }
//        else{
//
//            TestDriveBubbleView.isHidden = false
//            //  print("nope view is not on the screen")
//        }
        
        
        
        
        //MARK:- For Spec CLN
        
        
        
        
        
        _ = specTBL.headerView(forSection: 0)
        //specTBL.rect(forSection: 0)
        _ = specTBL.convert(CGPoint.zero, from: vehiclePriceView)
        
        if scrollView == specTBL {
            //let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: self.view.frame.height/2)
            //let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/3)
            let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
            print("ContainerHalf : ",containerHalf.debugDescription)
            //  print("imageHeaderView",imageHeaderView.frame.origin.y)
            //print("vehi",vehiclePriceView.frame.origin.y)
            //    print("galle",gallaryView.frame.origin.y)
            //     print("specifica",specificationView.frame.origin.y)
            //    print(containerHalf.origin,"containerHalf")
            //    print("tableFooterContainerView",tableFooterContainerView.frame.origin.y)
            //                    initialVehiclePriceViewY = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
            //                    initialGallaryViewY = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y
            //            vehiclePriceView.frame.origin.y = initialVehiclePriceViewY
            //            gallaryView.frame.origin.y = initialGallaryViewY
            //vehiclePriceViewRect = vehiclePriceView.frame
            
            //--     vehicleGalleryViewRect = gallaryView.frame
            //vehiclePriceViewRect.origin.y = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
            //--     vehicleGalleryViewRect.origin.y = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y
            
            
            let imageViewPoint = imageHeaderView.convert(CGPoint(x: imageHeaderView.frame.minX, y: imageHeaderView.frame.minY), to: scrollView)
            let imageViewRect = CGRect(origin: imageViewPoint, size: imageHeaderView.frame.size)
            
            let variantViewPoint = variantView.convert(CGPoint(x: variantView.frame.minX, y: variantView.frame.minY), to: specTBL)
            let variantViewRect = CGRect(origin: variantViewPoint, size: variantView.frame.size)
            
            let priceViewPoint = vehiclePriceView.convert(CGPoint(x: vehiclePriceView.frame.minX, y: vehiclePriceView.frame.minY), to: scrollView)
            let priceViewRect = CGRect(origin: priceViewPoint, size: vehiclePriceView.frame.size)
            // print("Vehicle pRice view point ", CGRect(origin: priceViewPoint, size: vehiclePriceView.frame.size))
            
            let galleryViewPoint = gallaryView.convert(CGPoint(x: gallaryView.frame.minX, y: gallaryView.frame.minY), to: scrollView)
            let galleryViewRect = CGRect(origin: galleryViewPoint, size: gallaryView.frame.size)
            
            let specificationHeader = specTBL.headerView(forSection: 0) ?? UIView()
            let specificationViewPoint = specificationHeader.convert(CGPoint(x: specificationHeader.frame.minX, y: specificationHeader.frame.minY), to: scrollView)
            let specificationViewRect = CGRect(origin: specificationViewPoint, size: specificationHeader.frame.size)
            
            print("image View Rect ",imageViewRect.debugDescription)
            print("Variant View Rect ",variantViewRect.debugDescription)
            print("Price point Rect ",priceViewRect.debugDescription)
            print("Gallery View Rect ",galleryViewRect.debugDescription)
            print("Specification View Rect ",specificationViewRect.debugDescription)
            
            
            if (scrollView.bounds.intersects(imageViewRect)) {
                // Do work here
                print("view is visible,-image")
                selectedSpecCVC = 0
                DispatchQueue.main.async{
                    self.TestDriveBubbleView.isHidden = true
                    self.specListCLN.scrollToItem(at: IndexPath(item: self.selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                    self.specListCLN.reloadData()
                }
                
            }
            if (scrollView.contentOffset.y >= 420 && scrollView.contentOffset.y <= 900 ) {  //scrollView.bounds.intersects(variantViewRect)
                // Do work here
                print("view is visible - car models")
                selectedSpecCVC = 1
                DispatchQueue.main.async{
                    self.TestDriveBubbleView.isHidden = true
                    self.specListCLN.scrollToItem(at: IndexPath(item: self.selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                    self.specListCLN.reloadData()
                }
                
            }
            
            if scrollView.contentOffset.y > 905 &&  scrollView.contentOffset.y < priceViewRect.minY {      // (scrollView.bounds.intersects(specificationViewRect))
                // Do work here
                print("view is visible-specificzs")
                selectedSpecCVC = 2
                DispatchQueue.main.async {
                    self.TestDriveBubbleView.isHidden = false
                    self.specListCLN.scrollToItem(at: IndexPath(item: self.selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                    self.specListCLN.reloadData()
                }
            }
            
            
            
            if (scrollView.bounds.intersects(priceViewRect)) {
                // Do work here
                print("view is visible-vehicleprice")
                selectedSpecCVC = 3
                DispatchQueue.main.async{
                    self.TestDriveBubbleView.isHidden = false
                    self.specListCLN.scrollToItem(at: IndexPath(item: self.selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                    self.specListCLN.reloadData()
                }
                
            }
            
            
            if (scrollView.bounds.intersects(galleryViewRect)) {
                // Do work here
                print("view is visible-gallersy")
                selectedSpecCVC = 4
                DispatchQueue.main.async{
                    self.TestDriveBubbleView.isHidden = false
                    self.specListCLN.scrollToItem(at: IndexPath(item: self.selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                    self.specListCLN.reloadData()
                }
            }
            print("Scroll view bounds ",scrollView.bounds, " gallery view frame ",gallaryView.frame, " PriceView frame ",vehiclePriceView.frame)
            
        }
        
        /*
        if shouldCheckScroll{
        
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
        {
            // then we are at the end
        }
        
        let viewFrame = TestDriveView.frame
        
        let container = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
        
        
        // We may have received messages while this tableview is offscreen
        if (viewFrame.intersects(container)) {
            // Do work here
            //print("view is visible")
            TestDriveBubbleView.isHidden = true
        }
        else{
            
            TestDriveBubbleView.isHidden = false
              //  print("nope view is not on the screen")
        }
        
        
        //MARK:- For Spec CLN
        

        
        
        
        _ = specTBL.headerView(forSection: 0)
            //specTBL.rect(forSection: 0)
        _ = specTBL.convert(CGPoint.zero, from: vehiclePriceView)
        
        if scrollView == specTBL {
            //let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: self.view.frame.height/2)
            let containerHalf = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height/3)

            print("imageHeaderView",imageHeaderView.frame.origin.y)
            //print("vehi",vehiclePriceView.frame.origin.y)
            print("galle",gallaryView.frame.origin.y)
            print("specifica",specificationView.frame.origin.y)
            print(containerHalf.origin,"containerHalf")
            print("tableFooterContainerView",tableFooterContainerView.frame.origin.y)
//                    initialVehiclePriceViewY = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
//                    initialGallaryViewY = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y
//            vehiclePriceView.frame.origin.y = initialVehiclePriceViewY
//            gallaryView.frame.origin.y = initialGallaryViewY
            //vehiclePriceViewRect = vehiclePriceView.frame
            vehicleGalleryViewRect = gallaryView.frame
            //vehiclePriceViewRect.origin.y = vehiclePriceView.frame.origin.y + tableFooterContainerView.frame.origin.y
            vehicleGalleryViewRect.origin.y = gallaryView.frame.origin.y + tableFooterContainerView.frame.origin.y

            print("vehiNewwwwww",initialVehiclePriceViewY)
            print("galleNewwwww",initialGallaryViewY)

            if (imageHeaderView.frame.intersects(containerHalf)) {
                // Do work here
                print("view is visible,-image")
                selectedSpecCVC = 0
                specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                specListCLN.reloadData()
                
            }
            else if (specificationView.frame.intersects(containerHalf)) {
                // Do work here
                print("view is visible-specificzs")
                selectedSpecCVC = 1
                specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                specListCLN.reloadData()
                
            }/*else if (vehiclePriceViewRect.intersects(containerHalf)) {
                // Do work here
                print("view is visible-vehicleprice")
                selectedSpecCVC = 3
                specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                specListCLN.reloadData()
                
            }*/else if (vehicleGalleryViewRect.intersects(containerHalf)) {
                // Do work here
                print("view is visible-gallery")
                selectedSpecCVC = 4
                specListCLN.scrollToItem(at: IndexPath(item: selectedSpecCVC, section: 0), at: .centeredHorizontally, animated: true)
                specListCLN.reloadData()
            }


    }
//        else if (specFeatureView?.frame.intersects(containerHalf))! {
//            // Do work here
//            print("view is visible")
//            selectedSpecCVC = 2
//            specListCLN.reloadData()
//
//        }
        
        
//        print("scrollViewHeight" + scrollViewHeight.description)
//        print("scrollContentSizeHeight" + scrollContentSizeHeight.description)
//        print("scrollOffset" + scrollOffset.description)
//
//        print("scrollView.contentOffset.y" + scrollView.contentOffset.y.description)
//        print("scrollView.frame.size.height" + scrollView.frame.size.height.description)
//        print("scrollView.frame.size.height" + scrollView.frame.size.height.description)
//
//        let gal = specTBL.convert(CGPoint.zero, from: gallaryView)
//        let gal2 = specTBL.convert(CGPoint.zero, to: gallaryView)
//        print("gal" + gal.debugDescription)
//        print("gal2" + gal2.debugDescription)
        
    }
        */
    }
    
}


extension PLNewCarDetailVC {
    
    private func addToFavList(positionId:Int, sender:UIButton)  {
        sender.isUserInteractionEnabled = false
        BaseThread.asyncMain {
            let id = PLAppState.session.userID
            let carId =  dataSource.carListData[self.ind].id
            var parameterAddFavList = [String:Int]()
            parameterAddFavList["userid"] = id
            parameterAddFavList["newcarid"] = Int(carId)
            
            WebServices.postMethodWithoutActivity(url:"addtonewcarfavlist" ,
                                      parameter: parameterAddFavList,
                                      CompletionHandler:
                { (isFetched, result) in
                    sender.isUserInteractionEnabled = true
                    if isFetched
                    {
                        
                        let id = PLAppState.session.userID
                        self.getFavCount(uidFor:id)
                    }
                    else
                    {
                        
                        
                        
                    }
            })
            
            
            
        }
        
        
    }
    
    
    private func removeFromFavList(positionId:Int, sender:UIButton) {
        sender.isUserInteractionEnabled = false
        BaseThread.asyncMain {
            let id = UserDefaults.standard.value(forKey: "UserID") as! Int
            let carId = dataSource.carListData[self.ind].id
            var parameterAddFavList = [String:Int]()
            parameterAddFavList["userid"] = id
            parameterAddFavList["newcarid"] = Int(carId)
            
            WebServices.postMethodWithoutActivity(url:"deletenewcarfavlist" ,
                                      parameter: parameterAddFavList,
                                      CompletionHandler:
                { (isFetched, result) in
                    sender.isUserInteractionEnabled = true
                    if isFetched
                    {
                        let id = PLAppState.session.userID
                        self.getFavCount(uidFor:id)
                    }
                    else
                    {
                        
                        showBanner(message: "Oops error..!",color:.red)
                    }
            })
            
        }
        
    }
        
        
    private func getNewCarsDetail()
    {
        
        BaseThread.asyncMain
            {
                
                let id = UserDefaults.standard.value(forKey: "UserID") as! Int
                self.parameterNewCarDetail["userid"] = id
                self.parameterNewCarDetail["newcarid"] = self.newCarID
                
                
                WebServices.postMethodWithErrorValue(url:"getNewCarDetails" ,
                                         parameter: self.parameterNewCarDetail,
                                         CompletionHandler:
                    { (isFetched, result) in
                        if isFetched
                        {
                            
                            print(self.parameterNewCarDetail)
                            
                            
                            guard let data = result["data"] as? [String:Any] else { return }
                            print(data)
                            
                            
                                let temp = PLNewcarsDetailModel(dictionary: data)
                                    print(temp)
                            print(temp.variants[0].safety)
                            self.detailDataModel = temp
                               // self.dataSource.carListData.append(contentsOf: temp.carListData)
                        
                            self.selectedVarinatType = temp.variants[0].name
                            for i in 0..<self.detailDataModel.variants.count {
                            self.variantList[i] = self.detailDataModel.variants[i].variantname
                            self.featureArray.append(self.detailDataModel.variants[i].features)
                            }
                            self.setupViewForVariant(type: self.selectedVarinatType)
                            
                            
                            
                            
                        
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            
                            print(result)
                            if let errorMessage = result["message"] as? String {
                                showBanner(message: errorMessage.debugDescription,color:.red)
                            }
                        }
                })
                
                
                
        }
    }
}

extension PLNewCarDetailVC {
    
    func setupShowRoomPrice(type:String) {
        var exShoowRoomPrice =  PLNewcarsDetailModel.Variant.VehiclePrice()
        for i in 0..<detailDataModel.variants.count {
            if detailDataModel.variants[i].variantname == type {
                exShoowRoomPrice = detailDataModel.variants[i].vehicle_price!
                break
            }
        }
       // varaintLbl.text = selectedVarinatType
      //  modelLbl.text = variantData.name
        
        // PriceOfLBL
        mettalicPriceLBL.text = ": ₹ " + (exShoowRoomPrice.metalic_price)
        nonMetallicPrice.text = ": ₹ " + (exShoowRoomPrice.metalic_price)
        
    }
    
    func setupViewForVariant(type:String) {
       
        for i in 0..<detailDataModel.variants.count {
           if detailDataModel.variants[i].variantname == type {
                self.variantData = detailDataModel.variants[i]
                break
            }
        }
        
        print(self.variantData)
        print(variantData.safety)
        let tempID = variantData.id
        for i in 0..<variantData.vehicle_images.count{
            let a = variantData.vehicle_images[i]
            
            if a.id == tempID {
                gallaryImgArray = a.urls
            }
            
        }
        for i in 0..<variantData.vehicle_videos.count{
            let a = variantData.vehicle_videos[i]
            
            if a.id == tempID {
                gallaryVideoArray = a.urls
            }
            
        }
        
        
        //navigation
        
        titleCarLBL.text = variantData.name + " " +  variantData.variantname
        favCountLBL.text =  "-1"//variantData.

        //overView
        if variantData.isfavourite {
            favBTN.setImage(#imageLiteral(resourceName: "Liked"), for: .normal)
        } else {
            favBTN.setImage(#imageLiteral(resourceName: "1x_0007_favourite_gre_outline"), for: .normal)
        }
        carNameLBL.text = variantData.name
       
        
            let imagePath = variantData.vehicle_images[0].urls[0]
             carIMG.kf.setImage(with: URL(string: imagePath))
        
       
        priceLBL.text =  "Ex Showroom Price ₹" + variantData.start_prize + "₹" +  variantData.end_prize
//        if detailDataModel.isnexa == "1" {
//            poweredbyView.isHidden = false
//        }
//        else {
//            poweredbyView.isHidden = true
//        }
        
        
        //specification
        mileageLBL.text = variantData.mileage
        fuelTypeLBL.text = variantData.fuel_type
        transmissionTypeLBL.text = variantData.transmission
        engineTypeLBL.text = variantData.engine_capacity
        
        
        
       // dimensionKeys = variantData.dimension.ke
        
    
         performanceKeys = Array(variantData.performance.keys)
        suspensionKeys =  Array(variantData.suspension.keys)//[variantData.suspension.keys]
        // brakesKeys = [variantData.bra.keys]()
         dimensionKeys = Array(variantData.dimension.keys)//[variantData.dimension.keys]()
         capacityKeys = Array(variantData.capacity.keys)//[variantData.capacity.keys]()
         weightKeys = Array(variantData.weight.keys)//[variantData.weight.keys]()
         safetyKeys = Array(variantData.safety.keys)//[variantData.safety.keys]()
        featureKeys = Array(featureArray[0].keys)
        
        
        specTBL.reloadData()
        colorCLN.reloadData()
        variantCLN.reloadData()
        imageCLN.reloadData()
        videoCLN.reloadData()
        
        
        //Showroom Price
        
        //varaintLbl.text = selectedVarinatType
        //modelLbl.text = variantData.name
        
       // PriceOfLBL
        mettalicPriceLBL.text = ": ₹ " + (variantData.vehicle_price?.metalic_price)!
        nonMetallicPrice.text = ": ₹ " + (variantData.vehicle_price?.metalic_price)!
        
        gallaryImageLBL.text = "Images (" + gallaryImgArray.count.description + ")"
        gallaryVideoLBL.text = "Videos (" + gallaryVideoArray.count.description + ")"
        
        
        titleCarLBL.text = "Maruti Susiki " + variantData.name + " " + variantData.variantname
        
    }
    
    
    
    
    
    
    func getFavCount(uidFor:Int)
    {
        
        WebServices.postMethodWithoutActivity(url:"getNewCarsFav_Count", parameter: ["user_id":uidFor] ,
                                              CompletionHandler:
            { (isFetched, result) in
                
                //VarientSource = [varientDetails]()
                
                guard let dt = result["data"] as? [String:Any] else { return }
                guard let cou = dt["count"] as? Int else { return }
                
                if cou >= 10
                {
                  self.favCountLBL.isHidden = false
                  self.favCountLBL.text = "9+"
                }
                else if cou == 0{
                    self.favCountLBL.isHidden = true
                }else{
                   self.favCountLBL.isHidden = false
                   self.favCountLBL.text = "\(cou)"
                }
            
                
                
                
        })
        
    }
    
    
}


extension String {
    
    func dispayString() -> String {
        let temp = self.replacingOccurrences(of: "_", with: " ")
        return temp.capitalized
    }
    
    func changeToPrice()-> String {
        let temp =  Float(self)!/1000000
        return String(format: "%.2f", temp)
    }
   
    func changeToPriceInLakhs()-> String {
        let temp =  Float(self)!/100000
        if temp >= 1{
            let formatedString = String(format: "%.2f", temp)
            return "₹ " + formatedString + " Lakh"
        }else{
            return "₹ " + self
        }
    }
    
    
}



extension PLNewCarDetailVC {



}



