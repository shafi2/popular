//
//  PLNewCarListVC.swift
//  Popular
//
//  Created by admin on 11/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import CHIPageControl
import CoreData
import Kingfisher
import Alamofire

var dataSource = NewCarListDataModel()

var isNewCarFav = [""]

class PLNewCarsSliderImagesTVC: UITableViewCell {
    @IBOutlet var slideShow: ImageSlideshow!
    @IBOutlet var searchTF: UITextField!
    @IBOutlet var pageControll: CHIPageControlJaloro!
}


//MARK:- UsedCarsListTVC
class PLNewCarsListTVC: UITableViewCell {
    
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var addTofavouriteBTN: BaseButton!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet weak var nexaImage: UIImageView!
    @IBOutlet var favImage: UIImageView!
    
    
}


class PLNewCarListVC: UIViewController {
    
    @IBOutlet var vehicleTV: UITableView!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTF: UITextField!
    @IBOutlet weak var favroiteCountLbl: BaseLabel!
    
    
    @IBOutlet var emptyListIndicatorLBL: UILabel!
    var isSearchMode:Bool = false
    
    var co = 0
    
    fileprivate var slideShowsource: [ImageSource] = []
    fileprivate var currentSlideShowIndex: Double = 0
    fileprivate var searchWord: String = ""
    
    var parameterNewCarList = [String:String]()
    
    var originalData = NewCarListDataModel()
    
    var carSlidingImagesSource: [UIImage] = []
    var isCurrentlyTypingSearch = false
    var currentPage = 1
//    var isLoading = false
    var lastData = 0
    var imageArray = [String]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //var banner = Banner(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    override func viewWillAppear(_ animated: Bool) {
        
        co = 0
        self.searchWord = ""
        self.searchTF.text = ""
        if ifdatainDb()
        {
            self.setDataFrom()
            self.getNewCarsList()
            
        }
        else
        {
            if isNetworkAvailabel() {
                self.getNewCarsList()
            }
            else
            {
                showBanner(title: "Network Error", message: "No internet connectivity")
            }
            
            
        }
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            
            if isNetworkAvailabel() {
                
             self.getFavCount(uidFor:id)
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
            
           
        }else{
            favroiteCountLbl.isHidden = true
        }
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vehicleTV.addSubview(self.refreshControl)
        self.favroiteCountLbl.isHidden = false
        self.favroiteCountLbl.layer.cornerRadius = self.favroiteCountLbl.frame.height / 2
        
       NotificationCenter.default.addObserver(self, selector: #selector(uerLoggedIn), name: .loggedIn, object: nil)
        
    }
    
    @objc func uerLoggedIn(){
        print("user Loged in")
        
        let id = PLAppState.session.userID
        if id != 0
        {
            getNewCarsList()
            getFavCount(uidFor:id)
        }else{
            favroiteCountLbl.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpInterface() {
        let slideShowImages = [#imageLiteral(resourceName: "maruti_suzuki_ritz.jpg"),#imageLiteral(resourceName: "RITZsuperior-white.jpg"),#imageLiteral(resourceName: "maruti_suzuki_ritz.jpg"),#imageLiteral(resourceName: "RITZsuperior-white.jpg"),#imageLiteral(resourceName: "maruti_suzuki_ritz.jpg")]
        for image in slideShowImages {
            slideShowsource.append(ImageSource.init(image: image))
        }
    }
    
    func showHideListEmptyIndicator(){
        if dataSource.carListData.isEmpty{
            DispatchQueue.main.async {
                self.emptyListIndicatorLBL.isHidden = false
            }
        }else{
            DispatchQueue.main.async {
                self.emptyListIndicatorLBL.isHidden = true
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        // Simply adding an object to the data source for this example
        print("refresh")
        //refreshData()
        refreshControl.endRefreshing()
    }

    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: Editing Start
    
    @IBAction func editingStarted(_ sender: UITextField) {
        self.isCurrentlyTypingSearch = true
    }
    
    
    //Editing End
    @IBAction func searchEdited(_ sender: UITextField)
    {
        self.isCurrentlyTypingSearch = false
        if let text = sender.text, text.isNotEmpty, text != "", text != " "{
            searchWord = text
            DispatchQueue.main.async {
                self.searchTF.text = self.searchWord
            }
            print("Searching for : ",searchWord)
            
            if searchWord.count > 0
            {
                isSearchMode = true
                self.searhCars()
            }
            else
            {
               setDataFrom()
            }
        }else{
            self.isSearchMode = false
            DispatchQueue.main.async {
                self.searchView.isHidden = true
                self.searchTF.text = ""
            }
            searchWord = ""
            dataSource.carListData = originalData.carListData
            self.vehicleTV.reloadData()
            showHideListEmptyIndicator()
        }
        
    }
    
    
    
    
    @IBAction func FavListTapped(_ sender: UIButton) {
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                let nextVC =  storyBoardFavourites?.instantiateViewController(withIdentifier: "PLFavouritesVC") as! PLFavouritesVC
                favouritesListType = .newCars
                self.present(nextVC, animated: true, completion: nil)
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        
        
        
        
        
        
        
    }
    
    
}

extension PLNewCarListVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, text.isNotEmpty, text != "", text != " "{
            searchWord = text
            DispatchQueue.main.async {
                self.searchTF.text = self.searchWord
            }
            print("Searching for : ",searchWord)
            
            if searchWord.count > 0
            {
                isSearchMode = true
                self.searhCars()
            }
            else
            {
                setDataFrom()
            }
        }else{
            self.isSearchMode = false
            DispatchQueue.main.async {
                self.searchView.isHidden = true
                self.searchTF.text = ""
            }
            searchWord = ""
            dataSource.carListData = originalData.carListData
            self.vehicleTV.reloadData()
            showHideListEmptyIndicator()
        }
        textField.resignFirstResponder()
        return true
    }
}


extension PLNewCarListVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataSource.carListData.count == 0
        {
            
            self.vehicleTV.backgroundColor = .white
        }
        else
        {
            
            self.vehicleTV.backgroundColor = .clear
        }
        if isSearchMode == false{
            return dataSource.carListData.count + 1
        }else{
            return dataSource.carListData.count
        }
       // return dataSource.carListData.count == 0 ? 0 : dataSource.carListData.count + 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0, isSearchMode == false { //tableView.numberOfRows(inSection: 0) == (dataSource.carListData.count + 1)
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLNewCarsSliderImagesTVC", for: indexPath) as? PLNewCarsSliderImagesTVC else { return UITableViewCell() }
            var carImagesInputs: [InputSource] = []
            carImagesInputs.append(ImageSource(image: UIImage(named:"placeholder_Image")!))

            
            for i in self.imageArray{
                
                getImage(path: i, CompletionHandler: { (flag, img)  in
                    if flag {
                        carImagesInputs.append(ImageSource(image: img!))
                    } else {
                        
                        let imageViewTemp = UIImageView()

                        imageViewTemp.kf.setImage(with: URL(string:i), placeholder:#imageLiteral(resourceName: "placeholder_Image.jpg") , options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
                            //propertyImagesInputs.append(ImageSource(image: image!))
                            
                            if let a1 = imageViewTemp.image
                            {
                                self.carSlidingImagesSource.append(a1)
                                carImagesInputs.append(ImageSource(image: a1))
                            }
                            
                            
                            
                            //cell.slideShow.setImageInputs(propertyImagesInputs)
                        })
//                        let imageViewTemp = UIImageView()
//                        imageViewTemp.kf.setImage(with: URL(string: i), options: nil, progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
//                            self.carSlidingImagesSource.append(imageViewTemp.image!)
//
//                            print("ImgesSlider",image as Any)
//                            carImagesInputs.append(ImageSource(image: image!))
                            //cell.slideShow.setImageInputs(propertyImagesInputs)
                            
//                        })
                    }
                })
            }
            carImagesInputs.remove(at: 0)
            cell.searchTF.text = searchWord
            cell.pageControll.numberOfPages = carImagesInputs.count
            cell.pageControll.progress = currentSlideShowIndex
            //cell.slideShow.draggingEnabled = true
            DispatchQueue.main.async{
                cell.slideShow.setImageInputs(carImagesInputs)
            }
            cell.slideShow.slideshowInterval = 5
            cell.slideShow.contentScaleMode = .scaleToFill
            cell.slideShow.currentPageChanged = { index in
                self.currentSlideShowIndex = Double(index)
                cell.pageControll.progress = self.currentSlideShowIndex
            }
            cell.slideShow.layer.cornerRadius = 5
            
        
            cell.setNeedsLayout()
           
            return cell
            
        }else{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLNewCarsListTVC", for: indexPath) as? PLNewCarsListTVC else { return UITableViewCell() }

            var data = dataSource.carListData[0]
            if isSearchMode == false { //tableView.numberOfRows(inSection: 0) == (dataSource.carListData.count + 1)
                data = dataSource.carListData[indexPath.row-1]
            }else{
                data = dataSource.carListData[indexPath.row]
            }
            
            cell.addTofavouriteBTN.indexPath = indexPath
            cell.addTofavouriteBTN.tag = indexPath.row
            
            cell.modelLBL.text = data.name
            
            cell.priceLBL.text = "Ex Showroom Price ₹ " + data.price
            print("Price ",data.price)
            cell.addTofavouriteBTN.addTarget(self, action: #selector(addfv(_:)), for: .touchUpInside)
           // cell.addTofavouriteBTN.tag = indexPath.row
            //isNewCarFav.append("0")
            
            print("Name:\(data.name)","fav:\(data.isFav)")
            
            
            
            if data.isFav == 0
            {
                cell.favImage.image = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline.png")
            }
            else
            {
                 cell.favImage.image = #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")

            }
        
            
            
            
            
            if data.isnexa == "1" {
                cell.nexaImage.isHidden = false
            } else {
                 cell.nexaImage.isHidden = true
            }
            
           if data.colorImageas.count > 0
           {
             let imageUrl = imageURL + data.colorImageas[0].url
            
             print("car name " + data.name)
             print(imageUrl)
            
            
            cell.vehicleImage.kf.setImage(with: URL(string:imageUrl), options: [.requestModifier(getImageAuthorization())])
            }
            else
           {
            
              cell.vehicleImage.kf.setImage(with: URL(string:""))
            
            }
            

            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isSearchMode == false{
            return 0
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        return view
    }
    
    
    
    @objc func addfv(_ sender: UIButton)
    {
        
        
        let id = PLAppState.session.userID
        
        if id == 0
        {
           //showBanner(message: "Please login",color:.red)
            
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
            
        }
        else
        {
            let indP = IndexPath(row: sender.tag , section: 0)
            
            // self.vehicleTV.reloadRows(at: [indP], with: .none)
            
            let c = vehicleTV.cellForRow(at: indP) as! PLNewCarsListTVC
            print("Current cell for favourite : ",indP.description)
            //c.favImage.image = #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")
            
            if c.favImage.image == #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")
            {
                
                
                if isNetworkAvailabel() {
                    DispatchQueue.main.async {
                        c.favImage.image = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline")
                    }
                    
                    self.removeFromFavList(positionId: indP.row, sender: sender)
                }
                else
                {
                    
                    showBanner(title: "Network Error", message: "No internet connectivity")
                    
                }
                
                
            }
            else
            {
              
                
                if isNetworkAvailabel() {
                    DispatchQueue.main.async {
                        c.favImage.image = #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")
                    }
                    self.addToFavList(positionId: indP.row, sender: sender)
                }
                else
                {
                    
                    showBanner(title: "Network Error", message: "No internet connectivity")
                    
                }
                
            }
        }
        
        
        


        
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//         lastData = self.dataSource.carListData.count
//        if !isLoading && indexPath.row == lastData {
//            print(currentPage)
//            print(lastData)
//            print(dataSource.carListData.count)
//            currentPage += 1
//            self.getNewCarsList()
//        }
//    }
//
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if isLoading && indexPath.row == lastData {
//            isLoading = false
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //heightRatio > 175 ? heightRatio : 175
        //        view.frame.height * 0.385
        
        return  UITableViewAutomaticDimension //indexPath.row == 0 ? 236 : 254//260
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // condition - let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? PLNewCarsListTVC
            if tableView.numberOfRows(inSection: indexPath.section) == dataSource.carListData.count{
                let temp = dataSource.carListData[indexPath.row]
                print("Cat Name ",temp.name)
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLNewCarDetailVC") as! PLNewCarDetailVC
                
                if dataSource.carListData[indexPath.row].colorImageas.count != 0
                {
                    nextVC.ImageString = dataSource.carListData[indexPath.row].colorImageas[0].url
                }
                
                nextVC.CarName = dataSource.carListData[indexPath.row].name
                nextVC.carPrice = dataSource.carListData[indexPath.row].price
                nextVC.isNex = dataSource.carListData[indexPath.row].isnexa
                nextVC.ind = indexPath.row
                nextVC.isFav = dataSource.carListData[indexPath.row].isFav
                
                self.present(nextVC, animated: true, completion: nil)
            }else{
                if indexPath.row != 0{
                    _ = dataSource.carListData[indexPath.row-1]
                    
                    let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLNewCarDetailVC") as! PLNewCarDetailVC
                    
                    if dataSource.carListData[indexPath.row-1].colorImageas.count != 0
                    {
                        nextVC.ImageString = dataSource.carListData[indexPath.row-1].colorImageas[0].url
                    }
                    
                    nextVC.CarName = dataSource.carListData[indexPath.row-1].name
                    nextVC.carPrice = dataSource.carListData[indexPath.row-1].price
                    nextVC.isNex = dataSource.carListData[indexPath.row-1].isnexa
                    nextVC.ind = indexPath.row-1
                    nextVC.isFav = dataSource.carListData[indexPath.row-1].isFav
                    
                    self.present(nextVC, animated: true, completion: nil)
                }
            }
//                let temp = dataSource.carListData[indexPath.row]
//
//                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLNewCarDetailVC") as! PLNewCarDetailVC
//
//                if dataSource.carListData[indexPath.row].colorImageas.count != 0
//                {
//                    nextVC.ImageString = dataSource.carListData[indexPath.row].colorImageas[0].url
//                }
//
//                nextVC.CarName = dataSource.carListData[indexPath.row].name
//                nextVC.carPrice = dataSource.carListData[indexPath.row].price
//                nextVC.isNex = dataSource.carListData[indexPath.row].isnexa
//                nextVC.ind = indexPath.row
//                nextVC.isFav = dataSource.carListData[indexPath.row].isFav
//
//                self.present(nextVC, animated: true, completion: nil)
    }
    
    func showHideSearchView(){
        
        var shouldHide:Bool = false
        let visibleCells = vehicleTV.visibleCells
        for cell in visibleCells{
            if let _ = cell as? PLNewCarsSliderImagesTVC{
               shouldHide = true
            }
            
        }
        
        if shouldHide{
            DispatchQueue.main.async {
                self.searchView.isHidden = true
            }
        }else{
            DispatchQueue.main.async {
                self.searchView.isHidden = false
            }
        }
        
        /*
        DispatchQueue.main.async {
            if self.isSearchMode == false {  //self.vehicleTV.numberOfRows(inSection: 0) == (dataSource.carListData.count + 1)
                self.searchView.isHidden = true
                //self.vehicleTV.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
            }else{
                self.searchView.isHidden = false
               // self.vehicleTV.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                //searchTF.text = searchWord
            }
        }
 */
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        showHideSearchView()

        _ = abs(scrollView.contentOffset.y + scrollView.frame.size.height).description
        _ = abs(scrollView.contentSize.height).description

//        if (lhs == rhs) {
//            lastData = self.dataSource.carListData.count
////            if !isLoading && indexPath.row == lastData {
////                print(currentPage)
////                print(lastData)
////                print(dataSource.carListData.count)
//                currentPage += 1
//                self.getNewCarsList()
////            }
//        }
//        {
//            if self.AllResult.matchArray.count  == self.AllResult.MachCount
//            {
//                //                    print("NOT called")
//                //                    let banner = Banner(title: "Chavara Matrimony", subtitle: "No data found.", image: nil, backgroundColor: UIColor(red:0.28, green:0.63, blue:0.75, alpha:1.0), didTapBlock: nil)
//                //                    banner.show(duration: 2.0)
//
//            }
//            else
//            {
//                let type = UserDefaults.standard.string(forKey: "userID")
//                self.callMatches(sortString: sortOrder, uid_Array: self.seendUserIds, uid: type!)
//
//                //                    if self.AllResult.matchArray.count  < self.AllResult.MachCount
//                //                    {
//                //                         if seendUserIds.count == self.AllResult.matchArray.count
//                //                         {
//                //                              let type = UserDefaults.standard.string(forKey: "userID")
//                //                              self.callMatches(sortString: sortOrder, uid_Array: self.seendUserIds, uid: type!)
//                //                         }
//                //                    }
//            }
//        }

    }
    
    
    

}




// Mark:- Handling web service
extension PLNewCarListVC {
    private func getNewCarsList()
    {
        
        BaseThread.asyncMain
            {
            let id = PLAppState.session.userID
            print(id)
            self.parameterNewCarList["userid"] = id.description
                self.parameterNewCarList["page"] = self.currentPage.description
                self.parameterNewCarList["search"] = self.searchWord.description
                
                var NewCarUrl = ""
                
                if id == 0
                {
                     NewCarUrl = "get/new_cars"
                }
                else
                {
                    
                    NewCarUrl = "get/new_cars?user_id=\(id)"
                }
                
            
                WebServices.getMethodWithOutActivity(url:"\(NewCarUrl)", parameter: ["":""] ,
                       CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        
                        print(self.parameterNewCarList)
                        
                        
                        guard let data = result["data"] as? [String:Any] else { return }
                        guard let ban = data["banner"] as? [[String:Any]] else { return }
                        
                        self.imageArray.removeAll()
                        
                        for i in ban
                        {
                            let imagesL = i["images"] as! String
                            
                            self.imageArray.append(imageURL + imagesL)
                        }

                       let temp = NewCarListDataModel().getNewCarList(dataDict: data)
                       dataSource.carListData.removeAll()
                       dataSource.carListData.append(contentsOf: temp.carListData)
                       self.originalData.carListData.removeAll()
                       self.originalData.carListData.append(contentsOf: temp.carListData)
                       //self.setupSlider()
                       //self.vehicleTV.reloadData()
                        if self.ifdatainDb()
                        {
                            self.deleteAllRecords()
                         
                            self.SaveToDatafrom(dSour:data, imgUrl: self.imageArray)
                        }
                        else
                        {
                            self.deleteAllRecords()
                            
                            self.getDatafrom(dSour:data, imgUrl: self.imageArray)
                        }
                    }
                    else
                    {
                        print(result)
                        if let errorMessage = result["message"] as? String {
                            showBanner(message: errorMessage.debugDescription,color:.red)
                        }
                    }
            })
      
        }
    }
    
    
    func getDatafrom(dSour:[String:Any],imgUrl:[String])
    {
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dSour, options: .prettyPrinted)
            let jsonBannerData = try JSONSerialization.data(withJSONObject: imgUrl, options: .prettyPrinted)
            
            let newCars =  NewCarList(context: self.context)
            let ban = Banner(context: self.context)
            
            ban.image = jsonBannerData
            newCars.carid = 1
            newCars.jsonstr = jsonData
            try self.context.save()
            self.setDataFrom()
        }catch{
            print(error)
        }
        
    }
    
    
    func SaveToDatafrom(dSour:[String:Any],imgUrl:[String])
     {
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dSour, options: .prettyPrinted)
            let jsonBannerData = try JSONSerialization.data(withJSONObject: imgUrl, options: .prettyPrinted)

            let newCars =  NewCarList(context: self.context)
            let ban = Banner(context: self.context)
            
            ban.image = jsonBannerData
            
            
            newCars.carid = 1
            newCars.jsonstr = jsonData
            self.deleteAllRecords()
            try self.context.save()
            self.setDataFrom()
        }catch{
            print(error)
        }
     }
    
    
    
    func setDataFrom()
    {
        do{
            let tasks1 = try context.fetch(NewCarList.fetchRequest())
            let tasks2 = try context.fetch(Banner.fetchRequest())
            
            let dd = tasks1[0] as! NewCarList
            let dd2 = tasks2[0] as! Banner
           
            print(tasks2)
            let dda = dd.jsonstr
            let ddb = dd2.image
           
            let jj = try JSONSerialization.jsonObject(with: dda!, options: []) as? [String:Any]
            let jj1 = try JSONSerialization.jsonObject(with: ddb!, options: []) as? [String]

            if jj == nil || jj1 == nil {
                return
            }
            
            let temp = NewCarListDataModel().getNewCarList(dataDict: jj!)
            self.imageArray = jj1!
            
            dataSource.carListData.removeAll()
            dataSource.carListData.append(contentsOf: temp.carListData)
  
//             for i in dataSource.carListData
//             {
//                 if i.isFav == 1
//                 {
//                  co = co + 1
//                }
//            }
//
//             self.favroiteCountLbl.text = "\(co)"
            
            self.setupSlider()
            if isCurrentlyTypingSearch == false{
                self.vehicleTV.reloadData()
            }
            
            print(dataSource.carListData.count)
            print(temp.carListData[0].fuel_type)
            
            //self.getNewCarsList()
            //self.getVarientDummy(idFor:1)
            
            
        }
        catch
        {
            print(error)
        }
        
    }
    
    func ifdatainDb() -> Bool
    {
        
        var temp = NewCarListDataModel()
        
        do{
        
        let tasks1 = try context.fetch(NewCarList.fetchRequest())
        
          
         let cc = tasks1.count
            
         if cc > 0
         {
            let dd = tasks1[0] as! NewCarList
            
            let dda = dd.jsonstr
            
            let jj = try JSONSerialization.jsonObject(with: dda!, options: []) as? [String:Any]
            
            if jj != nil {
                
                return true
            }
            
            temp = NewCarListDataModel().getNewCarList(dataDict: jj!)
            
            print(temp.carListData[0].fuel_type)
         }
        else
         {
            return false
         }
        
        
            
          
        }
        catch
        {
            print(error)
        }
            
        
        if temp.carListData.count > 0
        {
          return true
        }
        
        return false
        
    }
    
    
    
    

    
//    func refreshData() {
//        BaseThread.asyncMain
//            {
//                let id = PLAppState.session.userID
//                self.parameterNewCarList["userid"] = id.description
//                self.parameterNewCarList["page"] = 0.description
//
//                WebServices.postMethod(url:"getNewCarList" ,
//                           parameter: self.parameterNewCarList,
//                           CompletionHandler:
//                    { (isFetched, result) in
//                        if isFetched
//                        {
//
//                            print(self.parameterNewCarList)
//
//
//                            let data = result["data"] as! [String:Any]
//                            print(data)
//
//                            dataSource.banner.removeAll()
//                            dataSource.carListData.removeAll()
//                            dataSource.favouritecount = 0
//                            self.vehicleTV.reloadData()
//                            dataSource = NewCarListDataModel().getNewCarList(dataDict: data)
//
//                            print(dataSource.carListData)
//
//
//                            self.setupSlider()
//
//                            self.vehicleTV.reloadData()
//
//                           // self.scrollViewDidScroll(self.vehicleTV)
//
//                        }
//                        else
//                        {
//                            showBanner(message: "Oops error..!",color:.red)
//                        }
//                })
//
//
//
//        }
//
//    }
    
    func searhCars() {
        DispatchQueue.main.async {
            self.searchView.isHidden = false
        }
        
      //  let filteredSource = dataSource.carListData
        
       // print(filteredSource.count ,dataSource.carListData.count )
       
//            if searchWord == "" {
//                dataSource = dataSourceTmp
//            }else{
        
               // let filtered = filteredSource.filter { $0.name.lowercased().contains(searchWord.lowercased()) }
        dataSource.carListData = originalData.carListData
        let filteredData = dataSource.carListData.filter {
            print("Find Word : ",$0.name.lowercased(),"Search Word : ",searchWord.lowercased())
            return $0.name.lowercased().contains(searchWord.lowercased())
        }
        print("Filter Count Before : ",filteredData.count)
                 dataSource.carListData.removeAll()
                dataSource.carListData = filteredData
        print("Filtered Count After : ",filteredData.count)
                print(dataSource.carListData.map({$0.name}))
           // }
        showHideListEmptyIndicator()
        DispatchQueue.main.async {
           self.vehicleTV.reloadData()
        }
    }
    
    
    
    private func addToFavList(positionId:Int, sender: UIButton)  {
        sender.isUserInteractionEnabled = false
        BaseThread.asyncMain {
            let id = PLAppState.session.userID
            let carId = self.vehicleTV.numberOfRows(inSection: 0) == dataSource.carListData.count ? dataSource.carListData[positionId].id : dataSource.carListData[positionId - 1].id
            var parameterAddFavList = [String:Int]()
            parameterAddFavList["userid"] = id
            parameterAddFavList["newcarid"] = carId

            WebServices.postMethodWithoutActivityAndMessage(url:"addtonewcarfavlist" ,
                       parameter: parameterAddFavList,
                       CompletionHandler:
                { (isFetched, result) in
                    sender.isUserInteractionEnabled = true
                    if isFetched
                    {
                        self.updatefavCount()
                        _ = IndexPath(row: positionId, section: 0)
                        print(result)
                     print(positionId+1)
                        
                        if self.vehicleTV.numberOfRows(inSection: 0) == dataSource.carListData.count{
                            dataSource.carListData[positionId].isFav = 1
                        }else{
                            dataSource.carListData[positionId - 1].isFav = 1
                        }
                        
//                        dataSource.carListData[positionId - 1].isFav = 1
//                        print(dataSource.carListData[positionId - 1].name)
                        
                        _ = IndexPath(item: positionId, section: 0)
                        DispatchQueue.main.async {
                          //  self.vehicleTV.reloadRows(at: [indexPath], with: .none)
                        }
                    }
                    else
                    {
                        //showBanner(message: "Oops error..!",color:.red)
                        //self.dataSource.carListData[positionId].isfavourite = false
                        
                       // print(dataSource.carListData[positionId - 1].name)

                    }
            })



        }


    }
    
    
    private func removeFromFavList(positionId:Int, sender: UIButton) {
        sender.isUserInteractionEnabled = false
        BaseThread.asyncMain {
            let id = PLAppState.session.userID
            let carId = self.vehicleTV.numberOfRows(inSection: 0) == dataSource.carListData.count ? dataSource.carListData[positionId].id : dataSource.carListData[positionId - 1].id
            var parameterAddFavList = [String:Int]()
            parameterAddFavList["userid"] = id
            parameterAddFavList["newcarid"] = carId

            WebServices.postMethodWithoutActivityAndMessage(url:"deletenewcarfavlist" ,
                       parameter: parameterAddFavList,
                       CompletionHandler:
                { (isFetched, result) in
                    sender.isUserInteractionEnabled = true
                    if isFetched
                    {
                        self.updatefavCount()
                        //self.dataSource.carListData[positionId].isfavourite = false
                        //self.vehicleTV.reloadRows(at: [IndexPath(row: positionId+1, section: 0)], with: .none)
                        
                        if self.vehicleTV.numberOfRows(inSection: 0) == dataSource.carListData.count{
                            dataSource.carListData[positionId].isFav = 0
                        }else{
                            dataSource.carListData[positionId - 1].isFav = 0
                        }
                        
//                        dataSource.carListData[positionId - 1].isFav = 0
//                        print(dataSource.carListData[positionId - 1].name)
                        _ = IndexPath(item: positionId, section: 0)
                        DispatchQueue.main.async {
                           // self.vehicleTV.reloadRows(at: [indexPath], with: .none)
                        }
                        
                        
                           //self.vehicleTV.reloadData()
                    }
                    else
                    {
                      //  showBanner(message: "Oops error..!",color:.red)
                        //self.dataSource.carListData[positionId].isfavourite = true

                    }
            })



        }


    }
    
    func updatefavCount() {
        let id = PLAppState.session.userID
        
        if id != 0
        {
            self.getFavCount(uidFor:id)
        }else{
            self.favroiteCountLbl.isHidden = true
        }
        
        

    }
    
    func setupSlider() {
        
        
        
        var propertyImagesInputs: [InputSource] = []
        
        print(self.imageArray.count)
        
        if self.imageArray.count != 0
        {
        
        for i in self.imageArray{
            
            if let source = KingfisherSource(urlString:"\(i)", placeholder: nil, options: [.requestModifier(getImageAuthorization())]){
                propertyImagesInputs.append(source)
                print("propertyImagesInputs.append(source)")
            }

            
            print(i)
//            let imageViewTemp = UIImageView()
//            imageViewTemp.kf.setImage(with: URL(string:i), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
//
//
//                if let a1 = imageViewTemp.image
//                {
//
//                    propertyImagesInputs.append(ImageSource(image: a1))
//                }
//
//
//
//            })
            
            
            
//            imageViewTemp.kf.setImage(with: URL(string: i), options: nil, progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
//                print(err)
//                print(image)
//                print()
//
//                self.carSlidingImagesSource.append(imageViewTemp.image!)
//
//                slideShowsource.append(ki)
//
//                print("ImgesSlider",image as Any)
//                propertyImagesInputs.append(ImageSource(image: image!))
//                //cell.slideShow.setImageInputs(propertyImagesInputs)
//
//            })
//
        }
        
    }
        
        guard let cell = self.vehicleTV.dequeueReusableCell(withIdentifier: "PLUsedCarsSliderImagesTVC") as? PLUsedCarsSliderImagesTVC else { return }
        
        cell.searchTF.text = self.searchWord
        cell.pageControll.numberOfPages = propertyImagesInputs.count
        cell.pageControll.progress = self.currentSlideShowIndex
        cell.slideShow.setImageInputs(propertyImagesInputs)
        cell.slideShow.slideshowInterval = 5
        cell.slideShow.contentScaleMode = .scaleToFill
        cell.slideShow.currentPageChanged = { index in
            self.currentSlideShowIndex = Double(index)
            cell.pageControll.progress = self.currentSlideShowIndex
        }
        
        
        
        
        cell.layoutIfNeeded()
       
    }

    
    
    //MARK:-Delete
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Banner")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        let deleteFetch2 = NSFetchRequest<NSFetchRequestResult>(entityName: "NewCarList")
        let deleteRequest2 = NSBatchDeleteRequest(fetchRequest: deleteFetch2)
        
        do {
            try context.execute(deleteRequest)
            try context.execute(deleteRequest2)
            try context.save()
            print("Successfull")
            
            
        } catch {
            print ("There was an error")
        }
    }
    func deleteData() {
        
        
        // let task = tasks[0]
        // context.delete(task)
        // (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    
    func downloadImage(path:String) {
        ImageDownloader.default.downloadImage(with: URL(string: path)!, options: [], progressBlock: nil) {
            (image, error, url, data) in
            print("Downloaded Image: \(String(describing: image))")
            self.saveImage(image: image!, path: path)
        }
    }
    
    func saveImage(image:UIImage,path:String){
        ImageCache.default.store(image, original: nil, forKey: path, processorIdentifier: "", cacheSerializer: DefaultCacheSerializer.default, toDisk: true, completionHandler: {
           
        })
    }
    
    func getImage(path:String,CompletionHandler:@escaping ((Bool,UIImage?)->())) {
        ImageCache.default.retrieveImage(forKey: path, options: nil) {
            image, cacheType in
            if let image = image {
                print("Get image \(image), cacheType: \(cacheType).")
                CompletionHandler(true, image)
            } else {
                CompletionHandler(false,nil)
                print("Not exist in cache.")
            }
    }
    }
    
    
    func getFavCount(uidFor:Int)
    {
        
        WebServices.postMethodWithoutActivityAndMessage(url:"getNewCarsFav_Count", parameter: ["user_id":uidFor] ,
                                              CompletionHandler:
            { (isFetched, result) in
                
                //VarientSource = [varientDetails]()
                
                
                if isFetched
                {
                    guard let dt = result["data"] as? [String:Any] else { return }
                    guard let cou = dt["count"] as? Int else { return }
                    DispatchQueue.main.async {
                        if cou != 0{
                            
                            self.favroiteCountLbl.isHidden = false
                            if cou > 9{
                                self.favroiteCountLbl.text = "9+"
                            }else{
                                self.favroiteCountLbl.text = "\(cou)"
                            }
                        }else{
                            self.favroiteCountLbl.isHidden = true
                        }
                    }
                    
                }
                else
                {
                   showBanner(title: "Server Error", message: "Something went wrong while connecting")
                }
                
                
                
        })
        
    }
    
    
}









