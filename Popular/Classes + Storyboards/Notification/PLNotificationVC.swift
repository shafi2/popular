//
//  PLNotificationVC.swift
//  Popular
//
//  Created by Appzoc on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLNotificationVC: UIViewController {

    @IBOutlet var collectionRef: UICollectionView!
    @IBOutlet var tableRef: UITableView!
    
    var sectionList:[NotificationSection] = []
    var selectedSection:Int = 0
    
    var allNotificationData:[NotificationDataMain.NotificationData] = []
    var usedCarNotificationData:[NotificationDataMain.NotificationData] = []
    var newCarNotificationData:[NotificationDataMain.NotificationData] = []
    var hrNotificationData:[NotificationDataMain.NotificationData] = []
    
    var currentSource:[NotificationDataMain.NotificationData] = []
    
    var isReloadRequired:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showForceUpdate), name: .forceUpdateNotify, object: nil)
        setUpInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UpdateManager.main.forceUpdateAvailable{
            UpdateManager.main.showUpdateAlert(on: self)
        }
        if isReloadRequired{
            self.setUpWebCall()
            isReloadRequired = false
        }
        self.view.layoutSubviews()
    }
    
    @objc func showForceUpdate(){
        if UpdateManager.main.forceUpdateAvailable{
            UpdateManager.main.showUpdateAlert(on: self)
        }
    }
    
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    func addSwipeToTable(){
        let swipeGestureRight = UISwipeGestureRecognizer()
        swipeGestureRight.direction = .right
        swipeGestureRight.addTarget(self, action: #selector(swipeRight))
        self.tableRef.addGestureRecognizer(swipeGestureRight)
        let swipeGestureLeft = UISwipeGestureRecognizer()
        swipeGestureLeft.direction = .left
        swipeGestureLeft.addTarget(self, action: #selector(swipeLeft))
        self.tableRef.addGestureRecognizer(swipeGestureLeft)
    }
    
    @objc func swipeLeft(){
        //print("Alt Right")
        if selectedSection < sectionList.count - 1, selectedSection >= 0{
            selectedSection += 1
            deselectAllSection()
            sectionList[selectedSection].isSelected = true
            switch selectedSection {
            case 0:
                currentSource = allNotificationData
            case 1:
                currentSource = newCarNotificationData
            case 2:
                currentSource = usedCarNotificationData
            case 3:
                currentSource = hrNotificationData
            default:
                currentSource = allNotificationData
            }
            tableRef.reloadData()
            collectionRef.reloadData()
            let currentIndex = IndexPath(row: selectedSection, section: 0)
            collectionRef.scrollToItem(at: currentIndex, at: .centeredHorizontally, animated: true)
        }
        
    }
    
    @objc func swipeRight(){
      //  print("Alt Left")
        if selectedSection <= sectionList.count - 1, selectedSection > 0{
            selectedSection -= 1
            deselectAllSection()
            sectionList[selectedSection].isSelected = true
            switch selectedSection {
            case 0:
                currentSource = allNotificationData
            case 1:
                currentSource = newCarNotificationData
            case 2:
                currentSource = usedCarNotificationData
            case 3:
                currentSource = hrNotificationData
            default:
                currentSource = allNotificationData
            }
            tableRef.reloadData()
            collectionRef.reloadData()
            let currentIndex = IndexPath(row: selectedSection, section: 0)
            collectionRef.scrollToItem(at: currentIndex, at: .centeredHorizontally, animated: true)
        }
    }
    
    func testDateChecker(){
        let dString = "2018-09-12 10:32:10"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let _ = dateFormatter.date(from: dString){
            //print("Days ",findNumberOfDaysOrHours(from: date))
        }
    }
    
    func setUpInitialData(){
        tableRef.estimatedRowHeight = 250
        DispatchQueue.main.async {
            self.tableRef.contentInset = UIEdgeInsetsMake(10, 0, 0, 0)
        }
        addSwipeToTable()
        
        let allSection = NotificationSection(sectionName: "All", sectionID: 0, isSelected: true)
        let usedCarSection = NotificationSection(sectionName: "Used Car", sectionID: 0, isSelected: false)
        let newCarSection = NotificationSection(sectionName: "New Car", sectionID: 0, isSelected: false)
        self.sectionList = [allSection,newCarSection, usedCarSection]
        if PLAppState.session.isEmployee{
            let hrSection = NotificationSection(sectionName: "HR", sectionID: 0, isSelected: false)
            self.sectionList.append(hrSection)
        }
        self.collectionRef.reloadData()
    }
    
    func selectSection(section:Int){
        self.selectedSection = section
        switch section {
        case 0:
            currentSource = allNotificationData
        case 1:
            currentSource = newCarNotificationData
        case 2:
            currentSource = usedCarNotificationData
        case 3:
            currentSource = hrNotificationData
        default:
            currentSource = allNotificationData
        }
        tableRef.reloadData()
    }
    
    func setUpWebCall(){
        var param:[String:String] = [:]
        if PLAppState.session.userID == 0{
            param = ["user_id":"", "is_employee":PLAppState.session.isEmployee.hashValue.description]
        }else{
            param = ["user_id":PLAppState.session.userID.description, "is_employee":PLAppState.session.isEmployee.hashValue.description]
        }
        
        print(param)
        WebServices.postMethod(url: "getnotification", parameter: param) { (isComplete, json) in
            if isComplete{
                do{
                    let trueJson = json["data"]
                    if let jsonData = serializeJSON(json: trueJson as Any){
                        let decoder = JSONDecoder()
                        let parsedData = try decoder.decode(NotificationDataMain.self, from: jsonData)
                        self.allNotificationData = parsedData.all
                        self.usedCarNotificationData = parsedData.usedcar
                        self.newCarNotificationData = parsedData.newcar
                        self.hrNotificationData = parsedData.employee ?? []
                        self.currentSource = self.allNotificationData
                        DispatchQueue.main.async {
                            self.tableRef.reloadData()
                        }
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    

}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)  == 1 { return "\(years(from: date))Year"   }
        if years(from: date)  > 1 { return "\(years(from: date))Year"   }
        if months(from: date)  == 1 { return "\(months(from: date))Month"  }
        if months(from: date)  > 1 { return "\(months(from: date))Months"  }
        if weeks(from: date)   == 1 { return "\(weeks(from: date))Week"   }
        if weeks(from: date)   > 1 { return "\(weeks(from: date))Weeks"   }
        if days(from: date)    == 1 { return "\(days(from: date))Day"    }
        if days(from: date)    > 1 { return "\(days(from: date))Days"    }
        if hours(from: date)   == 1 { return "\(hours(from: date))Hour"   }
        if hours(from: date)   > 1 { return "\(hours(from: date))Hours"   }
        if minutes(from: date) == 1 { return "\(minutes(from: date))Minute" }
        if minutes(from: date) > 0 { return "\(minutes(from: date))Minutes" }
        if seconds(from: date) == 1 { return "\(seconds(from: date))Second" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))Seconds" }

        return ""
    }
}


