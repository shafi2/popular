//
//  RedeemCashVC.swift
//  Popular
//
//  Created by Appzoc on 05/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class RedeemCashVC: UIViewController,SideOptionsDelegate {
    

    var param = [String:Any]()
    var locArry = [String:Any]()

    @IBOutlet var LocationTF: UITextField!
    private var isLocationGetOngoing: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getLocationWithoutActivity()
    }

    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DismissTapped(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }

        dismiss()
    }
    
    
    @IBAction func LocationBTNTapped(_ sender: UIButton) {
        if locArry.isEmpty, !isLocationGetOngoing {
            getLocation()
        }else {
            if !locArry.isEmpty {
                showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! [[String:Any]], idKey: "id", nameKey: "name", isStringId: false))

            }

        }

    }
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
        //print(data.id,data.name)
        self.LocationTF.text = data.name
        
       // print(data.name,data.id,data.id2)
        
        param["location_id"] = data.id
        
    }
    

    @IBAction func SubmitBTNTapped(_ sender: UIButton) {
        
        if LocationTF.text != ""
        {
           getdata()
        }
        else
        {
            showBanner(title: "", message: "Please select location")
        }
    }
    
    func getLocation()
    {
        WebServices.getMethodWith(url:"api/redeem_location", parameter: [:], CompletionHandler: { (isFetched, result) in
            if isFetched
            {
                self.locArry = result
                
            }
            else
            {
                
                showBanner(title: "Server Error", message: "Something went wrong while connecting")
                
            }
        })
        

    }
    
    func getLocationWithoutActivity()
    {
        
        self.isLocationGetOngoing = true

        WebServices.getMethodWithOutActivity(url:"api/redeem_location", parameter: [:], CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    self.locArry = result
                    
                }
                else
                {
                    
                    
                    
                }
                
                BaseThread.asyncMain {
                    self.isLocationGetOngoing = false
                }
                
        })

    }

    
    func getdata()
    {
        
        param["user_id"] = PLAppState.session.userID
        
        param["type_id"] = 7
        
        WebServices.postMethod(url:"redeemCash", parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    BaseThread.asyncMain {
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        
                        
                        nextVC.dismisType = 0
                        
                        
                        self.present(nextVC, animated: true, completion: nil)

                    }

                }
                else
                {
                    showBanner(title: "Server Error", message: "Something went wrong while connecting")
                }
                
                
                
                
        })
    }
    

}
