//
//  AccidentDentWorkVC.swift
//  Popular
//
//  Created by Appzoc on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker

class AccidentDentWorkVC: UIViewController,SideOptionsDelegate,OpalImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    typealias JsonImage = [String: [UIImage]]

    var selectedImageView = 1
    
    var isnewcarref = false
    @IBOutlet var popUpTypeLB: UILabel!
    @IBOutlet var locationTF: UITextField!
    
    var postParaMeters = [String:Any]()
    //var postParaMetersImage = [String:Any]()
    var imageParam = JsonImage()
    var imArray = [UIImage(),UIImage(),UIImage()]
    var locArry = [String:Any]()
    private var isLocationGetOngoing: Bool = false

    
    @IBOutlet var Image1: UIImageView!
    @IBOutlet var Image2: UIImageView!
    @IBOutlet var Image3: UIImageView!
    
    @IBOutlet var imageCloseBTN1: UIButton!
    @IBOutlet var imageCloseBTN2: UIButton!
    @IBOutlet var imageCloseBTN3: UIButton!
    
//    private let imagePicker1 = OpalImagePickerController()
//    private let imagePicker2 = OpalImagePickerController()
//    private let imagePicker3 = OpalImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popUpTypeLB.text = "Accident/Dent Work"
        Image1.layer.cornerRadius = 4
        Image1.layer.masksToBounds = true
        
        Image2.layer.cornerRadius = 4
        Image2.layer.masksToBounds = true
        
        Image3.layer.cornerRadius = 4
        Image3.layer.masksToBounds = true

//        imagePicker1.imagePickerDelegate = self
//        imagePicker2.imagePickerDelegate = self
//        imagePicker3.imagePickerDelegate = self

        postParaMeters["user_id"] = PLAppState.session.userID.description
        
        
        
        postParaMeters["type_id"] = "6"
        

        
        getLocationWithoutActivity()
        
        
        
        setUI()
        
        // Do any additional setup after loading the view.
    }
    
    func setUI()
    {
        imageCloseBTN1.isHidden = true
        imageCloseBTN2.isHidden = true
        imageCloseBTN3.isHidden = true
        
        Image1.image = UIImage(named:"addImage")
        Image2.image = UIImage(named:"addImage")
        Image3.image = UIImage(named:"addImage")
        
        
        imArray.removeAll()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismisPopUpBT(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }

        dismiss()
    }
    
    @IBAction func selectLocationBT(_ sender: UIButton) {
        if locArry.isEmpty, !isLocationGetOngoing {
            getLocation()
        }else {
            //print(locArry)
            
            if !locArry.isEmpty {
                showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! [[String:Any]], idKey: "id", nameKey: "name", isStringId: false))

            }

        }

        //(fromArray: self.locArry["data"] as! [[String:Any]], isStringId = false)
    }
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
//        print(data.id,data.name)
        self.locationTF.text = data.name
        
        
        postParaMeters["location_id"] = data.id.description
        
    }
    
    
    @IBAction func imageCloseBTNTapped1(_ sender: UIButton) {
        
        imageCloseBTN1.isHidden = true
        Image1.image = UIImage(named:"addImage")
    }
    
    @IBAction func imageCloseBTNTapped2(_ sender: UIButton) {
        
        imageCloseBTN2.isHidden = true
        Image2.image = UIImage(named:"addImage")
    }
    
    @IBAction func imageCloseBTNTapped3(_ sender: UIButton) {
        
        imageCloseBTN3.isHidden = true
        Image3.image = UIImage(named:"addImage")
    }
    
    
    
    @IBAction func uploadImageBTNTapped1(_ sender: UIButton) {
        let imagePicker1 = OpalImagePickerController()
        imagePicker1.imagePickerDelegate = self
        selectedImageView = 1
        imagePicker1.maximumSelectionsAllowed = 1

        present(imagePicker1)
    }
    
    @IBAction func uploadImageBTNTapped2(_ sender: UIButton) {
        let imagePicker2 = OpalImagePickerController()
        imagePicker2.imagePickerDelegate = self

        
        selectedImageView = 2
        imagePicker2.maximumSelectionsAllowed = 1

        present(imagePicker2)
    }
    
    @IBAction func uploadImageBTNTapped3(_ sender: UIButton) {
        let imagePicker3 = OpalImagePickerController()
        imagePicker3.imagePickerDelegate = self

        selectedImageView = 3
        imagePicker3.maximumSelectionsAllowed = 1

        present(imagePicker3)
    }
    
    
       func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        
        switch selectedImageView {
        case 1:
                 Image1.image = images[0]
                 imageCloseBTN1.isHidden = false
        case 2:
                 Image2.image = images[0]
                 imageCloseBTN2.isHidden = false
        case 3:
                 Image3.image = images[0]
                 imageCloseBTN3.isHidden = false
        default:
            break
             //print("")
        }
        
         picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    
    
    func getLocation()
    {
        WebServices.getMethodWith(url:"get/locations?type=6" ,
                                  parameter: [:],
                                  CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    self.locArry = result
                    
                }
                else
                {
                    
                    
                    
                }
        })

    }
    
    func getLocationWithoutActivity()
    {
        self.isLocationGetOngoing = true
        
        WebServices.getMethodWithOutActivity(url:"get/locations?type=6" ,
                                             parameter: [:],
                                             CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    self.locArry = result
                    
                }
                else
                {
                    
                    
                    
                }
                BaseThread.asyncMain {
                    self.isLocationGetOngoing = false
                }
                
        })

    }

    
    @IBAction func submitBT(_ sender: UIButton) {
        

      if Image1.image != UIImage(named:"addImage")
      {
        if let im = Image1.image
        {
           //imArray[0] = im
            imArray.insert(im.resized(toWidth: 450)!, at: 0)
        }
        
        
      }
        if Image2.image != UIImage(named:"addImage")
        {

            if let im = Image2.image
            {
                //imArray[1] = im
                imArray.insert(im.resized(toWidth: 450)!, at: 1)
            }
        }
        if Image3.image != UIImage(named:"addImage")
        {

            if let im = Image3.image
            {
                //imArray[2] = im
                imArray.insert(im.resized(toWidth: 450)!, at: 2)
            }
        }
        

                    if BaseValidator.isNotEmpty(string: locationTF.text)
                    {
                        imageParam["image[]"] = imArray
                        //print(imArray)
                        postEnq()
                    }
                    else
                    {
                        showBanner(message: "Please Select Location")
                    }
        
        
        
    }
    
    
    
    func postEnq()
    {
        
        WebServices.postMethodMultiPartImage(url:"post_refer" ,
                                             parameter: self.postParaMeters, imageParameter: self.imageParam,
                                             CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    BaseThread.asyncMain {
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        nextVC.dismisType = 0
                        self.present(nextVC, animated: true, completion: nil)
                        
                    }

                }
                else
                {
                    
                    
                    
                }
        })

        
        
        
    }
    
    
}
