//
//  BuildMyNetworkVC.swift
//  Popular
//
//  Created by Appzoc on 05/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Contacts

class BuildMyNetworkVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var selectAllIMG: UIImageView!
    
    @IBOutlet var listTV: UITableView!
    let cellID = "cellID"
    
    var contacts = [Contact]()
    var selectedContacts = [IndexPath]()
    var selectedContacts_Temp = [IndexPath]()
    var contactsWithSections = [[Contact]]()
    let collation = UILocalizedIndexedCollation.current() // create a locale collation object, by which we can get section index titles of current locale. (locale = local contry/language)
    var sectionTitles = [String]()

    var selectAllTap = false
    private var totalContacts: Int = 0
    
    private func fetchContacts() {
        
        let store = CNContactStore()
        
        store.requestAccess(for: (.contacts)) { (granted, err) in
            if let _ = err{
                //print("Failed to request access",err)
                return
            }
            
            if granted {
                //print("Access granted")
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let fetchRequest = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                fetchRequest.sortOrder = CNContactSortOrder.userDefault
                
                do {
                    try store.enumerateContacts(with: fetchRequest, usingBlock: { ( contact, error) -> Void in
                        
                        guard let phoneNumber = contact.phoneNumbers.first?.value.stringValue else {return}
                        self.contacts.append(Contact(givenName: contact.givenName, familyName: contact.familyName, mobile: phoneNumber))
                        
                    })
                    
                    for index in self.contacts.indices{
                        
                        print(self.contacts[index].givenName)
                        print(self.contacts[index].familyName)
                        print(self.contacts[index].mobile)
                    }
                    
                    self.setUpCollation()
                    
                    DispatchQueue.main.async {
                        self.listTV.reloadData()
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                
            }else{
                print("Access denied")
            }
        }
        
    }
    
    override func viewDidLoad() {
        //print("didloadddd")
        fetchContacts()
    }
    
    
    
    @IBAction func BackBTNTapped(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }

        dismiss()
    }

     func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactsWithSections[section].count
        
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTVC", for: indexPath) as! ContactTVC
        let currentContact = contactsWithSections[indexPath.section][indexPath.row]
        
        cell.NameLB?.text = currentContact.givenName + " " + currentContact.familyName
        cell.PhoneLB?.text = currentContact.mobile
        
        
        if !selectedContacts_Temp.contains(indexPath)
        {
            selectedContacts_Temp.append(indexPath)
        }
        
        
//        if selectAllTap
//        {
//            cell.tickImage.image = #imageLiteral(resourceName: "check-box.png")
//        }
//        else
//        {
            if selectedContacts.contains(indexPath)
            {
                cell.tickImage.image = #imageLiteral(resourceName: "check-box.png")
            }
            else
            {
                cell.tickImage.image = #imageLiteral(resourceName: "check-box-empty")
            }
        //}
        
        
        
        
        
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        listTV.deselectRow(at: indexPath, animated: false)
        
    //    _ = tableView.cellForRow(at: indexPath) as! ContactTVC

        
        /*
        DispatchQueue.main.async {
            if self.selectedContacts.count == self.selectedContacts_Temp.count
            {
                self.selectAllTap = true
                self.selectAllIMG.image = #imageLiteral(resourceName: "check-box.png")
            }
            else
            {
                self.self.selectAllTap = false
                self.selectAllIMG.image = #imageLiteral(resourceName: "check-box-empty.png")
            }
        }
        */

        
        if selectedContacts.contains(indexPath)
        {
            if let index = selectedContacts.index(of: indexPath) {
                selectedContacts.remove(at: index)
            }
        }
        else
        {
           selectedContacts.append(indexPath)
        }

        if totalContacts == selectedContacts.count {
            //print("selectedAlll-equlasss")
            selectAllTap = false
            SelectAllBTNTapped(UIButton())
        }else {
            //print("not selected- alllll")
            selectAllTap = true
            selectAllIMG.image = #imageLiteral(resourceName: "check-box-empty.png")
        }
       // print("statussssss",self.selectAllTap)
       // print("selectedContacts",self.selectedContacts.count)
    
        
        listTV.reloadData()
       
        
       
        
    }
    
    
    //Changing color for the Letters in the section titles
     func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.from(hex: "2EB0B5")
        }
     }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
     func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    func setUpCollation(){
        let (arrayContacts, arrayTitles) = collation.partitionObjects(array: self.contacts, collationStringSelector: #selector(getter: Contact.givenName))
        self.contactsWithSections = arrayContacts as! [[Contact]]
        self.sectionTitles = arrayTitles
        
//        print(contactsWithSections.count)
//        print(sectionTitles.count)
        
        for section in 0 ..< sectionTitles.count {
            totalContacts = totalContacts + contactsWithSections[section].count
        }
       // print("totalContacts:",totalContacts)
        
    }
    
// selected all contacts
    @IBAction func SelectAllBTNTapped(_ sender: UIButton) {
        
        if selectAllTap
        {
            selectAllTap = false
            if !selectedContacts.isEmpty, selectedContacts.count != totalContacts {
                SelectAllBTNTapped(UIButton())

            }else if selectedContacts.count == totalContacts {
                selectAllIMG.image = #imageLiteral(resourceName: "check-box-empty.png")
                selectedContacts.removeAll()

            }
        }
        else
        {
            selectAllTap = true
            selectAllIMG.image = #imageLiteral(resourceName: "check-box.png")
            selectedContacts = selectedContacts_Temp
            selectedContacts.removeAll()
            for section in 0 ..< sectionTitles.count {
                
                for row in 0 ..< contactsWithSections[section].count {
                    
                    selectedContacts.append(IndexPath(row: row, section: section))
                }
                
            }
            //print("SelectedIndexpaths",selectedContacts.count)

        }
        listTV.reloadData()
    }
    



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func SubmitBTNTapped(_ sender: UIButton) {
        
        var Arr = [[String:String]]()
        
        for i in selectedContacts
        {
            let contact = contactsWithSections[i.section][i.row]
            
//            print(contact.givenName + " " + contact.familyName)
//            print(contact.mobile)
            
            let dic = [contact.givenName + contact.familyName:contact.mobile]
            
            Arr.append(dic as! [String : String])
            
            
         
        }
        
        let str = json(from: Arr)
        //print(str as Any)
        
        self.postEnq(param: str!)
        
        //self.convertToJSON(dictionary: Arr)
    }
    
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }

    
    
    
    func convertToJSON(dictionary:[[String:Any]]){
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
            print("JSON : \n",json)

            
        }catch{
            print(error)
        }
    }
    
    
    func postEnq(param:String)
    {
        var paramSend = [String:Any]()
        paramSend["contacts"] = param
        paramSend["user_id"] = PLAppState.session.userID
        WebServices.postMethod(url:"build_mynetwork" ,
                               parameter: paramSend,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    //self.didMissVC()
                    BaseThread.asyncMain {
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        nextVC.dismisType = 0
                        self.present(nextVC, animated: true, completion: nil)
                        
                    }
                    
                }
                else
                {
                    
                    
                }
        })
        
    }
    
}


class Contact : NSObject {
    @objc var givenName: String!
     var familyName: String!
     var mobile: String!
    
    init(givenName: String, familyName: String, mobile: String) {
        self.givenName = givenName
        self.familyName = familyName
        self.mobile = mobile
    }
}

extension UILocalizedIndexedCollation {
    //func for partition array in sections
    func partitionObjects(array:[AnyObject], collationStringSelector:Selector) -> ([AnyObject], [String]) {
        var unsortedSections = [[AnyObject]]()
        
        //1. Create a array to hold the data for each section
        for _ in self.sectionTitles {
            unsortedSections.append([]) //appending an empty array
        }
        //2. Put each objects into a section
        for item in array {
            let index:Int = self.section(for: item, collationStringSelector:collationStringSelector)
            unsortedSections[index].append(item)
        }
        //3. sorting the array of each sections
        var sectionTitles = [String]()
        var sections = [AnyObject]()
        for index in 0 ..< unsortedSections.count { if unsortedSections[index].count > 0 {
            sectionTitles.append(self.sectionTitles[index])
            sections.append(self.sortedArray(from: unsortedSections[index], collationStringSelector: collationStringSelector) as AnyObject)
            }
        }
        return (sections, sectionTitles)
    }
}
