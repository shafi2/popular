//
//  MyNetworkVC.swift
//  Popular
//
//  Created by Appzoc on 12/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class MyNetworkVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var listVC: UITableView!
    
    var sectionTitles = [String]()
    var contactsWithSections = [[Contact]]()
    let collation = UILocalizedIndexedCollation.current()
    var contacts = [Contact]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMylist()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBttontap(_ sender: UIButton) {
        
        dismiss()
    }
    
    
    
    func getMylist()
    {
        var paramSend = [String:Any]()
       
        paramSend["user_id"] = PLAppState.session.userID
        
        BaseThread.asyncMain {
            
            
            
            WebServices.postMethod(url:"getMyNetWork" ,
                                   parameter: paramSend,
                                   CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
  
                        let dat = result["data"] as! [[String:Any]]
                        
                        for dd in dat
                        {
                            
                            let name = dd["name"] as! String
                            let phn = dd["contacts"] as! String
                            self.contacts.append(Contact(givenName: name, familyName: "", mobile: phn))
                        }
                        self.setUpCollation()
                        
                    }
                    else
                    {
                        
                        
                        
                    }
            })
            
            
            
        }
        
        
    }


    func setUpCollation(){
        let (arrayContacts, arrayTitles) = collation.partitionObjects(array: self.contacts, collationStringSelector: #selector(getter: Contact.givenName))
        self.contactsWithSections = arrayContacts as! [[Contact]]
        self.sectionTitles = arrayTitles
        
        //print(contactsWithSections.count)
        //print(sectionTitles.count)
        listVC.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactsWithSections[section].count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTVCList", for: indexPath) as! ContactTVCList
        //let cell = ContactsCell(style: .subtitle, reuseIdentifier: cellID)
        
        //cell.link = self // custom delegation
        //
        let contact = contactsWithSections[indexPath.section][indexPath.row]
        cell.selectionStyle = .default
        cell.NameLB?.text = contact.givenName + " " + contact.familyName
        cell.PhoneLB?.text = contact.mobile
        

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }

}
