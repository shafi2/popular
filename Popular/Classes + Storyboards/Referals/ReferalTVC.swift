//
//  ReferalTVC.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class ReferalTVC: UITableViewCell {
    
    @IBOutlet weak var tittleLB: UILabel!
    @IBOutlet weak var IconIMV: UIImageView!
    @IBOutlet var rightArrowImage: UIImageView!
    @IBOutlet weak var baseVIew: BaseView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ReferalTVCEXP: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private var previousIndexPath: IndexPath?
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListColletion", for: indexPath) as! ListColletion
        cell.baseVW.cornerRadius = Float(cell.baseVW.frame.height/2)
        cell.tittleLB.text = ListSource[indexPath.item]
        cell.baseVW.backgroundColor = .white
        cell.tittleLB.textColor = .black
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.invalidateLayout()
        let width = UIScreen.main.bounds.width * 0.39
        return CGSize(width: width, height: 50)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? ListColletion {
            if previousIndexPath == nil {
                cell.baseVW.backgroundColor = .themeGreen
                cell.tittleLB.textColor = .white
                previousIndexPath = nil
            }else if indexPath == previousIndexPath {
                if cell.baseVW.backgroundColor == .white {
                    
                    cell.baseVW.backgroundColor = .themeGreen
                    cell.tittleLB.textColor = .white
                }else {
                    cell.baseVW.backgroundColor = .white
                    cell.tittleLB.textColor = .black

                }
            }else if previousIndexPath != nil { // first time - previous indexpath is nil
                
                if let previousCell = collectionView.cellForItem(at: previousIndexPath!) as? ListColletion {
                    previousCell.baseVW.backgroundColor = .white
                    cell.tittleLB.textColor = .black
                    
                }

            }
            
        }

        switch indexPath.item {
        case 0:

            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "ENQPopUpVC") as! ENQPopUpVC
            
            vc.isnewcarref = true
            prsentVC1?.present(vc)
            
             break
            
        case 1:
            
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "ENQPopUpVC") as! ENQPopUpVC
            
            vc.isnewcarref = false
            prsentVC1?.present(vc)
            
            break
            
        case 2:
            
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "ServiceENQPopup") as! ServiceENQPopup
            
            vc.isnewcarref = true
            prsentVC1?.present(vc) //InsurancePopupVC
            
            break
        case 3:
            
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "InsurancePopupVC") as! InsurancePopupVC
            
            vc.isnewcarref = true
            prsentVC1?.present(vc) //InsurancePopupVC
            
            break
        case 4:
            
              if isGotoNext()
              {
                let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "BuildMyNetworkVC") as! BuildMyNetworkVC
                
                
                prsentVC1?.present(vc) //InsurancePopupVC
              }

            
            break
        case 5:
            
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "AccidentDentWorkVC") as! AccidentDentWorkVC
            
            vc.isnewcarref = true
            prsentVC1?.present(vc) //InsurancePopupVC
            
            break
            
        default:
           // print("")
            break
        }
    }
    
    let ListSource = ["New Car","Used Car","Service a car","Insurance","Build my network","Accident/Dent work"]
    @IBOutlet weak var listCV: UICollectionView!
    
    var prsentVC1:UIViewController? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.listCV.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func isGotoNext() -> Bool
    {
        
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                return true
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            prsentVC1?.present(segueVC)
        }
        return false
        
    }
    
}

class ListColletion : UICollectionViewCell {
    
    @IBOutlet weak var tittleLB: UILabel!
    @IBOutlet weak var baseVW: BaseView!
    let prsentVC:UIViewController? = nil
    
}
