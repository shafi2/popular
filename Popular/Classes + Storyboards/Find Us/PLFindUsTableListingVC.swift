//
//  PLFindUsTableListingVC.swift
//  Popular
//
//  Created by Appzoc on 10/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit


class PLFindUsHeaderTVC: UITableViewCell{
    
}

class PLFindUsAddressTVC: UITableViewCell{
    
    @IBOutlet var branchHeadNameLBL: UILabel!
    @IBOutlet var branchHeadNumberLBL: UILabel!
    @IBOutlet var branchHeadEmailLBL: UILabel!
    
    @IBOutlet var branchHeadAddressLBL: UILabel!
    
    @IBOutlet var viewInMapBTN: BaseButton!
    
}

class PLFindUsTableListingVC: UIViewController {
    
    @IBOutlet var tableRef: UITableView!
    
    var addressData:[PLFindUsVC.FindUsModel.AddressData] = []
    
    var latitude:String = "0"
    var longitude:String = "0"
    
    var mapLink = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    @IBAction func viewInMapTapped(_ sender: BaseButton) {
        latitude = addressData[sender.tag].latitude
        longitude = addressData[sender.tag].longitude
        mapLink = addressData[sender.tag].map_link ?? "https://www.google.com/maps/@\(self.latitude),\(self.longitude),18z"
        openInMap()
    }
    
    func openInMap(){
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = mapLink //"https://www.google.com/maps/@\(latitude),\(longitude),18z"
//        print("Browser URL ",browserURL)
//        print("App URL ",mapAppURL)
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!), latitude != "0", longitude != "0"{
            UIApplication.shared.open(URL(string: "\(mapAppURL)")!, options: [:], completionHandler: nil)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            //UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
            UIApplication.shared.open(URL(string: "\(browserURL)")!, options: [:], completionHandler: nil)

        }
    }
}

extension PLFindUsTableListingVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = addressData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PLFindUsAddressTVC") as! PLFindUsAddressTVC
        cell.branchHeadNameLBL.text = cellData.branch_head
        cell.branchHeadNumberLBL.text = cellData.head_mobile ?? "-"
        cell.branchHeadEmailLBL.text = cellData.head_email ?? "-"
        cell.branchHeadAddressLBL.text = cellData.address
        cell.viewInMapBTN.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "PLFindUsHeaderTVC") as! PLFindUsHeaderTVC
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    
}
