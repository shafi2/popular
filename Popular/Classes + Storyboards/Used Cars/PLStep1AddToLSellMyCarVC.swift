//
//  PLStep1AddToLSellMyCarVC.swift
//  Popular
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLStep1AddToLSellMyCarVC: UIViewController, InterfaceSettable, UITextFieldDelegate {
    
    @IBOutlet var priceTF: UITextField!
    @IBOutlet var registrationNoTF: UITextField!
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var nextBTN: BaseButton!
    
    @IBOutlet var backgroundBlurView: UIVisualEffectView!
    final var price = ""
    final var imageOfCar = UIImage()
    final var selectedCarDetails: PLSellMyCarMyVehiclesModel?
    
    private var regNo = ""
    private var isVisibleRegistration: Bool = false
    private let placeHolderPrice = "Expected Price.."
    private let placeHolderRegNo = "Enter Registration number.."

    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundBlurView.alpha = 0.95
        setUpInterface()
    }

    func setUpInterface() {
        vehicleImage.layer.cornerRadius = 6
        vehicleImage.layer.masksToBounds = true
        vehicleImage.image = imageOfCar
        nextBTN.layer.zPosition = 1
        if let selectedCar = selectedCarDetails, selectedCar.reg_no.isNotEmpty {
            registrationNoTF.text = selectedCar.reg_no
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        //print(price,"priceeeeeeeee")
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        if isVisibleRegistration {
            if validateRegistrationData() {
                let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLStep2AddToLSellMyCarVC") as! PLStep2AddToLSellMyCarVC
                segueVC.imageOfCar = self.imageOfCar
                segueVC.expPrice = Double(priceTF.text ?? "0")!
                segueVC.regNum = registrationNoTF.text ?? ""
                segueVC.selectedCarDetails = self.selectedCarDetails
                present(segueVC, animated: false, completion: nil)
            }
        }else{
            if validatePriceData() {
                animateTextField()
            }
        }
    }

    private func validatePriceData() -> Bool {
        if BaseValidator.isNotEmpty(string: priceTF.text), priceTF.text != "Expected Price.." {
            
            if BaseValidator.isValid(digits: priceTF.text!) {
                return true
            }else {
                showBanner(message: "Please enter valid price")
                return false
            }
        }else{
            showBanner(message: "Please enter price")
            return false
        }

    }
    
    private func validateRegistrationData() -> Bool {
        if BaseValidator.isNotEmpty(string: registrationNoTF.text), registrationNoTF.text != "Enter Registration number.." {
            return true
        }else{
            showBanner(message: "Please enter registration number")
            return false
        }
        
    }
    
    func animateTextField(){
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseIn, animations: {
            self.priceTF.alpha = CGFloat(0)
        }, completion: nil)

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.registrationNoTF.alpha = CGFloat(1)
        }, completion: nil)
        isVisibleRegistration = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == priceTF {
            if priceTF.text == "Expected Price.." {
                priceTF.text = nil
            }
        }else {
            if registrationNoTF.text == "Enter Registration number.." {
                registrationNoTF.text = nil
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == priceTF, validatePriceData() {
            animateTextField()
        }else{

        }
    }
    
}


