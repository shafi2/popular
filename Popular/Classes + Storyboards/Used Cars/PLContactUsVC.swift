//
//  PLContactUsVC.swift
//  Popular
//
//  Created by Appzoc on 16/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import JBDatePicker

class PLContactUsVC: UIViewController, InterfaceSettable {
    

    @IBOutlet var homeView: BaseView!
    @IBOutlet var dateTimeSelectionView: UIView!
    
    @IBOutlet var dateSelectionView: BaseView!
    @IBOutlet var timeSelectionView: BaseView!
    @IBOutlet var calendarView: JBDatePickerView!
   
    @IBOutlet var yearLBL: UILabel!
    @IBOutlet var dateLBL: UILabel!
    @IBOutlet var monthLBL: UILabel!
    @IBOutlet var startTimeBTN: UIButton!
    @IBOutlet var endTimeBTN: UIButton!
    @IBOutlet var amBTN: UIButton!
    @IBOutlet var pmBTN: UIButton!
    @IBOutlet weak var dateBTN: UIButton!
    @IBOutlet weak var timeBTN: UIButton!
    
    @IBOutlet var timePicker: UIPickerView!
    
    // calender properties
    var dateToShow: Date = Date()
    var colorForWeekDaysViewBackground: UIColor = .clear
    var shouldShowMonthOutDates: Bool = false
    
    
    
    
    private var isSelectedTime: Bool = false
    private var selectedStartTime: String = "09:00"
    private var selectedStartAMPM: String = "AM"
    private var selectedEndTime: String = "05:00"
    private var selectedEndAMPM: String = "PM"
    private var selectedDateString: String = ""
    private var selectedDate: Date = Date()
    private var currentDate = Date()

    
    
    //
    fileprivate var amPmText = "AM"
    fileprivate var startAMPM = "AM"
    fileprivate var endAMPM = "AM"
    fileprivate var hoursSource: [String] = []
    fileprivate var minutesSource: [String] = []
    fileprivate var isStartTime = true
    fileprivate var startHour = "09"
    fileprivate var startMinute = "00"
    fileprivate var endHour = "05"
    fileprivate var endMinute = "00"

    fileprivate var selectedDateMain = Date()

    var parameterContact = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setUpInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpInterface()
    }
    
    func setUpInterface() {
        // setting current date as the selected date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        yearLBL.text = formatter.string(from: selectedDate)
        formatter.dateFormat = "EEE MMMM dd"
        dateLBL.text = formatter.string(from: selectedDate)
        formatter.dateFormat = "MMMM"
        monthLBL.text = formatter.string(from: selectedDate)

        
        
        hoursSource.removeAll()
        minutesSource.removeAll()
        calendarView.delegate = self
        showHideViews()
        for hour in 1...12 {
            hoursSource.append(hour.leadingZero)
        }
        for minute in 0...59 {
            minutesSource.append(minute.leadingZero)
        }
        
        startHour = currentDate.hour.intValue.leadingZero
        startMinute = currentDate.minute.leadingZero
        selectedStartTime = startHour + ":" + startMinute
        selectedStartAMPM = currentDate.ampm
        
        endHour = currentDate.hour.intValue.leadingZero
        endMinute = currentDate.minute.leadingZero
        selectedEndTime = endHour + ":" + endMinute
        selectedEndAMPM = currentDate.ampm
        
        setAMPM()
        
        startTimeBTN.setTitle(selectedStartTime, for: .normal)
        endTimeBTN.setTitle(selectedEndTime, for: .normal)
        
        timePicker.selectRow(startHour.intValue-1, inComponent: 0, animated: false)
        timePicker.selectRow(startMinute.intValue, inComponent: 1, animated: false)
        selectedDate = calendarView.selectedDateView.date!

        
        
    }

    fileprivate func showHideViews(){
        homeView.isHidden = false
        dateTimeSelectionView.isHidden = true
        dateSelectionView.isHidden = true
        timeSelectionView.isHidden = true
        dateTapped(UIButton())

    }
    
    private func setAMPM() {
        if isStartTime {
            if selectedStartAMPM == "AM"{
                self.amBTN.alpha = 1
                self.pmBTN.alpha = 0.3
                
            }else {
                self.amBTN.alpha = 0.3
                self.pmBTN.alpha = 1
            }
        }else {
            if selectedEndAMPM == "AM" {
                self.amBTN.alpha = 1
                self.pmBTN.alpha = 0.3
            }else {
                self.amBTN.alpha = 0.3
                self.pmBTN.alpha = 1
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func homeTapped(_ sender: UIButton) {
        
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                homeView.isHidden = true
                dateTimeSelectionView.isHidden = false
                dateSelectionView.isHidden = false
                self.dateBTN.setTitleColor(UIColor.black, for: .normal)
                self.timeBTN.setTitleColor(UIColor.lightGray, for: .normal)
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        
       
    }
    
    @IBAction func dateTapped(_ sender: UIButton) {
        dateSelectionView.isHidden = false
        timeSelectionView.isHidden = true
        self.dateBTN.setTitleColor(UIColor.black, for: .normal)
        self.timeBTN.setTitleColor(UIColor.lightGray, for: .normal)
        dateToShow = Date()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        timeSelectionView.isHidden = false
        dateSelectionView.isHidden = true
        self.timeBTN.setTitleColor(UIColor.black, for: .normal)
        self.dateBTN.setTitleColor(UIColor.lightGray, for: .normal)
        
//        self.startTimeBTN.alpha = 1
//        self.endTimeBTN.alpha = 0.3
//        self.amBTN.alpha = 1
//        self.pmBTN.alpha = 0.3
//        timePicker.reloadAllComponents()
        self.startTimeBTN.alpha = 1
        self.endTimeBTN.alpha = 0.3
        timePicker.reloadAllComponents()
        isSelectedTime = true

    }
    
    @IBAction func previousMonthTapped(_ sender: UIButton) {
        calendarView.loadPreviousView()
    }
    
    @IBAction func nextMonthTapped(_ sender: UIButton) {
        calendarView.loadNextView()
    }
    
    @IBAction func startTimeTapped(_ sender: UIButton) {
        //highlight
//        self.startTimeBTN.alpha = 1
//        self.endTimeBTN.alpha = 0.3
//        isStartTime = true
//        timePicker.selectRow(hoursSource.enumerated().filter({$0.element == "\(startHour)"}).first?.offset ?? 0, inComponent: 0, animated: true)
//        timePicker.selectRow(minutesSource.enumerated().filter({$0.element == "\(startMinute)"}).first?.offset ?? 0, inComponent: 1, animated: true)
//        timePicker.reloadAllComponents()
//        setAmPmVisibility()
        
        self.startTimeBTN.alpha = 1
        self.endTimeBTN.alpha = 0.3
        isStartTime = true
        setAMPM()
        timePicker.reloadAllComponents()
        //"\(startHour.intValue-1)"
        
        timePicker.selectRow(hoursSource.enumerated().filter({$0.element == startHour }).first?.offset ?? 0, inComponent: 0, animated: true)
        
        timePicker.selectRow(minutesSource.enumerated().filter({$0.element == startMinute }).first?.offset ?? 0, inComponent: 1, animated: true)

        
        
    }
    
    @IBAction func endTimeTapped(_ sender: UIButton) {
//        self.startTimeBTN.alpha = 0.3
//        self.endTimeBTN.alpha = 1
//        isStartTime = false
//        timePicker.selectRow(hoursSource.enumerated().filter({$0.element == "\(endHour)"}).first?.offset ?? 0, inComponent: 0, animated: true)
//        timePicker.selectRow(minutesSource.enumerated().filter({$0.element == "\(endMinute)"}).first?.offset ?? 0, inComponent: 1, animated: true)
//        timePicker.reloadAllComponents()
//        setAmPmVisibility()
        
        self.startTimeBTN.alpha = 0.3
        self.endTimeBTN.alpha = 1
        isStartTime = false
        setAMPM()
        timePicker.reloadAllComponents()
        
        timePicker.selectRow(hoursSource.enumerated().filter({$0.element == endHour }).first?.offset ?? 0, inComponent: 0, animated: true)
        timePicker.selectRow(minutesSource.enumerated().filter({$0.element == endMinute }).first?.offset ?? 0, inComponent: 1, animated: true)

    }
    
    @IBAction func amTapped(_ sender: UIButton) {
//        //self.amBTN.alpha = 1
//        //self.pmBTN.alpha = 0.3
//        if isStartTime{
//            startAMPM = "AM"
//        }else{
//            endAMPM = "AM"
//        }
//        setAmPmVisibility()
        
        self.amBTN.alpha = 1
        self.pmBTN.alpha = 0.3
        amPmText = "AM"
        
        if isStartTime {
            selectedStartAMPM = amPmText
        }else {
            selectedEndAMPM = amPmText
            
        }

    }
    
    @IBAction func pmTapped(_ sender: UIButton) {
//       // self.amBTN.alpha = 0.3
//        //self.pmBTN.alpha = 1
//        if isStartTime{
//            startAMPM = "PM"
//        }else{
//            endAMPM = "PM"
//        }
//        setAmPmVisibility()
        self.amBTN.alpha = 0.3
        self.pmBTN.alpha = 1
        amPmText = "PM"
        if isStartTime {
            selectedStartAMPM = amPmText
        }else {
            selectedEndAMPM = amPmText
            
        }

    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
//        guard validateTime() else {return}
//        print( "Date : ",dateLBL.text! + "\n Month : " + monthLBL.text! + "\n Year : " + yearLBL.text!)
//        _ = dateLBL.text! //+ " " + monthLBL.text! + " " + yearLBL.text!
//        let startTimeContact = startHour + ":" + startMinute + " " + startAMPM
//        let endTimeContact = endHour + ":" + endMinute + " " + endAMPM
//        print("Start time : ",startTimeContact," End time",endTimeContact )
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yyyy"
//        parameterContact["call_date"] = formatter.string(from: selectedDateMain)
//        print("Seledted Date :",formatter.string(from: selectedDateMain))
//        //from_call_time
//        //to_call_time
//        parameterContact["from_call_time"] = startTimeContact
//        parameterContact["to_call_time"] = endTimeContact
//        parameterContact["userid"] = PLAppState.session.userID
//        parameterContact["type"] = "14"
//        print("parameterContact",parameterContact)
//        postContact()
//        showHideViews()
        
//        print( dateLBL.text! + " " + monthLBL.text! + " " + yearLBL.text!)
//        let dateForContact = dateLBL.text! + " " + monthLBL.text! + " " + yearLBL.text!
//
//        print(startHour + ":" + startMinute + "-" + endHour + ":" + endMinute)
//        let timeForContact = startHour + ":" + startMinute + "-" + endHour + ":" + endMinute
        
        generateTime()
        
    }
    
    private func generateTime() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        selectedDateString = formatter.string(from: selectedDate)
        
        let startTimeFull = selectedStartTime + " " + selectedStartAMPM
        let endTimeFull = selectedEndTime + " " + selectedEndAMPM
        let startTimeFullWithDate =  selectedDateString + " " + startTimeFull
        let endTimeFullWithDate = selectedDateString + " " + endTimeFull

//        print("selectedDateString",selectedDateString)
//        print("startTimeFull",startTimeFull)
//        print("endTimeFull",endTimeFull)
        if validateData(start: startTimeFullWithDate, end: endTimeFullWithDate) {
            //delegate?.selectedDateAndTime(dateString: selectedDateString, startTimeString: startTimeFull, endTimeString: endTimeFull)
            postContact(dateString: selectedDateString, startTimeString: startTimeFull, endTimeString: endTimeFull)
            dismiss()
            
        }
        
    }
    
    
    private func validateData(start: String, end: String) -> Bool {
        
        if isSelectedTime {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm a" //"dd-MM-yyyy hh:mm a"
            let date1 = formatter.date(from: start)
            let date2 = formatter.date(from: end)
            
            let now = Date()
            
            if /*now.compare(date1!) == .orderedDescending ||*/ now.compare(date2!) == .orderedDescending {
                showBanner(message: "Choose start and end time later than current time")
                return false
            }else if /*now.compare(date1!) == .orderedSame ||*/ now.compare(date2!) == .orderedSame {
                showBanner(message: "Choose start and end time later than current time")
                return false
                
            }
            
            
            switch date1!.compare(date2!) {
            case .orderedAscending:
                //print("Date A is earlier than date B")
                return true
            case .orderedDescending:
                //print("Date A is later than date B")
                showBanner(message: "Start time is later than end time")
                return false
            case .orderedSame:
               // print("The two dates are the same")
                showBanner(message: "Start time and end time are same")
                
                return false
            }
            
        }else {
            showBanner(message: "Please choose a time")
            return false
        }
        
    }

    
//    func validateTime() -> Bool{
//        guard startHour != "00" || endHour != "00" else {
//            showBanner(message: "Set Time")
//            return false
//        }
//        return true
//    }
    
    func setAmPmVisibility(){
        if isStartTime{
            if startAMPM == "AM"{
                amBTN.alpha = 1
                pmBTN.alpha = 0.3
            }else{
                amBTN.alpha = 0.3
                pmBTN.alpha = 1
            }
        }else{
            if endAMPM == "AM"{
                amBTN.alpha = 1
                pmBTN.alpha = 0.3
            }else{
                amBTN.alpha = 0.3
                pmBTN.alpha = 1
            }
        }
    }

}

extension PLContactUsVC: JBDatePickerViewDelegate {
    
    func didSelectDay(_ dayView: JBDatePickerDayView) {
        //day selection
        guard let selectedDate = dayView.date else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        yearLBL.text = formatter.string(from: selectedDate)
        formatter.dateFormat = "EEE MMMM dd"
        dateLBL.text = formatter.string(from: selectedDate)
        //formatter.dateFormat = "YYYY MM DD"
        selectedDateMain = selectedDate
        formatter.dateFormat = "MMMM"
        monthLBL.text = formatter.string(from: selectedDate)
    }
    
    func shouldAllowSelectionOfDay(_ date: Date?) -> Bool {
        guard let date = date else {return true}
        let comparison = NSCalendar.current.compare(date, to: Date(), toGranularity: .day)
        //print("Comparison",comparison)
        if comparison == .orderedAscending {
            return false
        }
        return true
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
       // print(monthView.description)
        monthLBL.text = monthView.monthDescription
    }

}

extension PLContactUsVC: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? hoursSource.count : minutesSource.count

    }
    

}

extension PLContactUsVC: UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let title = component == 0 ? hoursSource[row] : minutesSource[row]
        let attributedTitle = NSAttributedString(string: title, attributes: [NSAttributedStringKey.foregroundColor: UIColor.from(hex: "2EB0B5")])
    
        return attributedTitle
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isStartTime {
            if component == 0 {
                startHour = hoursSource[row]
            }else{
                startMinute = minutesSource[row]
            }
           // print("sta",startMinute,startHour)
            BaseThread.asyncMain {
                self.startTimeBTN.setTitle( self.startHour + ":" + self.startMinute, for: .normal)
                self.selectedStartTime = self.startTimeBTN.title(for: .normal)!

            }
        }else{
            if component == 0 {
                endHour = hoursSource[row]
            }else{
                endMinute = minutesSource[row]
            }
           // print("ednd",endMinute,endHour)
            BaseThread.asyncMain {
                self.endTimeBTN.setTitle( self.endHour + ":" + self.endMinute, for: .normal)
                self.selectedEndTime = self.endTimeBTN.title(for: .normal)!
            }

        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 80
    }

    
}


// Mark:- Handling web service
extension PLContactUsVC {
    
    private func postContact(dateString: String, startTimeString: String, endTimeString: String) {
        
        parameterContact["userid"] = PLAppState.session.userID
        parameterContact["call_date"] = selectedDateString
        parameterContact["from_call_time"] = startTimeString
        parameterContact["to_call_time"] = endTimeString
        parameterContact["type"] = "14"
        print("postContact->UsedCarContactUs:",parameterContact)

        
        WebServices.postMethod(url:"contactUs", parameter: self.parameterContact, CompletionHandler: { (isFetched, result) in
                if isFetched
                {
                    print(self.parameterContact)
                    print(result)
                    BaseThread.asyncMain {
                        let segueVC = self.storyBoardPopUps!.instantiateViewController(withIdentifier: "PLThankingVC")
                        self.present(segueVC)
                        
                    }
                    
                }
                else
                {
                    //showBanner(message: "Oops error..!")
                }
        })
    }
    
}
