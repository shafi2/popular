//
//  PLImageSelectorSellMyCarVC.swift
//  Popular
//
//  Created by Appzoc on 12/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import CameraManager
import OpalImagePicker
import AVFoundation
import NotificationBannerSwift

class PLImageSelectorSellMyCarCVC: UICollectionViewCell {
    
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var modelLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class PLImageSelectorSellMyCarVC: UIViewController, InterfaceSettable, OpalImagePickerControllerDelegate  {
    
    @IBOutlet var previewView: UIView!
    @IBOutlet var chooseVehicleLBL: UILabel!
    @IBOutlet var myVehicleCV: UICollectionView!
    @IBOutlet var backgroundBlurView: UIVisualEffectView!
    
    private let cameraHandler = CameraManager()
    private let imagePicker = OpalImagePickerController()
    private var myVehiclesDataSource: [PLSellMyCarMyVehiclesModel] = []
    private var selectedCarDetails: PLSellMyCarMyVehiclesModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundBlurView.alpha = 0.95
        getMyVehiclesDataWeb()
        getMakeData()
        setUpInterface()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraHandler.stopCaptureSession()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpInterface() {
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        
        cameraHandler.showAccessPermissionPopupAutomatically = false
        cameraHandler.writeFilesToPhoneLibrary = true
        cameraHandler.flashMode = .auto
        let cameraState = cameraHandler.currentCameraStatus()
        switch cameraState {
        case .ready:
            self.startCamera()
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (accessAllowed) in
                if accessAllowed {
                    self.startCamera()
                }
                else{
                    showBanner(title: "Access denied", message: "Please allow access to camera to take photo")
                }
            }
            break
        case .accessDenied:
            showBanner(title: "Access denied", message: "Please allow access to camera to take photo")
            break
        case .noDeviceFound:
            showBanner(title: "", message: "Camera not available")
        }
        
    }
    
    fileprivate func startCamera()
    {
        cameraHandler.addPreviewLayerToView(previewView, newCameraOutputMode: .stillImage)
        cameraHandler.showErrorBlock = {(erTitle: String, erMessage: String) -> Void in
            showBanner(title: "", message: "Camera not available")
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        //debugPrint("imagesPicked",images as AnyObject)
        picker.dismiss(animated: true, completion: nil)
        cameraHandler.resumeCaptureSession()
        
        if !images.isEmpty {
            let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLStep1AddToLSellMyCarVC") as! PLStep1AddToLSellMyCarVC
            segueVC.imageOfCar = images[0].resized(toWidth: 450)!
            present(segueVC, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func captureImageTapped(_ sender: UIButton) {
        cameraHandler.capturePictureWithCompletion({ (image, error) -> Void in
            if error == nil {
                //debugPrint("CapturedImage",image as AnyObject)
                if let capturedImage = image {
                    let segueVC = self.storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLStep1AddToLSellMyCarVC") as! PLStep1AddToLSellMyCarVC
                    segueVC.imageOfCar = capturedImage
                    self.present(segueVC)
                }

            }
            else {
                showBanner(title: "", message: "Camera not available")
                self.cameraHandler.resumeCaptureSession()
            }
        })
        
    }
    
    @IBAction func chooseGalleryTapped(_ sender: BaseButton) {
        present(imagePicker, animated: true, completion: nil)
    }
    
}


// collection view datasource
extension PLImageSelectorSellMyCarVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myVehiclesDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLImageSelectorSellMyCarCVC", for: indexPath) as? PLImageSelectorSellMyCarCVC else { return UICollectionViewCell() }
        
        let sourceData = myVehiclesDataSource[indexPath.item]
        
        cell.vehicleImage.kf.setImage(with: URL(string: baseURLImage + sourceData.image), placeholder: #imageLiteral(resourceName: "placeholder_Image"), options: [.requestModifier(getImageAuthorization())])
        cell.modelLBL.text =  sourceData.carmodel + " "  + sourceData.variantname
        
        return cell
    }
    
    
}


// collection view delegates
extension PLImageSelectorSellMyCarVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("PLImageSelectorSellMyCarVC_Index",indexPath)
        
        if let cell = collectionView.cellForItem(at: indexPath) as? PLImageSelectorSellMyCarCVC {
            if let vehicleImage = cell.vehicleImage.image, vehicleImage != UIImage(named: "placeholder_Image") {
                let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLStep1AddToLSellMyCarVC") as! PLStep1AddToLSellMyCarVC
                segueVC.imageOfCar = vehicleImage
                segueVC.selectedCarDetails = myVehiclesDataSource[indexPath.item]
                present(segueVC, animated: true, completion: nil)
                
            }else{
                //print("PlaceHodler image is there")
            }
        }

    }
}

// Webservice
extension PLImageSelectorSellMyCarVC {
    // http://app.appzoc.com/popular/backend/api/getMyVehicles

    private func getMyVehiclesDataWeb() {
        let url = "getMyVehicles"
        var params = Json()
        params["user_id"] = PLAppState.session.userID
        WebServices.postMethodWithoutActivity(url: url, parameter: params) { (isSucceeded, response) in
            
            guard isSucceeded else { return }
            guard let responseArray = response["data"] as? JsonArray, !responseArray.isEmpty else { return }
            BaseThread.asyncMain {
                self.myVehiclesDataSource = PLSellMyCarMyVehiclesModel.getValue(fromArray: responseArray)
                
                self.myVehicleCV.reloadData()
                
            }
            
            
        }
    }
    
    private func getMakeData(){
        let url = "getMake"
        
        WebServices.postMethodWithoutActivity(url: url, parameter: [:]) { (isSucceeded, response) in
            
            guard isSucceeded else { return }
            guard let responseData = response["data"] as? JsonArray else { return }
            
            let resultArray = PLSideOptionsModel.getData(fromArray: responseData, idKey: "id", nameKey: "name", isStringId: false)
            GenericCarDetails.shared.makes = resultArray
            
            
            
        }
        
    }

    
}

class GenericCarDetails {
    private init(){
    }
    static let shared = GenericCarDetails()
    var makes: [PLSideOptionsModel] = []
    var models: [PLSideOptionsModel] = []
    var variants: [PLSideOptionsModel] = []
}


class PLSellMyCarMyVehiclesModel
{
    var vehicleid:Int = 0
    var reg_no:String = ""
    var carmodelid:Int = 0
    var carmodel:String = ""
    var variantid:Int = 0
    var variantname:String = ""
    var makename:String = ""
    var makenid:Int = 0
    var image:String = ""
    
    init(dictionary: Json) {
        
        vehicleid = dictionary["vehicleid"] as? Int ?? 0
        reg_no = dictionary["reg_no"] as? String ?? ""
        carmodelid = dictionary["carmodelid"] as? Int ?? 0
        carmodel = dictionary["carmodel"] as? String ?? ""
        variantid = dictionary["variantid"] as? Int ?? 0
        variantname = dictionary["variantname"] as? String ?? ""
        makename = dictionary["makename"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
        makenid = dictionary["makenid"] as? Int ?? 0
    }

    class func getValue(fromArray: JsonArray) -> [PLSellMyCarMyVehiclesModel] {
        var modelArray: [PLSellMyCarMyVehiclesModel] = []
        for item in fromArray {
            
            modelArray.append(PLSellMyCarMyVehiclesModel(dictionary: item))
            
        }
        
        return modelArray
    }
    
    
}




