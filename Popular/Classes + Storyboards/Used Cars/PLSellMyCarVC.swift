//
//  PLSellMyCarVC.swift
//  Popular
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLSellMyCarTVC: UITableViewCell {
    
    @IBOutlet var vehicleImage: BaseImageView!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var deleteBTN: BaseButton!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var yearLBL: UILabel!
}

class PLAddedToSellMyCarNotifier {
    var isNotAddedVehicle = true
    static let shared = PLAddedToSellMyCarNotifier()
    private init() {
    }
}

class PLSellMyCarVC: UIViewController, InterfaceSettable {

    @IBOutlet var noVehiclesView: BaseView!
    @IBOutlet var vehicleTV: UITableView!
    @IBOutlet var addVehicleRoundBTN: UIButton!
    
    fileprivate var isSellMyCarEmpty: Bool = true
    
    var parameterSellCarList = [String:Any]()
    var sellmyListDataSource = SellMyCarListModel()
    private var pagination: Int = 0
    private var carCount: Int = 0
    private var isPaginatedCallOnGoing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getSellCarList()
        
        
        setUpInterface()
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                getSellCarList()
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {

    }

    func setUpInterface() {
        if isSellMyCarEmpty {
            noVehiclesView.isHidden = false
            addVehicleRoundBTN.isHidden = true
            vehicleTV.isHidden = true
        }else {
            vehicleTV.reloadData()
            noVehiclesView.isHidden = true
            addVehicleRoundBTN.isHidden = false
            vehicleTV.isHidden = false
            vehicleTV.contentInset.bottom = 90
        }
        
    }
    
    // delegated from sellmycar adding step
    func reloadVC(){
        //print("Relaoding VCCCCC")
        getSellCarListWithoutActivity()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isViewDidLoadedPageVC = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //print("viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isViewDidLoadedPageVC = false
    }
    
    
    @IBAction func addVehicleTapped(_ sender: UIButton) {
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLImageSelectorSellMyCarVC") as! PLImageSelectorSellMyCarVC
                present(segueVC, animated: false, completion: nil)
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        
        
       

    }
    
   
    @IBAction func deleteVehicleTapped(_ sender: BaseButton) {
        //print("deleteIndexpath", sender.indexPath as Any)
//        vehicleTVCount -= 1
//        vehicleTV.reloadData()
//        if vehicleTVCount == 0 {
//            isSellMyCarEmpty = true
//            setUpInterface()
//        }
        guard let indexPath = sender.indexPath else { return }
        self.deleteSellCarList(usedcarID: sender.tag, index: indexPath.row)

    }
    
    
}


// Handling Tableview datasources
extension PLSellMyCarVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellmyListDataSource.SellCarListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLSellMyCarTVC", for: indexPath) as? PLSellMyCarTVC else { return UITableViewCell() }
        
        let data = sellmyListDataSource.SellCarListData[indexPath.row]
        
        cell.modelLBL.text = data.brand + " " + data.model
        cell.varientLBL.text = "Model : " + data.variant.description
        cell.yearLBL.text = "Year : " + data.modelYear.description
        
        cell.vehicleImage.kf.setImage(with: URL(string: data.image), placeholder: #imageLiteral(resourceName: "placeholder_Image"), options: [.requestModifier(getImageAuthorization())])
        
       
        cell.deleteBTN.indexPath = indexPath
        cell.deleteBTN.tag = data.usedcarid
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 141
    }
}

// Handling Tableview delegates
extension PLSellMyCarVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = sellmyListDataSource.SellCarListData[indexPath.row]
        if data.usedcarid != 0 {
            let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLSellMyCarDetailsVC") as! PLSellMyCarDetailsVC
            segueVC.carId = data.usedcarid
            self.present(segueVC, animated: true, completion: nil)

        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row < sellmyListDataSource.SellCarListData.count - 5, !isPaginatedCallOnGoing, sellmyListDataSource.SellCarListData.count < carCount {
            getSellCarListWithoutActivity(isPaginated: true)
            isPaginatedCallOnGoing = true
        }
    }
    
}


// Mark:- Handling web service
extension PLSellMyCarVC {
    private func getSellCarList(){
        self.parameterSellCarList["user_id"] = PLAppState.session.userID
        self.parameterSellCarList["page"] = pagination

        WebServices.postMethod(url:"getsellmycarlist" ,
                               parameter: self.parameterSellCarList,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    let errorcode = result["errorcode"] as! Int
                    if errorcode == 0 {
                        let data = result["data"] as! [[String:Any]]
                        self.sellmyListDataSource = SellMyCarListModel().getSellCarList(Data: data)
                        if self.sellmyListDataSource.SellCarListData.count == 0 {
                            self.isSellMyCarEmpty = true
                        } else {
                            self.isSellMyCarEmpty = false
                        }
                        
                        BaseThread.asyncMain {
                            self.setUpInterface()
                            self.pagination += 1
                        }

                    }
                    
                }
                else
                {
                    //showBanner(message: "Oops error..!")
                }
        })
        
        
    }
    
    private func getSellCarListWithoutActivity(isPaginated: Bool = false) {
        self.parameterSellCarList["user_id"] = PLAppState.session.userID
        if isPaginated {
            self.parameterSellCarList["page"] = pagination
        }else {
            pagination = 0
            self.parameterSellCarList["page"] = pagination
        }
        
        WebServices.postMethodWithoutActivity(url:"getsellmycarlist" ,
                                              parameter: self.parameterSellCarList,
                                              CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    let errorcode = result["errorcode"] as! Int
                    if errorcode == 0 {
                        let data = result["data"] as! JsonArray
                        
                        BaseThread.asyncMain {
                            if isPaginated {
                                let newSource = SellMyCarListModel().getSellCarList(Data: data)
                                self.sellmyListDataSource.SellCarListData.append(contentsOf: newSource.SellCarListData)
                            }else {
                                self.sellmyListDataSource = SellMyCarListModel().getSellCarList(Data: data)
                            }
                            
                            if !isPaginated {
                                
                                if let count = result["sellmycarcount"] as? Int {
                                    self.carCount = count
                                }
                                
                                if self.sellmyListDataSource.SellCarListData.count == 0 {
                                    self.isSellMyCarEmpty = true
                                } else {
                                    self.isSellMyCarEmpty = false
                                }
                                
                                //print("notPaginatedCalll")
                            }
                            
                            self.setUpInterface()
                            self.pagination += 1
                            self.isPaginatedCallOnGoing = false

                        }

                    }
                    
                }
                else
                {
                    //showBanner(message: "Oops error..!")
                }
        })
        
    }
    
    func deleteSellCarList(usedcarID:Int, index: Int){
        
        var parameterDeleteSellCar = [String:Any]()
        
        let id = PLAppState.session.userID.description
        parameterDeleteSellCar["user_id"] = id
        parameterDeleteSellCar["usedcarid"] = usedcarID
        WebServices.postMethod(url:"deletesellmycar" ,parameter: parameterDeleteSellCar, CompletionHandler: { (isFetched, result) in
                if isFetched
                {
                    let errorcode = result["errorcode"] as! Int
                    if errorcode == 0 {
                        BaseThread.asyncMain {
                            self.sellmyListDataSource.SellCarListData.remove(at: index)
                            self.vehicleTV.reloadData()
                            
//                            self.sellmyListDataSource.SellCarListData.removeAll()
//                            self.getSellCarList()
                        }
                    }
                }
                else
                {
                    showBanner(message: "Vehicle not deleted.Try again")
                }
        })
        
    }
    
}





class SellMyCarListModel {
    var SellCarListData = [PLSellMyCarListModel]()
    
    func getSellCarList(Data:[[String:Any]]) -> SellMyCarListModel {
        
        for i in 0..<Data.count {
            let carDetail = Data[i]
            let carData = PLSellMyCarListModel()
            
            carData.usedcarid = carDetail["usedcarid"] as? Int ?? -1
            
            if let year = carDetail["model_year"] as? Int {
                carData.modelYear = year.description
            }else {
                carData.modelYear = "Not Mentioned"
            }
            
            carData.image = carDetail["image"] as? String ?? "Not Mentioned"
            carData.brand = carDetail["brand"] as? String ?? "Not Mentioned"
            carData.model = carDetail["model"] as? String ?? "Not Mentioned"
            carData.variant = carDetail["variant"] as? String ?? "Not Mentioned"
            
            SellCarListData.append(carData)
        }
        
        return self
    }
    
    
}

class PLSellMyCarListModel {
    var usedcarid = -1
    var modelYear = "No Data"
    var image = "No Data"
    var brand = "No Data"
    var model = "No Data"
    var variant = "No Data"
    
    
}


