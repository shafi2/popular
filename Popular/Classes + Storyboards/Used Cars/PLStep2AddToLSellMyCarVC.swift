//
//  PLStep2AddToLSellMyCarVC.swift
//  Popular
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Alamofire

class PLStep2AddToLSellMyCarVC: UIViewController, InterfaceSettable, SideOptionsDelegate, UITextFieldDelegate {
    
    @IBOutlet var makeLBL: UILabel!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var yearTF: UITextField!
    @IBOutlet var makeBTN: UIButton!
    @IBOutlet var ModelBTN: UIButton!
    @IBOutlet var varientBTN: UIButton!
    
    fileprivate var dataSourceModels: [String] = []
    fileprivate var dataSourceVarients: [String] = []

    fileprivate var isTappedModelDropDown: Bool = true

    final var imageOfCar = UIImage()
    final var expPrice = Double()
    final var regNum = String()
    final var selectedCarDetails: PLSellMyCarMyVehiclesModel?
    

    var carModelValue = [String]()
    var carModelID = [Int]()
    var carVarientValue = [String]()
    var carVarientID = [Int]()
    
    private var selectedIndexPathModel: IndexPath?
    private var selectedIndexPathMake: IndexPath?
    private var selectedIndexPathVariant: IndexPath?
    private var selectedIndexPathLocation: IndexPath?

    private var selectedModelId: Int = 0
    private var oldModelId: Int = 0
    private var selectedMakeId: Int = 0
    private var oldMakeId: Int = 0
    private var selectedVariantId: Int = 0
    private var locationId: Int = 0
    private enum optionsTypes: Int {
        case make
        case model
        case variant
        case location
    }
    private var chosenOption: optionsTypes = .make
    private var modelArray: [PLSideOptionsModel] = []
    private var varientArray: [PLSideOptionsModel] = []
    private lazy var placeHolderModel = "Model"
    private lazy var placeHolderVariant = "Variant"
    private var isSelectedMake: Bool = false
    private var isSelectedModel: Bool = false
    private var locArry = [String:Any]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLocation()
        setUpInterface()
        
    }

    func setUpInterface() {
        if let selectedCarDetails = selectedCarDetails {
            makeLBL.text = selectedCarDetails.makename
            modelLBL.text = selectedCarDetails.carmodel
            varientLBL.text = selectedCarDetails.variantname
            
            selectedMakeId = selectedCarDetails.makenid
            selectedModelId = selectedCarDetails.carmodelid
            selectedVariantId = selectedCarDetails.variantid
            isSelectedMake = true
            isSelectedModel = true
            if BaseValidator.isNotEmpty(string: makeLBL.text) && modelLBL.text != "Make" && selectedMakeId != 0 {
                makeBTN.isUserInteractionEnabled = false
            }else {
                makeBTN.isUserInteractionEnabled = true

            }
            if BaseValidator.isNotEmpty(string: modelLBL.text) && modelLBL.text != "Model" && selectedModelId != 0 {
                ModelBTN.isUserInteractionEnabled = false
            }else {
                ModelBTN.isUserInteractionEnabled = true

            }
            if BaseValidator.isNotEmpty(string: varientLBL.text) && varientLBL.text != "Variant" && selectedVariantId != 0 {
                varientBTN.isUserInteractionEnabled = false
            }else {
                varientBTN.isUserInteractionEnabled = true

            }

            
        }
        
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    //Mark:- delegate from side options
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        //print("Data.id",data.id,data.name)

        switch chosenOption {
        case .make:
            selectedIndexPathMake = indexPath
            selectedMakeId = data.id
            makeLBL.text = data.name
            if selectedMakeId != oldMakeId {
                modelArray.removeAll()
                getCarModels()
                modelLBL.text = placeHolderModel
                selectedModelId = 0
                selectedVariantId = 0
                varientLBL.text = placeHolderVariant

            }
            oldMakeId = selectedMakeId
            isSelectedMake = true
        case .model:
            selectedIndexPathModel = indexPath
            selectedModelId = data.id
            modelLBL.text = data.name
            if selectedModelId != oldModelId {
                varientArray.removeAll()
                getCarVarients()
                selectedVariantId = 0
                varientLBL.text = placeHolderVariant

            }
            oldModelId = selectedModelId
            isSelectedModel = true
        case .variant:
            selectedIndexPathVariant = indexPath
            selectedVariantId = data.id
            varientLBL.text = data.name
        case .location:
            selectedIndexPathLocation = indexPath
            locationId = data.id
            locationLBL.text = data.name
            
        }
        
    }
    
    @IBAction func makeTapped(_ sender: UIButton) {
        
        chosenOption = .make
        showSideOption(presentOn: self, delegate: self, source: GenericCarDetails.shared.makes, filtertype: .anywhere, indexPath: selectedIndexPathMake)

    }
    
    @IBAction func modelTapped(_ sender: UIButton) {
        if isSelectedMake {
            chosenOption = .model
            showSideOption(presentOn: self, delegate: self, source: modelArray, filtertype: .anywhere, indexPath: selectedIndexPathModel)
        }else {
            showBanner(message: "Please select Make")
        }

    }
    
    @IBAction func varientTapped(_ sender: UIButton) {
        if isSelectedModel {
            chosenOption = .variant
            showSideOption(presentOn: self, delegate: self, source: varientArray, filtertype: .anywhere, indexPath: selectedIndexPathVariant)

        }else {
            showBanner(message: "Please select Model")

        }

    }
    
    @IBAction func locationTapped(_ sender: UIButton) {
        chosenOption = .location
        if !locArry.isEmpty {
            showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! JsonArray, isStringId: false), indexPath: selectedIndexPathLocation)
            
        }else {
            getLocation(isFromLocationBTN: true)
        }

    }
    
    // text field delegate for limitig 4 digits year entry
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    @IBAction func finishedTapped(_ sender: UIButton) {
        
        if isValidData() {
            submitSellingCarDetails()
        }
        
       // PLAddedToSellMyCarNotifier.shared.isNotAddedVehicle = false
        
    }
    
    private func isValidData() ->Bool {
        
        if BaseValidator.isNotEmpty(string: makeLBL.text) && modelLBL.text != "Make" && selectedMakeId != 0 &&
            BaseValidator.isNotEmpty(string: modelLBL.text) && modelLBL.text != "Model" && selectedModelId != 0 &&
            BaseValidator.isNotEmpty(string: varientLBL.text) && varientLBL.text != "Variant" && selectedVariantId != 0 &&
            BaseValidator.isNotEmpty(string: yearTF.text) && yearTF.text != "Model Year" &&
            BaseValidator.isNotEmpty(string: locationLBL.text) && modelLBL.text != "Location" && locationId != 0 {
            
            if BaseValidator.isValid(digits: yearTF.text!) {
                let yearEntry = Int(yearTF.text!)!
                
                if yearEntry <= Date().nextYear {
                    return true
                }else {
                    showBanner(message: "Please enter a valid year")
                    return false
                }
            }else {
                showBanner(message: "Please enter a valid digit")
                return false
            }
            
        }else {
            showBanner(message: "Please fill all fields")

            
            
            return false
        }
        
        
    }
    
}



// Mark:- Handling web service
extension PLStep2AddToLSellMyCarVC {
    
    private func getMakeData(completion: @escaping((Bool) -> ())){
        let url = "getMake"
        
        WebServices.postMethodWithoutActivity(url: url, parameter: [:]) { (isSucceeded, response) in
            
            guard isSucceeded else { return }
            guard let responseData = response["data"] as? JsonArray else { return }
            
            let resultArray = PLSideOptionsModel.getData(fromArray: responseData, idKey: "id", nameKey: "name", isStringId: false)
            BaseThread.asyncMain {
                GenericCarDetails.shared.makes = resultArray
            }
            completion(true)
        }
        
    }

    private func getCarModels(){
        let id = PLAppState.session.userID.description
        var parameterModelList = [String:Any]()
        parameterModelList["user_id"] = id
        parameterModelList["makeid"] = selectedMakeId
        WebServices.postMethod(url:"getModel" , parameter: parameterModelList, CompletionHandler: { (isFetched, result) in
            
            if isFetched
            {
                print(parameterModelList)
                guard let data = result["data"] as? [[String:Any]] else { return }
                let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                BaseThread.asyncMain {
                    self.modelArray = modeldata
//                    GenericCarDetails.shared.models = modeldata
                }
            }
        })
    }
    
    private func getCarVarients(){
        let id = PLAppState.session.userID.description
        var parameterModelList = [String:Any]()
        parameterModelList["user_id"] = id
        parameterModelList["model_id"] = selectedModelId
        print(parameterModelList)
        WebServices.postMethod(url:"getvariantList" , parameter: parameterModelList, CompletionHandler: { (isFetched, result) in
            
            if isFetched
            {
                //print(parameterModelList)
                guard let data = result["data"] as? [[String:Any]] else { return }
                let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                BaseThread.asyncMain {
                    self.varientArray = modeldata
                    //GenericCarDetails.shared.variants = modeldata
                }
            }
        })
    }

    private func getLocation(isFromLocationBTN: Bool = false) {
        
        let url: String = "get/locations?type=5"
        BaseThread.asyncMain {
            WebServices.getMethodWithOutActivity(url:url, parameter: [:], CompletionHandler: { (isFetched, result) in
                if isFetched
                {
                    self.locArry = result
                    if isFromLocationBTN {
                        BaseThread.asyncMain {
                            self.locationTapped(UIButton())
                        }
                    }
                    
                }
                else
                {
                }
                
            })
        }
    }
    
    private func submitSellingCarDetails(){
        let url = "addtosellmycar"
        let id = PLAppState.session.userID.description
        var parameters = Json()
        var imageParam = JsonImage()

        parameters = ["user_id": id,
            "carmodelid": selectedModelId.description,
            "expected_price": expPrice.description,
            "reg_no": regNum,
            "model_year": yearTF.text!,
            "makeid": selectedMakeId.description
            ]
        
        if selectedVariantId != 0 {
            parameters["variantid"] = selectedVariantId.description
        }
        
        if let selectedCar = selectedCarDetails {
            if selectedCar.image.isNotEmpty {
                parameters["images"] = selectedCar.image
                
                WebServices.postMethod(url: url, parameter: parameters) { (isSucceeded, response) in
                    
                    if isSucceeded {
                        BaseThread.asyncMain {
                            showBanner(message: "Added Successfully")
                            if let prevVC = self.presentingViewController?.presentingViewController?.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLSellMyCarVC {
                                //print("prevVCCCC")
                                prevVC.reloadVC()
                                self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                                
                            }
                        }
                    }else {
                        
                    }
                    
                }
                
            }
        }else {
            imageParam["images[]"] = [imageOfCar]
            WebServices.postMethodMultiPartImage(url: url, parameter: parameters, imageParameter: imageParam) { (isSucceeded, response) in
                
                if isSucceeded {
                    BaseThread.asyncMain {
                        showBanner(message: "Added Successfully")
                        if let prevVC = self.presentingViewController?.presentingViewController?.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLSellMyCarVC {
                            //print("prevVCCCC")
                            prevVC.reloadVC()
                            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            
                        }
                    }
                }else {
                    
                }
                
                
            }
        }
        
        
        
        


       
    }
    
}


/*
 
 
 
 fileprivate func setUpDropDown() {
 let appearance = DropDown.appearance()
 appearance.cellHeight = 50
 appearance.backgroundColor = UIColor.white
 appearance.selectionBackgroundColor = UIColor.lightGray
 appearance.separatorColor = UIColor.lightGray
 appearance.cornerRadius = 4
 appearance.shadowColor = UIColor.lightGray
 appearance.shadowOpacity = 0.9
 appearance.shadowRadius = 2
 appearance.animationduration = 0.2
 appearance.textColor = .black
 
 optionDropDown.dismissMode = .automatic
 optionDropDown.direction = .any
 optionDropDown.anchorView = modelLBL
 let offSet = modelLBL.frame.height + 2
 optionDropDown.bottomOffset = CGPoint(x: 0, y: offSet)
 optionDropDown.topOffset = CGPoint(x: 0, y: -offSet)
 optionDropDown.selectionAction = { (index: Int, item: String) in
 self.didSelectDropDown(item: item, at: index)
 }
 
 }
 
 let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
 let base64Credentials = credentialData.base64EncodedString(options: [])

 Alamofire.upload(multipartFormData:
 { multipartFormData in
 
 for (key, value) in parameters
 {
 multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
 //multipartFormData.append((value as AnyObject).data(using: .utf8)!, withName: key)
 }
 
 let imageData1:Data = (UIImageJPEGRepresentation(self.imageOfCar, 1))!
 
 multipartFormData.append(imageData1, withName: "images", fileName: "Proof1.jpeg", mimeType: "image/jpeg")
 
 
 },to: "\(baseURL)addtosellmycar",
 headers : ["Content-Type":"application/x-www-form-urlencoded", "Authorization": "Basic \(base64Credentials)"],
 encodingCompletion:
 { encodingResult in
 switch encodingResult {
 case .success(let upload, _, _):
 upload.responseJSON(completionHandler:
 { response in
 
 print(response.result)
 print("here")
 
 })
 
 
 case .failure(let encodingError):
 
 print(encodingError)
 
 }
 }
 )
 */
