//
//  PLThankingVC.swift
//  Popular
//
//  Created by Appzoc on 16/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

enum PLDismissType: Int {
    case single
    case double
    case tripple
}

class PLThankingVC: UIViewController {

    final var dismissType: PLDismissType = .single
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    @IBAction func continueTapped(_ sender: UIButton) {
        switch dismissType {
        case .single:
            dismiss()
        case .double:
            self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        case .tripple:
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
    }
    

}
