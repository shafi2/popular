//
//  PLLifestyle+DataModels.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//


extension PLLifestyleVC{
    /*
    id: 1,
    name: "Roof Lugguage Carrier",
    model: "WEHOLITM",
    price: 4500,
    car_model: 1,
    car_varient: 1,
    category: 2,
    type: 1,
    image: "storage/app/uploads/newcar_images/RKxqjySdIVIT0xTV8V0BVUwSJKjeD77v44U6rtQB.png",
    updated_at: "2018-08-29 05:42:01",
    created_at: "2018-08-21 06:00:03",
    deleted_at: "2018-08-20 18:30:00",
    product_fav_count: 0
    */
    struct LifeStyleDataModel: Codable{
        let id:Int
        let name:String
        let model:String
        let price:Int
        let carModel:Int
        let carVariant:Int
        let category:Int
        let type:Int
        let image:String
        var favProduct:Int
        
        private enum CodingKeys: String, CodingKey{
            case id, name, model, price, carModel = "car_model", carVariant = "car_varient", category, type, image, favProduct = "product_fav_count"
        }
    }
}
