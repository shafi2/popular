//
//  PLLifestyleCartVC.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleCartVC: UIViewController {

    
    @IBOutlet var tableRef: UITableView!
    
    //Checkout Section Outlets
    @IBOutlet var totalAmountLBL: UILabel!
    @IBOutlet var taxLBL: UILabel!
    @IBOutlet var amountPayable: UILabel!
    
    @IBOutlet var cartEmptyIndicatorLBL: UILabel!
    
    @IBOutlet var bottomContainer: BaseView!
    
    var cartSource:[LifestyleCartDataModel.CartModel] = []{
        didSet{
            if cartSource.count == 0{
                DispatchQueue.main.async {
                    self.cartEmptyIndicatorLBL.isHidden = false
                    self.bottomContainer.isHidden = true
                }
            }else{
                DispatchQueue.main.async {
                    self.cartEmptyIndicatorLBL.isHidden = true
                    self.bottomContainer.isHidden = false
                }
                
            }
        }
    }
    
    var passingData:LifestyleCartDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpWebcall()
    }
    
   

    @IBAction func addCounterTapped(_ sender: UIButton) {
        //print("User interaction ",sender.isUserInteractionEnabled)
        let param = ["type":"1","user_id":PLAppState.session.userID.description,"product_id":"\(cartSource[sender.tag].productID)"]
        print("Paramter ",param)
        sender.isUserInteractionEnabled = false
        WebServices.postMethodWithoutActivity(url: "add/cart", parameter: param) { (isComplete, json) in
            if isComplete{
                print("Added count")
                DispatchQueue.main.async {
                    self.cartSource[sender.tag].quantity += 1
                    self.tableRef.reloadData()
                    self.refreshList()
                    sender.isUserInteractionEnabled = true
                    //print("User interaction on success",sender.isUserInteractionEnabled)
                }
            }else{
                sender.isUserInteractionEnabled = true
                 print("User interaction on failure",sender.isUserInteractionEnabled)
            }
        }
    }
    
    @IBAction func subtractCountTapped(_ sender: UIButton) {
         //print("User interaction ",sender.isUserInteractionEnabled)
        if cartSource[sender.tag].quantity > 1{
            let param = ["type":"0","user_id":PLAppState.session.userID.description,"product_id":"\(cartSource[sender.tag].productID)"]
            sender.isUserInteractionEnabled = false
            WebServices.postMethodWithoutActivity(url: "add/cart", parameter: param) { (isComplete, json) in
                if isComplete{
                    print("Subracted count")
                    DispatchQueue.main.async {
                        self.cartSource[sender.tag].quantity -= 1
                        self.tableRef.reloadData()
                        self.refreshList()
                        sender.isUserInteractionEnabled = true
                        //print("User interaction on success",sender.isUserInteractionEnabled)
                    }
                }else{
                    sender.isUserInteractionEnabled = true
                    print("User interaction on failure",sender.isUserInteractionEnabled)
                }
            }
        }
    }
    
    /*
     delete cart:
     http://popular.dev.webcastle.in/api/delete/cart/1
     */
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        let param:[String:Any] = [:]
        WebServices.getMethodWith(url: "delete/cart/\(cartSource[sender.tag].id)", parameter: param) { (isComplete, json) in
            if isComplete{
                self.setUpWebcall()
            }
        }
    }
    
    
    @IBAction func proceedToCheckoutTapped(_ sender: UIButton) {
        let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestylePaymentVC") as! PLLifestylePaymentVC
        if let data = passingData{
            segueVC.totalAmount = data.totalAmount.description
            segueVC.taxAmount = data.tax.description
            segueVC.amountPayable = data.amountPayable.description
        }
        self.present(segueVC)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUpWebcall(){
        WebServices.getMethodWith(url: "get/cart/\(PLAppState.session.userID)", parameter: nil) { (isComplete, json) in
            if isComplete{
                let decoder = JSONDecoder()
                let trueJson = json["data"]
                do{
                    if let jsonData = serializeJSON(json: trueJson as Any){
                        let parsedData = try decoder.decode(LifestyleCartDataModel.self, from: jsonData)
                        self.cartSource = parsedData.cart
                        self.passingData = parsedData
                        self.tableRef.reloadData()
                        self.setUpBottomContainer(data: parsedData)
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func refreshList(){
        WebServices.getMethodWithOutActivity(url: "get/cart/\(PLAppState.session.userID)", parameter: nil) { (isComplete, json) in
            if isComplete{
                let decoder = JSONDecoder()
                let trueJson = json["data"]
                do{
                    if let jsonData = serializeJSON(json: trueJson as Any){
                        let parsedData = try decoder.decode(LifestyleCartDataModel.self, from: jsonData)
                        self.cartSource = parsedData.cart
                        //self.tableRef.reloadData()
                        self.passingData = parsedData
                        self.setUpBottomContainer(data: parsedData)
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func setUpBottomContainer(data:LifestyleCartDataModel){
        self.totalAmountLBL.text = "₹ " + data.totalAmount.description
        self.taxLBL.text = "+ ₹ " + data.tax.description
        self.amountPayable.text = "₹ " + data.amountPayable.description
        
    }
    
    
}
