//
//  PLLifestyleCart+Table.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleCartTVC: UITableViewCell{
    
    @IBOutlet var itemImage: BaseImageView!
    @IBOutlet var itemName: UILabel!
    @IBOutlet var itemModel: UILabel!
    @IBOutlet var itemPrice: UILabel!
    
    @IBOutlet var addBTNRef: UIButton!
    @IBOutlet var subtractBTNRef: UIButton!
    @IBOutlet var itemCountLBL: UILabel!
    @IBOutlet var deleteBTNRef: UIButton!
    
    func configureCell(with data: PLLifestyleCartVC.LifestyleCartDataModel.CartModel, index: IndexPath){
        addBTNRef.tag = index.row
        subtractBTNRef.tag = index.row
        deleteBTNRef.tag = index.row
        itemCountLBL.text = data.quantity.description
        
        itemName.text = data.cartProduct?.name ?? ""
        itemModel.text = "Model : \(data.cartProduct?.model ?? "")"
        itemPrice.text = "Rs : \(data.cartProduct?.price ?? 0)/-"
        //print("URL : \(baseURLImage)\(data.cartProduct?.image)")
        if let _ = data.cartProduct{
            itemImage.kf.setImage(with: URL(string: baseURLImage + (data.cartProduct?.image)!), placeholder: nil, options: [.requestModifier(getImageAuthorization())])
        }
    }
    
    override func prepareForReuse() {
        itemCountLBL.text = ""
        itemName.text = ""
        itemModel.text = ""
        itemPrice.text = ""
        itemImage.image = UIImage()
    }
    
}

extension PLLifestyleCartVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PLLifestyleCartTVC") as! PLLifestyleCartTVC
        cell.configureCell(with: cartSource[indexPath.row], index: indexPath)
        return cell
    }
}
