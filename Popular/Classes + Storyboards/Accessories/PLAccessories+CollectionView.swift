//
//  PLAccessories+CollectionView.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class PLAccessoriesCVC: UICollectionViewCell{
    
    @IBOutlet var mainLabel: UILabel!
    
    override func prepareForReuse() {
        mainLabel.textColor = UIColor.white.withAlphaComponent(0.3)
    }
}

extension PLAccesoriesVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLAccessoriesCVC", for: indexPath) as! PLAccessoriesCVC
        cell.mainLabel.text = collectionSource[indexPath.row]
        if indexPath.row == selectedCollectionSection{
            cell.mainLabel.textColor = UIColor.white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCollectionSection = indexPath.row
        switch indexPath.row {
        case 0:
            currentSource = interiorSource
        case 1:
            currentSource = exteriorSource
        case 2:
            currentSource = carCareSource
        default:
            currentSource = interiorSource
        }
        tableRef.reloadData()
        collectionRef.reloadData()
    }
}

extension PLAccesoriesVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: collectionView.frame.width/3-5, height: collectionView.frame.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 0, 0, 10)
//    }
}
