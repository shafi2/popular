//
//  PLAccessories+DataModel.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

/*
id: 4,
name: "Stereo",
model: "HEFEFSF",
price: 200,
car_model: 1,
car_varient: 1,
category: 0,
type: 2,
image: "storage/app/uploads/Banner/C1PQTrw0sg6XYhTqeIdkCg4oMS8KreHh2xWcgPZa.png",
updated_at: "2018-08-22 05:13:22",
created_at: "2018-08-21 06:21:50",
deleted_at: "2018-08-20 18:30:00"
*/
extension PLAccesoriesVC{
    
    struct AccessoryModel: Codable {
        let id:Int
        let name:String
        let model:String
        let price:Int
        let carModel:Int
        let carVariant:Int
        let category:Int
        let type:Int
        let image:String
        let updatedAt:String
        let createdAt:String
        let deletedAt:String
        
        private enum CodingKeys: String, CodingKey{
            case id, model, price, carModel = "car_model", carVariant = "car_varient", category, type, image, name, updatedAt = "updated_at", createdAt = "created_at", deletedAt = "deleted_at"
        }
    }
    
    
    enum AccessorySortType: Int{
        case latest = 0
        case lowToHigh = 1
        case highToLow = 2
        case none = 10
    }
}

protocol AccessorySortSelector {
    func setSortCategory()
}

protocol AccessorySearch{
    func performSearch(searchKey:String)
}

