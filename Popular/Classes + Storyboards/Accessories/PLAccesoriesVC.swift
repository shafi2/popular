//
//  PLAccesoriesVC.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

var accessorySortType:PLAccesoriesVC.AccessorySortType = .none

class PLAccesoriesVC: UIViewController {
    
    
    
    @IBOutlet var listEmptyIndicatorLBL: UILabel!
    @IBOutlet var collectionRef: UICollectionView!
    
    @IBOutlet var tableRef: UITableView!
    let refreshControl = UIRefreshControl()
    
    var collectionSource = ["Interior","Exterior","Car Care"]
    var interiorSource:[AccessoryModel] = []
    var exteriorSource:[AccessoryModel] = []
    var carCareSource:[AccessoryModel] = []
    var selectedCollectionSection:Int = 0
    
    var currentSource:[AccessoryModel] = []{
        didSet{
            if currentSource.count == 0{
                DispatchQueue.main.async {
                   self.listEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.listEmptyIndicatorLBL.isHidden = true
                }
            }
        }
    }
    
    var selectedModel:Int = 0
    var carModelList:[PLSideOptionsModel] = []
    var selectedCarModel:PLSideOptionsModel?
    
    override func viewWillAppear(_ animated: Bool) {
        setUpWebCall()
        getModelList()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        accessorySortType = .latest
        setUpInterFace()
    }
    
    func setUpInterFace(){
        tableRef.estimatedRowHeight = 136
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to Refresh")
        refreshControl.addTarget(self, action: #selector(setUpWebCall), for: .valueChanged)
        tableRef.addSubview(refreshControl)
        addSwipeToTable()
    }
    
    func addSwipeToTable(){
        let swipeGestureRight = UISwipeGestureRecognizer()
        swipeGestureRight.direction = .right
        swipeGestureRight.addTarget(self, action: #selector(swipeRight))
        self.tableRef.addGestureRecognizer(swipeGestureRight)
        let swipeGestureLeft = UISwipeGestureRecognizer()
        swipeGestureLeft.direction = .left
        swipeGestureLeft.addTarget(self, action: #selector(swipeLeft))
        self.tableRef.addGestureRecognizer(swipeGestureLeft)
    }
    
    @objc func swipeLeft(){
        //print("Alt Right")
        if selectedCollectionSection < 2, selectedCollectionSection >= 0{
            selectedCollectionSection += 1
            switch selectedCollectionSection {
            case 0:
                currentSource = interiorSource
            case 1:
                currentSource = exteriorSource
            case 2:
                currentSource = carCareSource
            default:
                currentSource = interiorSource
            }
            tableRef.reloadData()
            collectionRef.reloadData()
        }
        
    }
    
    @objc func swipeRight(){
      //  print("Alt Left")
        if selectedCollectionSection <= 2, selectedCollectionSection > 0{
            selectedCollectionSection -= 1
            switch selectedCollectionSection {
            case 0:
                currentSource = interiorSource
            case 1:
                currentSource = exteriorSource
            case 2:
                currentSource = carCareSource
            default:
                currentSource = interiorSource
            }
            tableRef.reloadData()
            collectionRef.reloadData()
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    
    @IBAction func sortTapped(_ sender: UIButton) {
        let segueVC = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "PLAccessorySortVC") as! PLAccessorySortVC
        segueVC.delegate = self
        self.present(segueVC)
    }
    
    
    @IBAction func searchTapped(_ sender: UIButton) {
        let segueVC = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "PLAccessorySearchPopUpVC") as! PLAccessorySearchPopUpVC
        segueVC.delegate = self
        self.present(segueVC)
    }
    
    @IBAction func clearSelectedModelTapped(_ sender: UIButton) {
        selectedCarModel = nil
        selectedModel = 0
        setUpWebCall()
    }
    
    
    @IBAction func modelTapped(_ sender: UIButton) {
        showSideOption(presentOn: self, delegate: self, source: carModelList)
    }
    /*
    http://popular.dev.webcastle.in/api/product_callback
    user_id:2
    product_id:2
    */
    @IBAction func requestCallBackTapped(_ sender: UIButton) {
       // print("Request callback")
        if PLAppState.session.isLoggedIn{
            WebServices.postMethod(url: "product_callback", parameter: ["user_id":"\(PLAppState.session.userID)","product_id":"\(sender.tag)"]) { (isComplete, json) in
                if isComplete{
                    let segueVC = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "PLAccessoriesCallBackVC") as! PLAccessoriesCallBackVC
                    self.present(segueVC)
                }
            }
        }else{
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
    }
    
    
//    func getModelList(){
//        WebServices.postMethod(url: "getModelList", parameter: ["user_id":"\(PLAppState.session.userID)"]) { (isCompleted, json) in
//            if isCompleted{
//                let data = json["data"] as? [[String:Any]] ?? []
//                let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "carmodelid", nameKey: "carmodelname", isStringId: false)
//                self.carModelList = modeldata
//            }
//        }
//    }
    
    func getModelList(){
        WebServices.getMethodWith(url: "get/accessories_model", parameter: ["user_id":"\(PLAppState.session.userID)"]) { (isCompleted, json) in
            if isCompleted{
                let data = json["data"] as? [String:Any] ?? [:]
                let variantData = data["model"] as? [[String:Any]] ?? []
                let modeldata = PLSideOptionsModel.getData(fromArray: variantData, idKey: "id", nameKey: "name", isStringId: false)
                self.carModelList = modeldata
            }
        }
    }
    
    @objc func setUpWebCall(){
        print("Model ",selectedCarModel?.name," ID ",selectedModel," ",selectedCarModel?.id)
        WebServices.getMethodWith(url: "get/accessories", parameter: ["category":"0","sort":"\(accessorySortType.rawValue)","model_id":"\(selectedModel)"]) { (isComplete, json) in
            if isComplete{
                print("Completed")
                self.parseData(for: 0, data: json["data"] as! [Any])
            }
        }
        WebServices.getMethodWith(url: "get/accessories",parameter: ["category":"1","sort":"\(accessorySortType.rawValue)","model_id":"\(selectedModel)"]) { (isComplete, json) in
            if isComplete{
                print("Completed")
                self.parseData(for: 1, data: json["data"] as! [Any])
            }
        }
        WebServices.getMethodWith(url: "get/accessories", parameter: ["category":"2","sort":"\(accessorySortType.rawValue)","model_id":"\(selectedModel)"]) { (isComplete, json) in
            if isComplete{
                print("Completed")
                self.parseData(for: 2, data: json["data"] as! [Any])
            }
        }
    }
    
    func parseData(for section:Int, data:[Any]){
        do{
            let decoder = JSONDecoder()
            if let jsonData = serializeJSON(json: data) {
                let parsedData = try decoder.decode([AccessoryModel].self, from: jsonData)
                switch section{
                case 0:
                    interiorSource = parsedData
                    if selectedCollectionSection == 0{
                        DispatchQueue.main.async {
                            self.currentSource = self.interiorSource
                            self.refreshControl.endRefreshing()
                            self.tableRef.reloadData()
                        }
                    }
                    print("Interior")
                case 1:
                    exteriorSource = parsedData
                    if selectedCollectionSection == 1{
                        DispatchQueue.main.async {
                            self.currentSource = self.exteriorSource
                            self.refreshControl.endRefreshing()
                            self.tableRef.reloadData()
                        }
                    }
                    print("Exterior")
                case 2:
                    carCareSource = parsedData
                    if selectedCollectionSection == 2{
                        DispatchQueue.main.async {
                            self.currentSource = self.carCareSource
                            self.refreshControl.endRefreshing()
                            self.tableRef.reloadData()
                        }
                    }
                    print("Car Care")
                default:
                    print("default")
                }
            }else {
                
            }
        }catch{
            print(error)
        }
    }

   

}

extension PLAccesoriesVC: AccessorySortSelector, SideOptionsDelegate, AccessorySearch {

    func performSearch(searchKey: String) {
        WebServices.getMethodWith(url: "get/accessories", parameter: ["category":"\(selectedCollectionSection)","sort":"\(accessorySortType.rawValue)","model_id":"\(selectedModel)","search":"\(searchKey)"]) { (isComplete, json) in
            if isComplete{
                print("Completed")
                self.parseData(for: self.selectedCollectionSection, data: json["data"] as! [Any])
            }
        }
    }
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        selectedCarModel = data
        selectedModel = data.id
        setUpWebCall()
    }
    
    func setSortCategory() {
        DispatchQueue.main.async {
            self.setUpWebCall()
        }
    }
    
    
}
