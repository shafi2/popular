//
//  PLAccessoriesCallBackVC.swift
//  Popular
//
//  Created by Appzoc on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLAccessoriesCallBackVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25, animations: {
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            })
        }
    }

    @IBAction func continueTapped(_ sender: UIButton) {
      self.dismiss()
    }
    

}
