//
//  PLServiceHome.swift
//  Popular
//
//  Created by admin on 16/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import ImageSlideshow
import Kingfisher
import CHIPageControl


class PLServiceHome: UIViewController {
    
    @IBOutlet var slideShow: ImageSlideshow!
    fileprivate var currentSlideShowIndex: Double = 0
    @IBOutlet var imageBannerVW: ImageSlideshow!
    @IBOutlet var pageControl: CHIPageControlJaloro!
    
    fileprivate var propertySlidingImagesSource: [UIImage] = []
    var imageArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideShow.backgroundColor = UIColor.black
        slideShow.pageIndicator = nil
        self.getServiceBanner()
        let userUpdateNotification = Notification.Name("notificationLogged")
        NotificationCenter.default.addObserver(self, selector: #selector(popVC), name: userUpdateNotification, object: nil)
    }
    
    @objc fileprivate func popVC(){
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceOriginalPrice") as! PLServiceOriginalPrice
        self.present(vc, animated: true)
        viewWillAppear(true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        //_ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PLHomeVC") as! PLHomeVC
        self.dismiss()
    }
    
    @IBAction func serviceBook(_ sender: Any) {
        
        let id = PLAppState.session.userID
        
        if id == 0
        {
            //showBanner(message: "Please login")
            
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
            
        }
        else
        {
            
            let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLBookYourService") as!PLBookYourService
            self.present(vc, animated: true)
        }
        
        
    }
    
    @IBAction func serviceCostCalcAction(_ sender: Any) {
        
        //showBanner(message: "Coming Soon")
        return
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceCostCalculator") as! PLServiceCostCalculator
        self.present(vc, animated: true)
        
        
//        let id = PLAppState.session.userID
//
//        if id == 0
//        {
//            //showBanner(message: "Please login")
//
//            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
//            present(segueVC, animated: true, completion: nil)
//
//        }
//        else
//        {
//
//            let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceCostCalculator") as! PLServiceCostCalculator
//            self.present(vc, animated: true)
//
//        }
        
        
        
    }
    
    @IBAction func serviceHistoryAction(_ sender: Any) {
        
        let id = PLAppState.session.userID
        
        if id == 0
        {
            //showBanner(message: "Please login")
            
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
            
        }
        else
        {
            let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceHistory") as! PLServiceHistory
            self.present(vc, animated: true)
        }
        
        
       
    }
}




extension PLServiceHome{
    func getServiceBanner()
    {
        //http://app.appzoc.com/popular/backend/api/getServiceBanner
        WebServices.getMethodWith(url: "getServiceBanner", parameter: ["":""],CompletionHandler: { (isFetched, result) in
            if isFetched
            {
                print(result)
                let data = result["data"] as! [String:Any]
                //print(data)
                let servicePrice = data["bannerimages"] as! [String]
                var propertyImagesInputs: [InputSource] = []
               // print(servicePrice)
                for item in servicePrice{
                    if let source = KingfisherSource(urlString: "\(item)", placeholder: nil, options: [.requestModifier(getImageAuthorization())]){
                        propertyImagesInputs.append(source)
                    }
                    self.slideShow.setImageInputs(propertyImagesInputs)
                    // self.propertySlidingImagesSource.setImageInputs(propertyImagesInputs)
                    self.slideShow.slideshowInterval = 5
                    self.slideShow.contentScaleMode = .scaleAspectFill
                    self.pageControl.numberOfPages = propertyImagesInputs.count
                    self.pageControl.progress = self.currentSlideShowIndex
                   // self.slideShow.pageControl.isHidden = true
                    self.slideShow.currentPageChanged = { index in
                        self.currentSlideShowIndex = Double(index)
                        self.pageControl.progress = self.currentSlideShowIndex
                    }
                }
                BaseThread.asyncMain {
                    
//                    for item in servicePrice{
//                        let imageViewTemp = UIImageView()
//                        imageViewTemp.kf.setImage(with: URL(string: (item as! String)), options: nil, progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
//                            self.propertySlidingImagesSource.append(image ?? UIImage())
//                            propertyImagesInputs.append(ImageSource(image: image ?? UIImage()))
//                            self.slideShow.setImageInputs(propertyImagesInputs)
//                            // self.propertySlidingImagesSource.setImageInputs(propertyImagesInputs)
//                            self.slideShow.slideshowInterval = 3
//                            self.slideShow.contentScaleMode = .scaleAspectFill
//                            self.pageControl.numberOfPages = propertyImagesInputs.count
//                            self.pageControl.progress = self.currentSlideShowIndex
//                            self.slideShow.pageControl.isHidden = true
//                            print(propertyImagesInputs)
//                            self.slideShow.currentPageChanged = { index in
//                                self.currentSlideShowIndex = Double(index)
//                                self.pageControl.progress = self.currentSlideShowIndex
//                            }
//                        })
//
//
//                    }
                    
                }
            }
            else
            {
                //showBanner(message: "Oops error..!")
            }
        })
        
        
        
    }
}

