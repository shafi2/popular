//
//  PLBookYourService.swift
//  Popular
//
//  Created by Appzoc-Macmini on 24/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher
var carModelPostId = String()
var carVarientPostId = String()
var regPostNo = String()

class carmodelCell : UICollectionViewCell{
    @IBOutlet var carImage: BaseImageView!
    @IBOutlet var brandName: UILabel!
    @IBOutlet var modelName: UILabel!
}

class PLBookYourService: UIViewController, SideOptionsDelegate {
   
    @IBOutlet var mdelNameLBL: UILabel!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var numberTF: UITextField!
    @IBOutlet var vehicleListEmptyIndicatorLBL: UILabel!
    @IBOutlet var serviceBookCV: UICollectionView!
    
    //// model data
    var carModel = [PLSideOptionsModel]()
    var myVehicleList:[PLMyVehiclesVC.MyVehicleModel] = []{
        didSet{
            if myVehicleList.count == 0{
                DispatchQueue.main.async {
                    self.vehicleListEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.vehicleListEmptyIndicatorLBL.isHidden = true
                }
            }
        }
    }
    private var varientArray: [PLSideOptionsModel] = []
    private enum optionsTypes: Int {
        case model
        case variant
    }
    private var chosenOption: optionsTypes = .model
    private var isSelectedModel: Bool = false
    private var selectedIndexPathModel: IndexPath?
    private var selectedIndexPathVariant: IndexPath?
    private var selectedModelId: Int = 0
    private var oldModelId: Int = 0
    private var selectedVariantId: Int = 0
    private lazy var placeHolderModel = "Model"
    private lazy var placeHolderVariant = "Vehicle Variant"
    private var isWebCallGoingVarient: Bool = false
    private var isWebCallGoingModel: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.numberTF.autocapitalizationType = .allCharacters
        numberTF.attributedPlaceholder = NSAttributedString(string: "Register Number",
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getCarModels()
        self.getMyVehicles()
    }


    @IBAction func backButton(_ sender: Any) {
        self.dismiss()
    }
    
    @IBAction func bookServiceAction(_ sender: Any) {
        guard bookServiceValidate() else { return }
        regPostNo = numberTF.text!
        let vc = self.storyBoardService!.instantiateViewController(withIdentifier: "PLServiceBookDateVC") as! PLServiceBookDateVC
        present(vc)
    }

    @IBAction func modelAction(_ sender: Any) {
        chosenOption = .model
        if carModel.isEmpty {
            if !isWebCallGoingModel {
                getCarModels()
            }
        }else {
            showSideOption(presentOn: self, delegate: self, source: carModel)
        }
    }
    
    @IBAction func varientAction(_ sender: Any) {
        
        if isSelectedModel {
            chosenOption = .variant
            if varientArray.isEmpty {
                if !isWebCallGoingVarient {
                    getCarVarient()
                }
            }else {
                showSideOption(presentOn: self, delegate: self, source: varientArray, filtertype: .anywhere, indexPath: selectedIndexPathVariant)
            }
        }else {
            showBanner(message: "Please Select Model")
            
        }

    }
    
    
    //Mark:- delegate from side options
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
        switch chosenOption {
        case .model:
            mdelNameLBL.text = data.name
            carModelPostId = data.id.description
            selectedModelId = data.id
            selectedIndexPathModel = indexPath
            if selectedModelId != oldModelId {
                varientArray.removeAll()
                getCarVarient()
                selectedVariantId = 0
                varientLBL.text = placeHolderVariant
            }
            oldModelId = selectedModelId
            isSelectedModel = true
        case .variant:
            selectedIndexPathVariant = indexPath
            selectedVariantId = data.id
            carVarientPostId = data.id.description
            varientLBL.text = data.name
        }
        
        
    }

    
    private func bookServiceValidate()->Bool{
        
        if BaseValidator.isNotEmpty(string: numberTF.text) &&
            BaseValidator.isNotEmpty(string: mdelNameLBL.text) && mdelNameLBL.text != placeHolderModel && selectedModelId != 0 &&
            BaseValidator.isNotEmpty(string: varientLBL.text) && varientLBL.text != placeHolderVariant && selectedVariantId != 0 {
            
            if let regNo = numberTF.text, regNo.containSpecialCharactersIncludingHyphen(){
                showBanner(message: "Registration Number Cannot Include Special Characters")
                return false
            }else {
                return true
            }
            
        }else {
            
            if !BaseValidator.isNotEmpty(string: mdelNameLBL.text) || mdelNameLBL.text == placeHolderModel || selectedModelId == 0 {
                showBanner(message: "Please Select Model")
                return false

            }
            
            if !BaseValidator.isNotEmpty(string: varientLBL.text) || varientLBL.text == placeHolderVariant || selectedVariantId == 0 {
                showBanner(message: "Please Select Vehicle Variant")
                return false
            
            }
            
            if !BaseValidator.isNotEmpty(string: numberTF.text) {
                showBanner(message: "Please Enter Register Number")
                return false
            }
            
            return false
        }
        
    }

    
}

//Mark:- CollectionView Handling
extension PLBookYourService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myVehicleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! carmodelCell
        cell.modelName.text = myVehicleList[indexPath.row].carModel
        cell.brandName.text = myVehicleList[indexPath.row].makeName
        let imgURL = URL(string: baseURLImage + myVehicleList[indexPath.row].image)
        cell.carImage.kf.setImage(with: imgURL, placeholder: #imageLiteral(resourceName: "placeholderVehicle"), options: [.requestModifier(getImageAuthorization())])
        //setImage(with: imgURL, placeholder: #imageLiteral(resourceName: "placeholderVehicle"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        carModelPostId = "\(myVehicleList[indexPath.row].carModelID)"
        regPostNo = "\(myVehicleList[indexPath.row].registrationNo)"
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceBookDateVC") as! PLServiceBookDateVC
        self.present(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //        let valueSelected = carModelID[indexPath.row] as! Int
        //        carModelPostId = valueSelected.description
        //        let valueSelected2 = regNo[indexPath.row] as! String
        //        regPostNo = valueSelected2
        //        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceBookDateVC") as! PLServiceBookDateVC
        //        self.present(vc, animated: true)
    }

}


// Mark:- Web services handling
extension PLBookYourService {
    
    private func getMyVehicles(){
        var parameterModelList = [String:Any]()
        parameterModelList["user_id"] = PLAppState.session.userID.description
        WebServices.postMethod(url:"getMyVehicles" ,
                               parameter: parameterModelList,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    if let jsonData = result["data"] as? [[String:Any]]{
                        let decoder = JSONDecoder()
                        if let serializedData = serializeJSON(json: jsonData){
                            do{
                                let parsedData = try decoder.decode([PLMyVehiclesVC.MyVehicleModel].self, from: serializedData)
                                self.myVehicleList = parsedData
                                
                            }catch{
                                print(error)
                            }
                        }
                    }
                    BaseThread.asyncMain {
                        self.serviceBookCV.reloadData()
                    }
                }
                else
                {
                    //showBanner(message: "Oops error..!")
                }
        })
        
    }
    
    
    
    private func getCarModels(){
        let parameterModelList = [String:Any]()
        isWebCallGoingModel = true

        WebServices.getMethodWith(url:"getmodels" , parameter: parameterModelList, CompletionHandler: { (isFetched, result) in
            if isFetched
            {
                BaseThread.asyncMain {
                    let data = result["data"] as! [[String:Any]]
                    let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                    self.carModel = modeldata
                    self.isWebCallGoingModel = false

                }
            }
            else
            {
                self.isWebCallGoingModel = false

                print("Error-Not getting models")
            }
        })
        
    }

    
    private func getCarVarient() {
        var params = Json()
        params["user_id"] = PLAppState.session.userID.description
        params["model_id"] = selectedModelId
        isWebCallGoingVarient = true
        WebServices.postMethod(url: "getvariantList", parameter: params) { (isSucceeded, response) in
            guard let data = response["data"] as? JsonArray else {
                self.isWebCallGoingVarient = false
                return
            }
            
            BaseThread.asyncMain {
                let varintData = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                self.varientArray = varintData
                self.isWebCallGoingVarient = false

            }
            
        }
    }

    
}
