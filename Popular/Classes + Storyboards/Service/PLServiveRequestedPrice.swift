//
//  PLServiveRequestedPrice.swift
//  Popular
//
//  Created by Appzoc-Macmini on 20/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLServiveRequestedPrice: UIViewController {
    var approxPrice = ""
    var carModelID = ""
    var variantID = ""
    var kilometer = ""
    
    
    @IBOutlet var priceLBL: UILabel!
    
    override func viewDidLoad() {
        ///global value
        bookType = 0
        super.viewDidLoad()
         priceLBL.text = "₹ \(approxPrice) /-"
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func OriginalPriceAction(_ sender: Any) {
        _ = self.storyboard?.instantiateViewController(withIdentifier: "PLServiceOriginalPrice") as! PLServiceOriginalPrice
        //   secondViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
       // self.present(secondViewController, animated: false, completion: nil)
        self.carmodelEnquiry()
        
    }
}

extension PLServiveRequestedPrice{
    private func carmodelEnquiry(){
        let id = PLAppState.session.userID.description
        var parameter = [String:Any]()
        parameter["userid"] = id
        parameter["type"] = "12"
        parameter["carmodel_id"] = carModelID
        parameter["variant_id"] = variantID
        parameter["kilometer"] = kilometer
        WebServices.postMethod(url:"carmodelEnquiry" ,
                                         parameter: parameter,
                                         CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    
                    let data = result["message"] as! String
                    print(data)
                    BaseThread.asyncMain{
                         let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "PLServiceOriginalPrice") as! PLServiceOriginalPrice
                        
                        self.present(secondViewController, animated: false, completion: nil)
                    }
                    
                    
                }
                else
                {
                    //showBanner(message: "Oops error..!")
                }
        })
        
    }
}

