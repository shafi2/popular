//
//  PLServiceHistoryDetails.swift
//  Popular
//
//  Created by Appzoc-Macmini on 23/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLServiceHistoryDetails: UIViewController,UITextViewDelegate {
    var date = String()
    var kilometers = String()
    var workNature = String()
    var status = String()
    var serviceName:String = ""
    var serviceStatus:String = ""
    var serviceID:String = ""
    var placeholderLabel : UILabel!
    
    @IBOutlet var textView: UITextView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var dateLBL: UILabel!
    @IBOutlet var distanceLBL: UILabel!
    @IBOutlet var workNatureLBL: UILabel!
    @IBOutlet var statusLBL: UILabel!
    @IBOutlet var feedbackView: UIView!
    @IBOutlet var serviceNameLBL: UILabel!
    

    override func viewDidLoad() {
        /////place holder for text view
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Write feedback"
        placeholderLabel.font = UIFont.systemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
       // textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
        statusLBL.text = serviceStatus.capitalized
        serviceNameLBL.text = serviceName
        super.viewDidLoad()
        if(status == "0"){
            statusLBL.textColor = UIColor(red: 24/255, green: 195/255, blue: 54/255, alpha: 1)
            feedbackView.isHidden = true
            scrollView.isScrollEnabled = false
        }
        else{
            scrollView.isScrollEnabled = true
            statusLBL.textColor = UIColor(red: 200/255, green: 61/255, blue: 59/255, alpha: 1)
            feedbackView.isHidden = false
        }
     dateLBL.text = date
     distanceLBL.text = kilometers + " Kms"
     workNatureLBL.text = workNature 
       
    }
    
    
    @IBAction func submitFeedbackTapped(_ sender: UIButton) {
        if let feedbackText = textView.text, feedbackText.isNotEmpty, feedbackText != "Write feedback..."{
            textView.resignFirstResponder()
            let param = ["userid":"\(PLAppState.session.userID)","serviceid":"\(serviceID)","feedback":"\(feedbackText)"]
            WebServices.postMethod(url: "serviceHistory", parameter: param, CompletionHandler: { (isComplete, json) in
                if isComplete{
                    print("Complete")
                    let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceOriginalPrice") as! PLServiceOriginalPrice
                    vc.pageType = .feedback
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }else{
            showBanner(message: "Please Enter Feedback")
        }
    }
    
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write feedback..."{
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text == " "{
            textView.text = "Write feedback..."
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
       self.dismiss(animated:true, completion: nil)
    }
    
   


}
