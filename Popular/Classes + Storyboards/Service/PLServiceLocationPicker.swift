//
//  PLServiceLocationPicker.swift
//  Popular
//
//  Created by Appzoc-Macmini on 26/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
var locationPostId = Int()
var ishome = 0
class PLServiceLocationPicker: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    @IBOutlet var locationView: UIView!
    var branchId = [Int]()
    var branchName = [String]()
    
    @IBOutlet var yesImage: UIImageView!
    @IBOutlet var yesLabel: UILabel!
    @IBOutlet var noImage: UIImageView!
    
    @IBOutlet var noLabel: UILabel!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @IBOutlet var locationPicker: UIPickerView!
    var pickerData:[PLEnquiryPopUpVC.LocationModel] = []
    
    override func viewDidLoad() {
        locationView.isHidden = false
        super.viewDidLoad()
        getBranches()
        //pickerData = ["a","s","s","a"]
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func proceedTapped(_ sender: UIButton) {
        locationView.isHidden = true
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceBookType") as! PLServiceBookType
        self.present(vc, animated: true)
    }
    

    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = pickerData[row].name ?? ""
//        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.font:UIFont(name: "Roboto-Bold", size: 80)!,NSAttributedStringKey.foregroundColor:UIColor.selectionLightBlue])
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 20, weight: .bold),NSAttributedStringKey.foregroundColor:UIColor.selectionLightBlue])
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        locationPostId = pickerData[row].id ?? 0 //branchId[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    @IBAction func yesButtonAction(_ sender: Any) {
        guard validateLocation() else {return}
       // locationView.isHidden = true
        ishome = 1
       // var image : UIImage = UIImage(named:"checked-symbol-1.png")!
        DispatchQueue.main.async {
            self.yesImage.image = UIImage(named:"checked-symbol")
            self.yesLabel.textColor = UIColor.from(hex: "00C4C8")
            self.noLabel.textColor = UIColor.from(hex: "494949")
            self.noImage.image = UIImage(named:"close-gray-shaded")
        }
        
//        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceBookType") as! PLServiceBookType
//        self.present(vc, animated: true)
    }
    
    @IBAction func noButtonAction(_ sender: Any) {
//         locationView.isHidden = true
        guard validateLocation() else {return}
         ishome = 0
        DispatchQueue.main.async {
            self.yesImage.image = UIImage(named:"checked-gray-shaded")
            self.noImage.image = UIImage(named:"close-cyan-shaded")
            self.noLabel.textColor = UIColor.from(hex: "00C4C8")
            self.yesLabel.textColor = UIColor.from(hex: "494949")
        }
        
        
//        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceBookType") as! PLServiceBookType
//        self.present(vc, animated: true)
    }
    
    func validateLocation()->Bool{
        if locationPostId == 0{
            showBanner(message: "Please select location")
            return false
        }
        else{
            return true
        }
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}

extension PLServiceLocationPicker{
    
    func parseLocationData(data:[[String:Any]]){
        let decoder = JSONDecoder()
        if let trueData = serializeJSON(json: data){
            do{
                let parsedData = try decoder.decode([PLEnquiryPopUpVC.LocationModel].self, from: trueData)
                //self.locArry = parsedData
                self.pickerData = parsedData
                if parsedData.count > 0{
                    locationPostId = parsedData.first!.id ?? 0
                }
                DispatchQueue.main.async {
                    print("Parsed Data ",parsedData)
                    self.locationPicker.reloadAllComponents()
                }
                
            }catch{
                print(error)
            }
        }
    }

    
    func getBranches(){
    WebServices.getMethodWith(url: "get/locations?type=2", parameter: ["":""],CompletionHandler: { (isFetched, result) in
    if isFetched
    {
//    print(result)
//    let data = result["data"] as! [[String:Any]]
//        print("Count : ",data.count)
        if let dataSection = result["data"] as? [[String:Any]]{
            self.parseLocationData(data: dataSection)
        }
   
//    BaseThread.asyncMain {
//        for item in data{
//            let Id = item["id"] as! Int
//            let name = item["name"] as! String
//            self.branchId.append(Id)
//            self.branchName.append(name)
//            self.pickerData.append(name)
//        }
//        locationPostId = self.branchId[0]
//        self.locationPicker.reloadAllComponents()
//          }
        
    }else{
   // showBanner(message: "Oops error..!")
           }
    })
    }
    
    
}
    
    
    
    

