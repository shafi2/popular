//
//  PLServiceOriginalPrice.swift
//  Popular
//
//  Created by Appzoc-Macmini on 20/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLServiceOriginalPrice: UIViewController {
    
    var pageType:ThankYouType = .serviceCost
    
    @IBOutlet var thankYouDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if pageType == .feedback{
            thankYouDescription.text = "For your valuable feedback"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        if pageType == .feedback{
            self.dismiss(animated: true, completion: nil)
        }else if (bookType == 1){
            let dismissingController = self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController
            dismissingController?.dismiss(animated: false, completion: nil)
        }
        else{
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
    }
    


}

enum ThankYouType{
    case feedback
    case serviceCost
}
