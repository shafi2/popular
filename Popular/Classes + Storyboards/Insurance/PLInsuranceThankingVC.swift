//
//  PLInsuranceThankingVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

//protocol InsuranceThankingDelegate {
//    func didFinishSelecting(documentImages: [UIImage])
//}


class PLInsuranceThankingVC: UIViewController, InsuranceUploadDelegate {

    @IBOutlet var uploadLBL: UILabel!
    @IBOutlet var contactLBL: UILabel!
    @IBOutlet var uploadBTN: UIButton!
    
    //final var delegate: InsuranceThankingDelegate?
    final var documentImages: [UIImage] = []
    final var parameters: Json = Json()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactLBL.isHidden = true
        uploadBTN.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        //delegate?.didFinishSelecting(documentImages: documentImages)
        dismiss()
    }
    
    @IBAction func uploadTapped(_ sender: UIButton) {
        let segueVC = self.storyBoardInsurance?.instantiateViewController(withIdentifier: "PLInsuranceUploadVC") as! PLInsuranceUploadVC
        segueVC.delegate = self
        segueVC.parameters = parameters
        present(segueVC, animated: false, completion: nil)
    }
    
    /// Delegate from PLInsuranceUploadVC
    func didFinishUploading() {
        contactLBL.isHidden = false
        uploadBTN.isHidden = true

    }
    
}
