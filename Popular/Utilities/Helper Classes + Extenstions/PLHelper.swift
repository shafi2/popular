//
//  PLHelper.swift
//  Popular
//
//  Created by Appzoc on 23/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import NotificationBannerSwift

var currentBannerMessage: String = ""

func showBanner(title: String = "", message: String, style: BannerStyle = .danger , color: UIColor = .darkGray){
    BaseThread.asyncMain {
        
        /// this is for showing a banner once if the message is same and if already there is a banner
        if currentBannerMessage == message {
            if NotificationBannerQueue.default.numberOfBanners > 0 {
                return
            }
        }
        
        let titleAttribute = [ NSAttributedStringKey.font: UIFont(name: "Roboto-Medium", size: 18.0)! ]
        let subtitleAttribute = [ NSAttributedStringKey.font: UIFont(name: "Roboto-Medium", size: 15.0)! ]
        let titleAttrString = NSAttributedString(string: title, attributes: titleAttribute)
        let subtitleAttrString = NSAttributedString(string: message, attributes: subtitleAttribute)

        let banner = NotificationBanner(attributedTitle: titleAttrString, attributedSubtitle: subtitleAttrString, style: style)
       // let banner = NotificationBanner(title: title, subtitle: message, style: style)
        banner.duration = 2.5
        banner.show(queuePosition: .front, bannerPosition: .top)
        banner.backgroundColor = color
        currentBannerMessage = message
    }
}

func showBannerOn(VC: UIViewController, title: String = "", message: String, style: BannerStyle = .danger){
    BaseThread.asyncMain {
        let banner = NotificationBanner(title: title, subtitle: message, style: style)
        banner.duration = 3
        banner.show(bannerPosition: .top)
    }
}

//MARK: Youtube ID From URL
func getIDFromURL(url:String) -> String?{
    //https://www.youtube.com/watch?v=mhz5CSBaCjA
    return url.components(separatedBy: "=").last
}

func getThumbnailImage(forUrl url: String) -> String? {
    
    if let videoID = getIDFromURL(url: url){
        //https://img.youtube.com/vi/EHAI_LDNhi4/3.jpg
        let ThumbnailURLString = "https://img.youtube.com/vi/\(videoID)/0.jpg"
        return ThumbnailURLString
    }else{
        return nil
    }
}

//MARK: Redirect to Settings To enable Access

func redirectToSettings(key:String, vc: UIViewController){
    let redirectAlert = UIAlertController(title: "Access Required for \(key)", message: "Please enable access to \(key) in Settings", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        redirectAlert.dismiss()
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
        redirectAlert.dismiss()
    }
    redirectAlert.addAction(okAction)
    redirectAlert.addAction(cancelAction)
    vc.present(redirectAlert)
}

//public func showBannerStyle(title: String, message: String, style: BannerStyle){
//    NotificationBanner(title: "", subtitle: "Invalid Email", style: .danger).show(queuePosition: .front)
//}

// Mark: - call this funcntion in the shouldChangeCharactersInRange function
func restrictTextFor(limit: Int, fullString: String, range: NSRange, string: String) -> Bool{
    let maxLength = limit
    let currentString: NSString = fullString as NSString
    let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
    return newString.length <= maxLength

}

/*
let calendar = NSCalendar.current

// Replace the hour (time) of both dates with 00:00
let date1 = calendar.startOfDay(for: firstDate)
let date2 = calendar.startOfDay(for: secondDate)

let components = calendar.dateComponents([.day], from: date1, to: date2)
*/

func findNumberOfDaysOrHours(from date:Date) -> String{
    let calendar = NSCalendar.current
    let firstDate = calendar.startOfDay(for: date)
    let currentDate = calendar.startOfDay(for: Date())
    var components = calendar.dateComponents([.day], from: firstDate, to: currentDate)
    if let dayCount = components.day{
        if  dayCount == 0{
            let hourCurrentDate = Date()
            components = calendar.dateComponents([.hour], from: date, to: hourCurrentDate)
            return (components.hour?.description ?? "0") + " Hour"
        }else{
            return dayCount.description + " Day"
        }
    }
    return ""
}

//MARK: Transition Background to Black
func transitionBackgroundToBlack(view:UIView){
    DispatchQueue.main.async {
        UIView.animate(withDuration: 0.17, animations: {
            view.backgroundColor = UIColor.black.withAlphaComponent(0.47)
        })
    }
}

enum PhoneModel: String {
    case model_5_5s
    case model_6_6s_7_8
    case model_6P_6sP_7P_8P
    case model_X
    case none
}

func detectPhoneModel() -> PhoneModel {
    var currentModel: PhoneModel = .none
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            //print("iPhone 5 or 5S or 5C")
            currentModel = .model_5_5s
        case 1334:
            //print("iPhone 6/6S/7/8")
            currentModel = .model_6_6s_7_8

        case 1920, 2208:
            //print("iPhone 6+/6S+/7+/8+")
            currentModel = .model_6P_6sP_7P_8P

        case 2436:
            //print("iPhone X")
            currentModel = .model_X

        default:
           // print("unknown")
            currentModel = .none

        }
    }
    return currentModel
}

func loadHomeVC(vc: UIViewController){
    BaseThread.asyncMain {
        let segueVC = vc.storyBoardMain!.instantiateViewController(withIdentifier: "PLHomeVC")
        vc.present(segueVC, animated: true, completion: nil)
    }
}

func getSegueVC(storyboardName: String, identifier: String) -> UIViewController{
    let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
    let segueVC = storyBoard.instantiateViewController(withIdentifier: identifier)
    return segueVC
}

func makeCallTo(number: String) {
    //print("makeCallTo")
    BaseThread.asyncMain {
        if let url = URL(string: "tel://" + number), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

    }
}

// if application did enter background while web service calling - when it app become active block the response messages
func handleDidBecomeActive(){
    BaseThread.background(delay: 2, background: {
        isEnteredBackGround = false
    })
}


