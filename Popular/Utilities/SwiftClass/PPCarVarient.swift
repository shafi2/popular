//
//  File.swift
//  Popular
//
//  Created by Appzoc on 24/10/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import CoreData

class PPCarVarient{
    
    var id : Int64
    var details: Data?
    
    init(id:Int64,details:Data?) {
        
        self.details = details
        self.id      = id
    }
    
    init(entity:CarVarient) {
        self.details = entity.details
        self.id      = entity.id
    }
    
    func updateCoredata(entity:CarVarient){
        entity.details = self.details
        entity.id      = self.id
    }
    
}


