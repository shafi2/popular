////
////  Webservices.swift
////  Exhibiz
////
////  Created by Appzoc on 22/02/18.
////  Copyright © 2018 appzoc. All rights reserved.
////
//
import Foundation
import Alamofire
import Kingfisher
//import BRYXBanner

/////// Appzoc URL
//let baseURL = "http://app.appzoc.com/popular/backend/api/"
//let baseURLImage = "http://app.appzoc.com"

/////// Webcastle URL
//let baseURL = "http://popular.dev.webcastle.in/api/"
//let baseURLImage = "http://popular.dev.webcastle.in/"
//let imageURL = "http://popular.dev.webcastle.in/"

//// Popular URL
//let baseURL = "http://ec2-184-169-235-33.us-west-1.compute.amazonaws.com/api/"
//let baseURLImage = "http://ec2-184-169-235-33.us-west-1.compute.amazonaws.com/"
//let imageURL = "http://ec2-184-169-235-33.us-west-1.compute.amazonaws.com/"

//// Popular URL Alter
let baseURL = "http://pvsapp.popularmaruti.com/api/"
let baseURLImage = "http://pvsapp.popularmaruti.com/"
let imageURL = "http://pvsapp.popularmaruti.com/"



let usernameAuth = "castler"//"popular"
let passwordAuth = "WeDidAwesome"//"123456"

let AppstoreLink = "https://itunes.apple.com/in/genre/ios/id36?mt=8"
//http://pvsapp.popularmaruti.com

public func getImageAuthorization() -> AnyModifier {
    let modifier = AnyModifier { request in
        var requestMutable = request
        let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        requestMutable.setValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
        
        return requestMutable
    }
    return modifier
}

public func authorizationHeader() -> [String: String]? {
    var isRequiredAuth = false
    
    if baseURL.containsIgnoringCase(find: "dev.webcastle") {
        isRequiredAuth = true
    }
    
    if isRequiredAuth {
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        return headers

    }else {
        return nil
    }
    
}


public func isNetworkAvailabel() -> Bool {
    
    if NetworkReachabilityManager()!.isReachable {
        return true
    } else {
        //showBanner(title: "Network Error", message: "No internet connectivity")
        return false
    }
    
}

class ActivityIndicatorOnView
{
    var activityView:UIActivityIndicatorView!
    let mainScreen = UIScreen.main.bounds
    let backgroundView = UIView()
    func start(onView: UIView)
    {
        
        //F96F02
        
        if activityView == nil
        {
            backgroundView.frame = mainScreen
            activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            activityView.frame = CGRect(x: (mainScreen.maxX/2)-20,
                                        y: (mainScreen.maxY/2)-20,
                                        width: 40, height: 40)
            backgroundView.addSubview(activityView)
            backgroundView.backgroundColor = UIColor(red:170/255, green:170/255, blue:170/255, alpha:0.5)
            onView.addSubview(backgroundView)
            //UIApplication.shared.keyWindow?.addSubview(self.view)
            activityView.startAnimating()
        }
        else if activityView.isAnimating == true
        {
        }
        else
        {
            onView.addSubview(backgroundView)
            activityView.startAnimating()
        }
    }
    
    func stop()
    {
        if activityView == nil || activityView.isAnimating == false
        {
        }
        else
        { activityView.stopAnimating()
            backgroundView.removeFromSuperview()
        }
    }
}


//    func showctivityOnVC(){
//    var activityView:UIActivityIndicatorView!
//    activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//    activityView.frame = CGRect(x: (emptyView.bounds.maxX/2)-20,
//                                y: (emptyView.bounds.maxY/2)-20,
//                                width: 40, height: 40)
//    emptyView.addSubview(activityView)
//    emptyView.backgroundColor = UIColor(red:170/255, green:170/255, blue:170/255, alpha:0.8)
//
//    activityView.startAnimating()
//    }

class ActivityIndicator
{
    var activityView:UIActivityIndicatorView!
    var view = UIView(frame: UIScreen.main.bounds)
    
    func start()
    {
        BaseThread.asyncMain {
            
            if self.activityView == nil
            {
                self.activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                self.activityView.frame = CGRect(x: (self.view.bounds.maxX/2)-20,
                                            y: (self.view.bounds.maxY/2)-20,
                                            width: 40, height: 40)
                self.view.addSubview(self.activityView)
                self.view.backgroundColor = UIColor(red:170/255, green:170/255, blue:170/255, alpha:0.8)
                UIApplication.shared.keyWindow?.addSubview(self.view)
                self.activityView.startAnimating()
            }
            else if self.activityView.isAnimating == true
            {
                //activityView.stopAnimating()
            }
            else
            {
                UIApplication.shared.keyWindow?.addSubview(self.view)
                self.activityView.startAnimating()
            }
            
        }
        //F96F02
    }
    
    func stop()
    {
        BaseThread.asyncMain {
            if self.activityView == nil || self.activityView.isAnimating == false
            {
            }
            else
            {
                self.activityView.stopAnimating()
                self.view.removeFromSuperview()
            }

        }
    }
}

public func serializeJSON(json:Any)->Data?{
    do{
        let serializedData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        return serializedData
    }catch{
        print("Serialize JSON Failed")
        print(error)
    }
    return nil
}

final class WebServices {
    
    // Parameterised GET Method
    class func getMethodWith(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
            let activity = ActivityIndicator()
            activity.start()
            
            print("\(baseURL)\(url)")
            
            Alamofire.request("\(baseURL)\(url)",
                method: .get,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                //.validate()
                .responseJSON
                { response in
                    print(response)
                    
                    activity.stop()
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            print(result)
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            let message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                showBanner(message: message)
                                CompletionHandler(false, ["":""])
                            }
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])
                        break
                    }
            }

        }
        
    }
    
    
    
    class func getMethodWithOutActivity(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
            print("\(baseURL)\(url)")
            
//            let user = usernameAuth
//            let password = passwordAuth
//            let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)"]
            
            Alamofire.request("\(baseURL)\(url)",
                method: .get,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .validate()
                .responseJSON
                { response in
                    //print(response as AnyObject)
                    switch(response.result)
                    {
                    case .success(_):
                        
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            //print(result)
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            let message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                showBanner(title: "", message: message)
                                CompletionHandler(false, ["":""])
                            }
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                    case .failure(_):
                        
                        CompletionHandler(false, [:])
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        break
                    }
            }

        }
        
    }
    
    
    
    

    
    
    class func postMethod3(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            let activity = ActivityIndicator()
            activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            print("parameter:",parameter)
            print(baseURL + url)
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                     print(response as AnyObject)

                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                             print(result)
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            let message = result["message"] as? String ?? "No Data"
                            
                            activity.stop()
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                showBanner(message: message)
                                CompletionHandler(false, ["":""])
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])

                        break
                        
                    }
                    
            }
        }
    }
    
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            let activity = ActivityIndicator()
            activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            print("URL:",baseURL.description + url)
            print("parameter:",parameter)
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                     //print(response as AnyObject)
                    activity.stop()

                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            
                            let Errorcode = result["errorcode"] as! Int
                            let message = result["message"] as? String ?? "No Data"
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                showBanner(message: message)
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])

                        break
                        
                    }
                    
            }
        }
    }
    
    class func postMethodWithoutActivityAndMessage(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            //        let activity = ActivityIndicator()
            //        activity.start()
            
            //            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
            //            let base64Credentials = credentialData.base64EncodedString(options: [])
            //            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            
            
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                    print(response as AnyObject)
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            _ = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            //                        activity.stop()
                            //print("Errorcode:",Errorcode,"Message:",result["Message"] as Any)
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            CompletionHandler(false, [:])
                            
                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        if !isEnteredBackGround {
                            //showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false,["":""])
                        break
                        
                    }
                    
            }
        }
    }
    
    class func postMethodWithoutActivity(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            //        let activity = ActivityIndicator()
            //        activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            

            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                     print(response as AnyObject)

                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            let message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            //                        activity.stop()
                            //print("Errorcode:",Errorcode,"Message:",result["Message"] as Any)
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                showBanner(message: message)
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false,["":""])
                        break
                        
                    }
                    
            }
        }
    }
    
    /// without activity and banner messages
    class func postMethodWithoutActivityBanner(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                    //print(response as AnyObject)
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            //let message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            //                        activity.stop()
                            print("Errorcode:",Errorcode,"Message:",result["Message"] as Any)
                            
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                CompletionHandler(false, [:])
                                
                            }
                            
                        }else {
                            //showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])
                            
                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        if !isEnteredBackGround {
                            //showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false,[:])
                        break
                        
                    }
                    
            }
        }
    }
    
    class func postMethodErrorCode(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
            let activity = ActivityIndicator()
            activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                    print(response as AnyObject)
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            print(result)
                            
                            let Errorcode = result["ErrorCode"] as? Int ?? 1
                            _ = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            activity.stop()
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])
                        
                        break
                        
                    }
                    
            }

        }
        
    }
    
    
    class func postMethodErrorCode2(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
            let activity = ActivityIndicator()
            activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                    print(response as AnyObject)
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            print(result)
                            
                            let Errorcode = result["errorcode"] as? String ?? ""
                            _ = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            activity.stop()
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])
                        
                        break
                        
                    }
                    
            }

        }
        
    }
    
    class func postMethodWithoutActivityIndicator(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            
            let activity = ActivityIndicator()
            activity.start()
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                    print(response as AnyObject)
                    
                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            print(result)
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            _ = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            //activity.stop()
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                CompletionHandler(false, ["":""])
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])
                        
                        break
                        
                    }
                    
            }

        }
        
        //    let activity = ActivityIndicator()
        //    activity.start()
        
    }
    
    
    class func postMethodWithErrorValue(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        } else {
            
            let activity = ActivityIndicator()
            activity.start()
            
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request("\(baseURL)\(url)",
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.default,
                headers: authorizationHeader())
                .responseJSON
                { response in
                     print(response as AnyObject)

                    switch(response.result)
                    {
                    case .success(_):
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                             print(result)
                            
                            let Errorcode = result["errorcode"] as? Int ?? 1
                            _ = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            activity.stop()
                            if Errorcode == 0
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {
                                CompletionHandler(false, result)
                                
                            }
                            
                        }else {
                            showBanner(message: "Server response error ")
                            CompletionHandler(false, [:])

                        }
                        break
                        
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, [:])

                        break
                        
                    }
                    
            }
        }
    }
    

    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }else {
            let activity = ActivityIndicator()
            activity.start()
            print("\(baseURL)\(url)")
            print(parameter)
            print(imageParameter)
            print("=====")
//            let credentialData = "\(usernameAuth):\(passwordAuth)".data(using: String.Encoding.utf8)!
//            let base64Credentials = credentialData.base64EncodedString(options: [])
//            let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
            
            Alamofire.upload(multipartFormData:
                { multipartFormData in
                    
                    for (key, value) in parameter
                    {
                        multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                    }
                    if !imageParameter.isEmpty {
                        let imageKey = imageParameter[imageParameter.startIndex].key
                        let imageArray = imageParameter[imageKey]
                        var index: Int = 1
                        for image in imageArray! {
                            
                            let imageData = UIImageJPEGRepresentation(image, 1)!
                            let imageFileName = imageKey.replacingOccurrences(of: "[]", with: "") + index.description + ".jpeg"
                            
//                            if imageKey.contains("]") {
//                                print("imageKeyContains array")
////                                imageKeyModified = imageKey.replacingOccurrences(of: "[", with: "")
////                                print("ImagheMod:",imageKeyModified)
////                                imageKeyModified = imageKey.replacingOccurrences(of: "]", with: "").trimmingCharacters(in: .whitespaces)
////                                print("ImagheMod222:",imageKeyModified)
////                                imageKeyModified = imageKeyModified +/* "[" +*/ index.description + "]"
//
//                                imageKeyModified = imageKey
//
//                            }else {
//                                print("imageKeyContains single image")
//                                imageKeyModified = imageKey
//                            }

                            multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                            index += 1
                            //print("ImageKey:", imageKey,"imagekeyModified:",image,"resizediMaeg",image.compressToBelow1MB(),":",imageData)
                        }
                        
                    }
                    
            },
                             to: "\(baseURL)\(url)",
                headers : nil,
                encodingCompletion:
                { encodingResult in
                    print(encodingResult as AnyObject)
                    
                    switch encodingResult {
                        
                    case .success(let upload, _, _):
                        
                        upload.responseJSON(completionHandler: { response in
                            print("Response Value ",response.result.value as Any)
                            activity.stop()
                            if response.result.value != nil
                            {
                                
                                let result = response.result.value as! [String:Any]
                                print(result)
                                
                                let Errorcode = result["errorcode"] as? Int ?? 1
                                let Message = result["message"] as? String ?? "Error occured while fetching data"
                                
                                if Errorcode == 0
                                {
                                    CompletionHandler(true, result)
                                }
                                else
                                {
                                    showBanner(message: Message)
                                    CompletionHandler(false, ["":""])
                                }
                            }else {
                                showBanner(message: "Server response error ")
                                CompletionHandler(false, [:])
                                
                            }
                        })
                        
                    case .failure(_):
                        print("Fail Call")
                        activity.stop()
                        if !isEnteredBackGround {
                            showBanner(title: "Network Error", message: "Can't connect with server")
                        }
                        handleDidBecomeActive()
                        CompletionHandler(false, ["":""])
                        
                    }
            }
            )
            
        }

        }
        
    
}




