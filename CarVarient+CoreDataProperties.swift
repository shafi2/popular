//
//  CarVarient+CoreDataProperties.swift
//  
//
//  Created by Appzoc on 24/10/18.
//
//

import Foundation
import CoreData


extension CarVarient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CarVarient> {
        return NSFetchRequest<CarVarient>(entityName: "CarVarient")
    }

    @NSManaged public var details: Data?
    @NSManaged public var id: Int64

}
